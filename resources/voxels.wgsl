@group(0) @binding(0) var<uniform> view_matrix: mat4x4<f32>;
@group(0) @binding(1) var<uniform> projection_matrix: mat4x4<f32>;

@group(1) @binding(1000) var<uniform> model_matrix: mat4x4<f32>;

@group(2) @binding(2000) var<uniform> voxel_resolution: vec3<u32>;
@group(2) @binding(2001) var<uniform> voxel_block_size: f32;

@group(3) @binding(3000) var texture: texture_2d<f32>;
@group(3) @binding(3001) var texture_sampler: sampler;



struct VertexInput {
    @location(0) position: vec3<f32>,
    //@location(1) vertex_color: vec4<f32>,
    //@location(2) vertex_normal: vec3<f32>,
    @location(3) texture_coordinates: vec2<f32>,
}

struct InstanceInput {
    @location(10) color: vec4<f32>,
    @location(11) orientation: u32,
    @location(12) index: u32,
}

struct VertexOutput {
    @builtin(position) screen_position: vec4<f32>,
    @location(0) world_position: vec3<f32>,
    @location(1) vertex_color: vec4<f32>,
    @location(2) texture_coordinates: vec2<f32>,
}

struct FragmentInput {
    @location(0) world_position: vec3<f32>,
    @location(1) interpolated_vertex_color: vec4<f32>,
    @location(2) texture_coordinates: vec2<f32>,
}

fn voxel_face_transformation_matrix(orientation: u32, voxel_cell_size: f32) -> mat4x4<f32> {
    var half_voxel_cell_size = voxel_cell_size / 2.0;

    switch(orientation) {
        // Left facing: rotate -90 deg on Y axis
        case 0u: {
            return mat4x4<f32>(
                0.0, 0.0, -1.0, 0.0,
                0.0, 1.0, 0.0, 0.0,
                1.0, 0.0, 0.0, 0.0,
                -half_voxel_cell_size, 0.0, 0.0, 1.0,
            );
        }

        // Right facing: rotate 90 deg on Y axis
        case 1u: {
            return mat4x4<f32>(
                0.0, 0.0, 1.0, 0.0,
                0.0, 1.0, 0.0, 0.0,
                -1.0, 0.0, 0.0, 0.0,
                half_voxel_cell_size, 0.0, 0.0, 1.0,
            );
        }

        // Bottom facing: rotate -90 deg on X axis
        case 2u: {
            return mat4x4<f32>(
                1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, -1.0, 0.0,
                0.0, 1.0, 0.0, 0.0,
                0.0, -half_voxel_cell_size, 0.0, 1.0,
            );
        }

        // Top facing: rotate 90 deg on X axis
        case 3u: {
            return mat4x4<f32>(
                1.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0,
                0.0, -1.0, 0.0, 0.0,
                0.0, half_voxel_cell_size, 0.0, 1.0,
            );
        }

        // Near facing: no rotation necessary, translate Z towards near
        case 4u: {
            return mat4x4<f32>(
                1.0, 0.0, 0.0, 0.0,
                0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, -half_voxel_cell_size, 1.0,
            );
        }

        // Far facing: rotate 180 deg on X or Y axis
        case 5u: {
            return mat4x4<f32>(
              1.0,  0.0,  0.0,  0.0,
              0.0, -1.0,  0.0,  0.0,
              0.0, -0.0, -1.0,  0.0,
              0.0,  0.0,  half_voxel_cell_size,  1.0,
            );
        }

        default: {
            return mat4x4<f32>(
                1.0, 0.0, 0.0, 0.0,
                0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 1.0,
            );
        }
    }
}

fn coordinates(index: u32, voxel_resolution: vec3<u32>) -> vec3<u32> {
    var width = voxel_resolution.x;
    var height = voxel_resolution.y;
    var width_height = width * height;

    var z = index / width_height;
    var z_remainder = index % width_height;
    var y = z_remainder / width;
    var x = z_remainder % width;

    return vec3<u32>(x, y, z);
}

@vertex
fn vs_main(vertex: VertexInput, instance: InstanceInput) -> VertexOutput {
    var color: vec4<f32> = instance.color;

    var half_voxel_block_size = voxel_block_size / 2.0;
    var grid_size = vec3<f32>(voxel_resolution) * voxel_block_size;
    var half_grid_size = grid_size / 2.0;

    var left_bottom_near_grid_corner = half_grid_size * -1.0;

    var coordinates = coordinates(instance.index, voxel_resolution);
    var voxel_center = left_bottom_near_grid_corner + half_voxel_block_size + voxel_block_size * vec3<f32>(coordinates);

    var homogeneous_position = vec4<f32>(vertex.position, 1.0);
    var orientation_matrix = voxel_face_transformation_matrix(instance.orientation, voxel_block_size);
    var homogeneous_oriented_position = orientation_matrix * homogeneous_position;
    var homogeneous_position_within_voxel = homogeneous_oriented_position + vec4<f32>(voxel_center, 0.0);

    var model_view_matrix: mat4x4<f32> = view_matrix * model_matrix;

    var output: VertexOutput;
    output.screen_position = projection_matrix * model_view_matrix * homogeneous_position_within_voxel;
    output.world_position = (model_view_matrix * homogeneous_position_within_voxel).xyz / homogeneous_position_within_voxel.w;
//    output.vertex_color = (instance.color + color) / 2.0;
    output.vertex_color = color;
    output.texture_coordinates = vertex.texture_coordinates;
    return output;
}

@fragment
fn fs_main(input: FragmentInput) -> @location(0) vec4<f32> {
//    var red: vec4<f32> = vec4<f32>(1.0, 0.0, 0.0, 1.0);
//    var green: vec4<f32> = vec4<f32>(0.0, 1.0, 0.0, 1.0);
//    var blue: vec4<f32> = vec4<f32>(0.0, 0.0, 1.0, 1.0);
//
//    var yellow: vec4<f32> = vec4<f32>(1.0, 1.0, 0.0, 1.0);
//    var cyan: vec4<f32> = vec4<f32>(0.0, 1.0, 1.0, 1.0);
//    var magenta: vec4<f32> = vec4<f32>(1.0, 0.0, 1.0, 1.0);
//    var white: vec4<f32> = vec4<f32>(1.0, 1.0, 1.0, 1.0);
//    var black: vec4<f32> = vec4<f32>(0.0, 0.0, 0.0, 1.0);
//
    var x: vec3<f32> = dpdx(input.world_position);
    var y: vec3<f32> = dpdy(input.world_position);
    var c: vec3<f32> = cross(x, y);
    var surface_normal: vec3<f32> = normalize(c);

//    var x_axis = vec3<f32>(1.0, 0.0, 0.0); // red
//    var y_axis = vec3<f32>(0.0, 1.0, 0.0); // green
//    var z_axis = vec3<f32>(0.0, 0.0, 1.0); // blue
//
//    var x_exposure = max(-dot(x_axis, surface_normal), 0.0);
//    var y_exposure = max(-dot(y_axis, surface_normal), 0.0);
//    var z_exposure = max(-dot(z_axis, surface_normal), 0.0);
//
//    var diffuse_color = vec4<f32>(x_exposure, y_exposure, z_exposure, 1.0);

//    // https://en.wikipedia.org/wiki/Lambertian_reflectance#Use_in_computer_graphics
    var light_position = vec3<f32>(0.0, 0.0, 1.0);
//    var lightColor = white;
//
    var light_direction = normalize(light_position - input.world_position);
    var intensity = max(dot(surface_normal, light_direction), 0.0);
    var color = textureSample(texture, texture_sampler, input.texture_coordinates);
    return ((color * 0.5) + (input.interpolated_vertex_color * 0.5)) * intensity;
}
