@group(0) @binding(9001) var<uniform> model_matrix: mat4x4<f32>;
@group(1) @binding(9002) var<uniform> view_matrix: mat4x4<f32>;
@group(1) @binding(9003) var<uniform> perspective_matrix: mat4x4<f32>;

struct VertexOutput {
    @builtin(position) screenPosition: vec4<f32>,
    @location(0) worldPosition: vec3<f32>,
    @location(1) color: vec4<f32>,
}

@vertex
fn vs_main(@location(10) vertex: vec3<f32>, @location(20) color: vec4<f32>) -> VertexOutput {
    var homogeneousVertex = vec4<f32>(vertex, 1.0);

    var output: VertexOutput;

    var model_view_matrix: mat4x4<f32> = view_matrix * model_matrix;

    output.screenPosition = perspective_matrix * model_view_matrix * homogeneousVertex;
    output.worldPosition = (model_view_matrix * homogeneousVertex).xyz / homogeneousVertex.w;
    output.color = color;
    return output;
}

struct FragmentInput {
    @location(0) worldPosition: vec3<f32>,
    @location(1) color: vec4<f32>,
}

@fragment
fn fs_main(input: FragmentInput) -> @location(0) vec4<f32> {
    var red: vec4<f32> = vec4<f32>(1.0, 0.0, 0.0, 1.0);
    var green: vec4<f32> = vec4<f32>(0.0, 1.0, 0.0, 1.0);
    var blue: vec4<f32> = vec4<f32>(0.0, 0.0, 1.0, 1.0);
    var white: vec4<f32> = vec4<f32>(1.0, 1.0, 1.0, 1.0);
    var yellow: vec4<f32> = vec4<f32>(1.0, 1.0, 0.0, 1.0);
    var cyan: vec4<f32> = vec4<f32>(0.0, 1.0, 1.0, 1.0);
    var magenta: vec4<f32> = vec4<f32>(1.0, 0.0, 1.0, 1.0);

    var x = dpdx(input.worldPosition);
    var y = dpdy(input.worldPosition);
    var c = cross(x, y);
    var surface_normal = normalize(c);

    // https://en.wikipedia.org/wiki/Lambertian_reflectance#Use_in_computer_graphics
    var light_position = vec3<f32>(0.0, 0.0, 1.0);
    var lightColor = white;

    var light_direction = normalize(light_position - input.worldPosition);

    var intensity = max(dot(surface_normal, light_direction), 0.0);

    return input.color * intensity;

}
