use log::{Level, LevelFilter};
use std::io::Write;
use env_logger::{Builder, WriteStyle};
use env_logger::fmt::Color;

pub fn initialize_logging(root_module: &str, root_module_level_filter: LevelFilter) {
    let mut builder = Builder::new();
    let root_module = String::from(root_module);
    builder.write_style(WriteStyle::Always);
    builder.filter_module(root_module.as_str(), root_module_level_filter);
    builder.filter_level(LevelFilter::Info);
    builder.format(move |formatter, record| {
        let timestamp = formatter.timestamp_millis();
        let message = record.args();
        // let level = record.level();
        let module = record.module_path().unwrap_or("(unknown module)");

        let header = format!("{} | {:<50} |", timestamp, module);
        let padding = " ".repeat(header.len() - 2);
        let padded_message = message.to_string().replace("\n", format!("\n{} | ", padding).as_str());

        let mut style = formatter.style();
        style.set_color(match record.level() {
            Level::Info => Color::Green,
            Level::Warn => Color::Yellow,
            Level::Error => Color::Red,
            Level::Debug => Color::Blue,
            Level::Trace => Color::Magenta,
        });

        // Display output from our own modules in bright
        if module.starts_with(root_module.as_str()) {
            style.set_intense(true);
            style.set_dimmed(false);
            style.set_bold(true);
        } else {
            style.set_intense(false);
            style.set_dimmed(true);
            style.set_bold(false);

        }

        let line = format!("{} {}", header, padded_message);

        return writeln!(formatter, "{}", style.value(line));

    });
    builder.init();
}