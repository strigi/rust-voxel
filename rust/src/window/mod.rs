use raw_window_handle::HasRawWindowHandle;
use crate::controls::Key;
use crate::math::Dimensions2u;

pub trait Window : HasRawWindowHandle {
    fn size(&self) -> Dimensions2u;
    fn poll_event(&mut self) -> Option<Event>;
    fn wait_event(&mut self) -> Event;
}

#[derive(Debug)]
pub enum Event {
    /// Resize event will always be emitted when the window is resized and also when it's shown for the first time.
    Resize(Dimensions2u),

    /// Input events are like key presses, mouse movement, gamepad buttons, ...
    Input(Key, f32),

    /// Emitted when the window wants to close (i.e. the user has pressed the close (X) button)
    /// Allows for gracefully shutting down the application.
    Close,
}
