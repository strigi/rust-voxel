use std::io::{Read, Seek, SeekFrom};
use crate::math::{Color, Dimensions2u};

pub struct Image {
    size: Dimensions2u,
    bits_per_pixel: u16,
    pixels: Box<[u8]>,
}

impl Image {
    pub fn read(file_path: &str) -> Self {
        log::info!("Reading bitmap file '{}'", file_path);
        let mut file = std::fs::File::open(file_path).unwrap();

        let mut one_byte_buffer = [0u8; 1];
        let mut two_byte_buffer = [0u8; 2];
        let mut three_byte_buffer = [0u8; 3];
        let mut four_byte_buffer = [0u8; 4];

        file.seek(SeekFrom::Start(0)).unwrap();
        file.read_exact(&mut two_byte_buffer).unwrap();
        if two_byte_buffer[0] != b'B' || two_byte_buffer[1] != b'M' {
            panic!("BMP file '{}' is not valid", file_path)
        }

        file.seek(SeekFrom::Start(10)).unwrap();
        file.read_exact(&mut four_byte_buffer).unwrap();
        let offset = u32::from_le_bytes(four_byte_buffer);
        log::trace!("Detected pixel data offset at seek position: {}", offset);

        file.seek(SeekFrom::Start(18)).unwrap();
        file.read_exact(&mut four_byte_buffer).unwrap();
        let width = u32::from_le_bytes(four_byte_buffer);
        file.seek(SeekFrom::Start(22)).unwrap();
        file.read_exact(&mut four_byte_buffer).unwrap();
        let height = u32::from_le_bytes(four_byte_buffer);
        let size = Dimensions2u::size(width, height);

        file.seek(SeekFrom::Start(28)).unwrap();
        file.read_exact(&mut two_byte_buffer).unwrap();
        let bits_per_pixel = u16::from_le_bytes(two_byte_buffer);
        log::debug!("Image has a size of {} pixels and a precision of {} bits per pixel.", size, bits_per_pixel);
        if bits_per_pixel < 32 {
            log::warn!("Due to current implementation limits, the image will be stored as 32 bits per pixel instead of the image's native {} bits per pixel, because only RGBA format is internally supported", bits_per_pixel);
        }

        file.seek(SeekFrom::Start(offset as u64)).unwrap();
        let mut pixels = Vec::with_capacity(size.area() as usize);
        for _ in 0..(width * height) {
            if bits_per_pixel == 8 {
                file.read_exact(&mut one_byte_buffer).unwrap();
                let monochrome = u8::from_le_bytes(one_byte_buffer);
                pixels.push(monochrome);
                pixels.push(monochrome);
                pixels.push(monochrome);
                pixels.push(255);
            } else if bits_per_pixel == 24 {
                file.read_exact(&mut three_byte_buffer).unwrap();
                let red = three_byte_buffer[2];
                let green = three_byte_buffer[1] ;
                let blue = three_byte_buffer[0];
                pixels.push(red);
                pixels.push(green);
                pixels.push(blue);
                pixels.push(255);
            } else {
                panic!("Bits per pixel {} not supported a the moment", bits_per_pixel);
            }
        }

        return Self {
            size,
            pixels: pixels.into_boxed_slice(),
            bits_per_pixel: 32,
        }
    }

    pub fn size_ref(&self) -> &Dimensions2u {
        return &self.size;
    }

    pub fn get_color_at(&self, x: u32, y: u32) -> Color {
        if x >= self.size.width() || y >= self.size.height() {
            panic!("Pixel index out of bounds: indexes ({}, {}) should be in range less than ({}, {})", x, y, self.size.width(), self.size.height())
        }

        let index = (x + y * self.size.width()) as usize;

        let bytes_per_pixel = 4;
        let red = self.pixels[(index * bytes_per_pixel) + 0];
        let green = self.pixels[(index * bytes_per_pixel) + 1];
        let blue = self.pixels[(index* bytes_per_pixel) + 2];
        return Color::rgb(red as f32 / 255.0, green as f32 / 255.0, blue as f32 / 255.0);
    }

    /// Returns the raw pixel data. The number of bytes per pixel depends on the bit depth and is currently either 8 (monochrome) or 24 (rgb)
    pub fn data_ref(&self) -> &[u8] {
        return self.pixels.as_ref();
    }
}
