use std::fmt::{Display, Error, Formatter, Result};
use crate::timer::Timer;

pub struct Statistics {
    frame_counter: u32,
    frame_counter_mark: u32,
    timer: Timer,
}

impl Statistics {
    pub fn new() -> Self {
        return Self {
            frame_counter: 0,
            frame_counter_mark: 0,
            timer: Timer::from_frequency(1.0),
        };
    }

    pub fn update(&mut self) {
        if self.timer.is_expired() {
            log::debug!("{}", self);
            self.frame_counter_mark = self.frame_counter;
            self.timer.mark();
        }
        self.frame_counter += 1;
    }

    pub fn seconds_per_frame(&self) -> f32 {
        return 1.0 / self.frames_per_second();
    }

    pub fn frame_delta(&self) -> u32 {
        return self.frame_counter - self.frame_counter_mark;
    }

    pub fn time_delta(&self) -> f32 {
        return self.timer.delta();
    }

    pub fn frames_per_second(&self) -> f32 {
        return self.frame_delta() as f32 / self.time_delta();
    }
}

impl Display for Statistics {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        let mut result = Ok(());
        result = result.and(write!(f, "FRAME RATE: rendered {} frames in {:.3} s which is an average of {:.1} fps frametime: {:.3} s frame counter: {}",
            self.frame_delta(),
            self.time_delta(),
            self.frames_per_second(),
            self.seconds_per_frame(),
            self.frame_counter
        ));
        return result;
    }
}