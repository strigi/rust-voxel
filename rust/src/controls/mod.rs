use std::collections::HashMap;

type Listener<T, A> = fn(ControlEvent<T, A>);
type ListenersMap<T, A> = HashMap<Key, Vec<Listener<T, A>>>;


pub struct Controls<T = (), A = ()> {
    target: T,
    values_map: HashMap<Key, f32>,
    once_listeners_map: ListenersMap<T, A>,
    times_listeners_map: ListenersMap<T, A>,
    attributes: A
}

#[derive(Debug, PartialEq)]
pub enum ListenerMode {
    ONCE,
    TIMES,
}

impl <T, A> Controls<T, A> {
    pub fn new(target: T, attributes: A) -> Self {
        return Self {
            target,
            values_map: HashMap::new(),
            once_listeners_map: HashMap::new(),
            times_listeners_map: HashMap::new(),
            attributes,
        }
    }

    pub fn update(&mut self, key: Key, value: f32) {
        if Self::active(value) && !self.is_active(&key) {
            self.emit(key.clone(), value, ListenerMode::ONCE);
        }
        self.values_map.insert(key, value);
    }

    /// Values higher than 0.5 are considered to be "on". This is so that both continuous and discrete values
    /// can be considered to have a binary "on" and "off" state
    pub fn active(value: f32) -> bool {
        return value > 0.5;
    }

    pub(super) fn emit(&mut self, key: Key, value: f32, mode: ListenerMode) {
        let listeners_map = if mode == ListenerMode::ONCE {
            &self.once_listeners_map
        } else {
            &self.times_listeners_map
        };

        let listeners = listeners_map.get(&key);
        if listeners.is_none() {
            return;
        }

        for listener in listeners.unwrap() {
            let event = ControlEvent {
                trigger: key.clone(),
                value,
                target: &mut self.target,
                attributes: &mut self.attributes
            };
            listener(event);
        }
    }

    pub fn dispatch(&mut self) {
        let active_keys = self.all_active();
        for (key, value) in active_keys {
            self.emit(key, value, ListenerMode::TIMES);
        }
    }

    pub fn is_active(&self, key: &Key) -> bool {
        let old = self.values_map.get(key);
        return if old.is_some() && Self::active(*old.unwrap()) {
            true
        } else {
            false
        }
    }

    // TODO: rewrite this in a more Rustic way
    pub fn all_active(&self) -> Vec<(Key, f32)> {
        let mut results: Vec<(Key, f32)> = Vec::new();
        for key in self.values_map.keys() {
            if self.is_active(key) {
                let value = *self.values_map.get(key).unwrap();
                results.push((key.clone(), value))
            }
        }
        return results;
    }

    /// Register an event listener for the given input key that will trigger once upon entering the
    /// active state. Repeats will not be made.
    pub fn once(&mut self, key: Key, listener: Listener<T, A>) {
        self.register(key, listener, ListenerMode::ONCE);
    }

    /// Register an event listener for the given input key that will many times (continuously)
    /// active state. Repeats will not be made.
    pub fn times(&mut self, key: Key, listener: Listener<T, A>) {
        self.register(key, listener, ListenerMode::TIMES);
    }

    fn register(&mut self, key: Key, listener: Listener<T, A>, mode: ListenerMode) {
        let listeners_map = self.listener_map_mut(mode);
        if !listeners_map.contains_key(&key) {
            listeners_map.insert(key, vec![listener]);
        } else {
            listeners_map.get_mut(&key).unwrap().push(listener);
        }
    }

    fn listener_map_mut(&mut self, mode: ListenerMode) -> &mut ListenersMap<T, A> {
        return if mode == ListenerMode::ONCE {
            &mut self.once_listeners_map
        } else {
            &mut self.times_listeners_map
        }
    }

    fn listener_map_ref(&self, mode: ListenerMode) -> &ListenersMap<T, A> {
        return if mode == ListenerMode::ONCE {
            &self.once_listeners_map
        } else {
            &self.times_listeners_map
        }
    }

    pub fn target_ref(&self) -> &T {
        return &self.target;
    }

    pub fn target_mut(&mut self) -> &mut T {
        return &mut self.target;
    }

    pub fn attributes_ref(&self) -> &A {
        return &self.attributes;
    }

    pub fn attributes_mut(&mut self) -> &mut A {
        return &mut self.attributes;
    }
}

pub struct ControlEvent<'a, T, A> {
    pub trigger: Key,
    pub value: f32,
    pub target: &'a mut T,
    pub attributes: &'a mut A
}

#[derive(Debug, PartialEq, Clone, Eq, Hash)]
pub enum Key {
    Escape,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    PrintScreen,
    ScrollLock,
    Pause,

    Grave,          // Querty: `~ or Azerty: ²³
    Num1,
    Num2,
    Num3,
    Num4,
    Num5,
    Num6,
    Num7,
    Num8,
    Num9,
    Num0,
    Minus,
    Equals,
    Backspace,
    Insert,
    Home,
    PageUp,
    NumLock,
    KeypadDivide,
    KeypadMultiply,
    KeypadMinus,

    Tab,
    Q,
    W,
    E,
    R,
    T,
    Y,
    U,
    I,
    O,
    P,
    LeftBracket,        // Azerty ^¨[
    RightBracket,       // Azerty $*]
    Return,
    Delete,
    End,
    PageDown,
    Keypad7,
    Keypad8,
    Keypad9,
    KeypadPlus,

    CapsLock,
    A,
    S,
    D,
    F,
    G,
    H,
    J,
    K,
    L,
    Semicolon,          // Azerty: mM
    Apostrophe,         // Azerty: ù%´
    Backslash,          // Azerty: µ£`
    Keypad4,
    Keypad5,
    Keypad6,

    LeftShift,
    NonUsBackslash,     // Azerty: <>\
    Z,
    X,
    C,
    V,
    B,
    N,
    M,                  // Azerty: ,?
    Comma,              // Azerty: ;.
    Period,             // Azerty: :/
    Slash,              // Azerty: =+~
    RightShift,
    Up,
    Keypad1,
    Keypad2,
    Keypad3,
    KeypadEnter,

    LeftControl,
    LeftAlt,
    Space,
    RightAlt,
    Application,
    RightControl,
    Left,
    Down,
    Right,
    Keypad0,
    KeypadPeriod,
}


//     pub fn control(&self, scene: &mut Scene) {
//         const SPEED: f32 = 0.05;
//         const ANGULAR_SPEED: f32 = SPEED * 10.0;
//
//         let mut x = 0f32;
//         let mut y = 0f32;
//         let mut z = 0f32;
//
//         let mut camera_alpha = 0f32;
//         let mut camera_beta = 0f32;
//         let mut camera_gamma = 0f32;
//
//         let mut mesh_alpha = 0f32;
//         let mut mesh_beta = 0f32;
//         let mut mesh_gamma = 0f32;
//
//         if self.is_active(InputKey::Left) {
//             x -= SPEED;
//         }
//
//         if self.is_active(InputKey::Right) {
//             x += SPEED;
//         }
//
//         if self.is_active(InputKey::PageUp) {
//             y += SPEED;
//         }
//
//         if self.is_active(InputKey::PageDown) {
//             y -= SPEED;
//         }
//
//         if self.is_active(InputKey::Up) {
//             z += SPEED;
//         }
//
//         if self.is_active(InputKey::Down) {
//             z -= SPEED;
//         }
//
//         if self.is_active(InputKey::Q) {
//             camera_alpha += ANGULAR_SPEED;
//         }
//
//         if self.is_active(InputKey::S) {
//             camera_alpha -= ANGULAR_SPEED;
//         }
//
//         if self.is_active(InputKey::W) {
//             camera_beta += ANGULAR_SPEED;
//         }
//
//         if self.is_active(InputKey::X) {
//             camera_beta -= ANGULAR_SPEED;
//         }
//
//         if self.is_active(InputKey::A) {
//             camera_gamma = ANGULAR_SPEED;
//         }
//
//         if self.is_active(InputKey::Z) {
//             camera_gamma = -ANGULAR_SPEED;
//         }
//
//         if self.is_active(InputKey::O) {
//             mesh_alpha -= ANGULAR_SPEED
//         }
//
//         if self.is_active(InputKey::P) {
//             mesh_alpha += ANGULAR_SPEED
//         }
//
//         if self.is_active(InputKey::L) {
//             mesh_beta -= ANGULAR_SPEED
//         }
//
//         if self.is_active(InputKey::M) {
//             mesh_beta += ANGULAR_SPEED
//         }
//
//         if self.is_active(InputKey::B) {
//             mesh_gamma -= ANGULAR_SPEED
//         }
//
//         if self.is_active(InputKey::N) {
//             mesh_gamma += ANGULAR_SPEED
//         }
//
//         let moved = x != 0f32 || y != 0f32 || z != 0f32;
//         if moved {
//             scene.get_camera_mut().add_position(&Vec3::xyz(x, y, z));
//         }
//
//         let camera_rotated = camera_alpha != 0f32 || camera_beta != 0f32 || camera_gamma != 0f32;
//         if camera_rotated {
//             scene.get_camera_mut().add_rotation(&Vec3::xyz(camera_alpha, camera_beta, camera_gamma));
//         }
//
//         let mesh = scene.find_mesh_by_id_mut(0);
//         if mesh.is_some() {
//             let mesh = mesh.unwrap();
//             if !self.is_active(InputKey::F1) {
//                 mesh.add_rotation(&Vec3::xyz(0.1, 0.1, 0.1));
//             }
//             mesh.add_rotation(&Vec3::xyz(mesh_alpha, mesh_beta, mesh_gamma));
//         }
//
//         let mesh = scene.find_mesh_by_id_mut(1);
//         if mesh.is_some() {
//             if !self.is_active(InputKey::F2) {
//                 mesh.unwrap().add_rotation(&Vec3::xyz(0.1, 0.0, 0.0));
//             }
//         }
//
//         let mesh = scene.find_mesh_by_id_mut(2);
//         if mesh.is_some() {
//             if !self.is_active(InputKey::F3) {
//                 mesh.unwrap().add_rotation(&Vec3::xyz(0.0, 0.2, 0.0));
//             }
//         }
//     }
// }
