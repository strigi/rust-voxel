mod renderer;
mod math;
mod platform;
mod scene;
mod controls;
mod controller;
mod window;
mod timer;
mod statistics;
mod image;
mod logging;

use log::LevelFilter;
use crate::controller::Controller;
use crate::window::{Event, Window};
use crate::platform::sdl::SDLWindow;
use crate::renderer::{Renderer};
use crate::logging::initialize_logging;

fn main() {
    initialize_logging(module_path!(), LevelFilter::Debug);
    let mut window = SDLWindow::new("Strigi's Voxel Renderer", 1280, 720);
    let mut controller = Controller::new(Renderer::new(&window, false));

    loop {
        let event_option = window.poll_event();
        if event_option.is_none() {
            controller.iteration();
            continue;
        }

        match event_option.unwrap() {
            Event::Close => {
                break;
            }

            Event::Resize(size) => {
                controller.resize(&size);
            }

            Event::Input(key, value) => {
                controller.input(key, value);
            }
        }
    }
}
