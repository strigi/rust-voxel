use std::fmt::{Display, Formatter};
use std::fs;
use log::debug;
use wgpu::{BindGroup, BindGroupLayout, BlendState, Buffer, BufferAddress, BufferUsages, ColorTargetState, ColorWrites, CompareFunction, DepthBiasState, DepthStencilState, Device, Face, FragmentState, FrontFace, Label, MultisampleState, PipelineLayoutDescriptor, PolygonMode, PrimitiveState, PrimitiveTopology, Queue, RenderPipeline, RenderPipelineDescriptor, ShaderModule, ShaderModuleDescriptor, ShaderSource, StencilState, TextureFormat, VERTEX_STRIDE_ALIGNMENT, VertexAttribute, VertexBufferLayout, VertexFormat, VertexState, VertexStepMode};
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use crate::image::Image;

use crate::math::{Color, Dimensions2f, Matrix4, Point2, Point3, Tuple2f, Vector3};

use super::{Model, ModelBase};
use super::Vertex;

pub struct PlaneMesh {
    size: Dimensions2f,
    model: ModelBase,
    vertices: Vec<Vertex>,
}

impl PlaneMesh {
    pub fn new(size: Dimensions2f) -> Self {
        let half_size = size.div_scalar(2.0);

        let top_left = Vertex::new(Point3::xyz(-half_size.width(), half_size.height(), 0.0), Color::red(), Point2::xy(0.0, 1.0));
        let bottom_left = Vertex::new(Point3::xyz(-half_size.width(), -half_size.height(), 0.0), Color::green(), Point2::xy(0.0, 0.0));
        let bottom_right = Vertex::new(Point3::xyz(half_size.width(), -half_size.height(), 0.0), Color::blue(), Point2::xy(1.0, 0.0));
        let top_right = Vertex::new(Point3::xyz(half_size.width(), half_size.height(), 0.0), Color::cyan(), Point2::xy(1.0, 1.0));

        let mut model = ModelBase::new(vec![0, 1, 2, 3], Some(Image::read("../resources/stone.bmp")));
        model.set_shader_source(fs::read_to_string("../../../resources/voxels.wgsl").expect("Unable to load shader").as_str());

        return Self {
            vertices: vec!(bottom_left, bottom_right, top_left, top_right),
            size,
            model,
        }
    }

    fn model_ref(&self) -> &ModelBase {
        return &self.model;
    }

    fn model_mut(&mut self) -> &mut ModelBase {
        return &mut self.model
    }

    pub(crate) fn vertices_ref(&self) -> &[Vertex] {
        return self.vertices.as_slice();
    }
}

impl Model for PlaneMesh {
    fn id(&self) -> u32 {
        return self.model_ref().id();
    }

    fn add_tag(&mut self, tag_name: &str) -> bool {
        return self.model.add_tag(tag_name);
    }

    fn tags(&self) -> Vec<&str> {
        return self.model.tags();
    }

    fn set_label(&mut self, label: &str) {
        self.model.set_label(label);
    }

    fn label(&self) -> Option<&str> {
        return self.model.label();
    }

    fn translate_by(&mut self, amount: &Vector3) {
        return self.model_mut().translate_by(amount);
    }

    fn translate_to(&mut self, new_position: &Point3) {
        self.model_mut().translate_to(new_position);
    }

    fn rotate_by(&mut self, amount: &Vector3) {
        self.model_mut().rotate_by(amount);
    }

    fn rotate_to(&mut self, new_orientation: &Vector3) {
        self.model_mut().rotate_to(new_orientation);
    }

    fn create_model_matrix(&self) -> Matrix4 {
        return self.model_ref().create_model_matrix();
    }

    fn indices_ref(&self) -> &[u32] {
        return self.model_ref().indices_ref();
    }

    fn shader_source_ref(&self) -> &str {
        return self.model_ref().shader_source_ref();
    }

    fn texture_ref(&self) -> Option<&Image> {
        return self.model.texture_ref();
    }

    fn create_vertex_buffers(&self, device: &Device) -> Vec<(VertexBufferLayout, Buffer)> {
        let mut vertex_buffers = Vec::new();

        let mut positions: Vec<u8> = Vec::new();
        let mut colors: Vec<u8> = Vec::new();
        let mut texture_coordinates: Vec<u8> = Vec::new();
        for vertex in self.vertices_ref() {
            positions.extend_from_slice(vertex.position_ref().bytes());
            colors.extend_from_slice(vertex.color_ref().bytes());
            texture_coordinates.extend_from_slice(vertex.texture_coordinates().bytes());
        }

        // Create vertex position buffer and layout. Each position takes 3 * f32 bytes.
        let position_buffer_layout = VertexBufferLayout {
            array_stride: Point3::BYTES as u64,
            step_mode: VertexStepMode::Vertex,
            attributes: &[
                VertexAttribute { offset: 0, shader_location: 10, format: VertexFormat::Float32x3 },
            ],
        };
        let position_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some(format!("PlaneMesh::{}::Buffer::vertex::positions", self.id()).as_str()),
            usage: BufferUsages::VERTEX,
            contents: positions.as_slice(),
        });
        vertex_buffers.push((position_buffer_layout, position_buffer));

        // Create vertex color buffer and layout. Each color takes 4 * f32 bytes.
        let color_buffer_layout = VertexBufferLayout {
            array_stride: Color::BYTES as u64,
            step_mode: VertexStepMode::Vertex,
            attributes: &[
                VertexAttribute { offset: 0, shader_location: 15, format: VertexFormat::Float32x4 },
            ],
        };
        let color_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some(format!("PlaneMesh::{}::Buffer::vertex::colors", self.id()).as_str()),
            usage: BufferUsages::VERTEX,
            contents: colors.as_slice(),
        });
        vertex_buffers.push((color_buffer_layout, color_buffer));

        // Create vertex texture coordinates buffer and layout. Each color takes 2 * f32 bytes.
        let texture_coordinates_buffer_layout = VertexBufferLayout {
            array_stride: Point2::BYTES as u64,
            step_mode: VertexStepMode::Vertex,
            attributes: &[
                VertexAttribute { offset: 0, shader_location: 20, format: VertexFormat::Float32x2 },
            ],
        };
        let texture_coordinates_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some(format!("PlaneMesh::{}::Buffer::vertex::texture coordinates", self.id()).as_str()),
            usage: BufferUsages::VERTEX,
            contents: texture_coordinates.as_slice(),
        });
        vertex_buffers.push((texture_coordinates_buffer_layout, texture_coordinates_buffer));

        return vertex_buffers;
    }

    fn create_index_buffer(&self, device: &Device) -> Buffer {
        return self.model.create_index_buffer(device);
    }

    fn create_instance_buffers(&self, device: &Device) -> (u32, Vec<(VertexBufferLayout, Buffer)>) {
        let instance_count = 3u32;
        let mut instance_buffers = Vec::with_capacity(2);

        // Create color instance buffer and layout. Each instance color takes 4 * f32 bytes.
        let color_buffer_layout = VertexBufferLayout {
            array_stride: Color::BYTES as u64,
            step_mode: VertexStepMode::Instance,
            attributes: &[
                VertexAttribute { offset: 0, shader_location: 25, format: VertexFormat::Float32x4 },
            ],
        };
        let mut instance_colors = Vec::with_capacity(instance_count as usize);
        instance_colors.extend(Color::red().bytes());
        instance_colors.extend(Color::green().bytes());
        instance_colors.extend(Color::blue().bytes());
        let instance_color_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some(format!("PlaneMesh::{}::Buffer::colors", self.id()).as_str()),
            contents: instance_colors.as_slice(),
            usage: BufferUsages::VERTEX,
        });
        instance_buffers.push((color_buffer_layout, instance_color_buffer));

        // Create offset instance buffer and layout. Each instance offset takes 1 * u8 bytes, but the minimum alignment is [`VERTEX_STRIDE_ALIGNMENT`]
        let offset_buffer_layout = VertexBufferLayout {
            array_stride: VERTEX_STRIDE_ALIGNMENT.max(std::mem::size_of::<u8>() as u64),
            step_mode: VertexStepMode::Instance,
            attributes: &[
                VertexAttribute { offset: 0, shader_location: 30, format: VertexFormat::Float32 },
            ],
        };
        let mut instance_offsets = Vec::with_capacity(instance_count as usize);
        instance_offsets.extend((-30f32).to_ne_bytes());
        instance_offsets.extend(0f32.to_ne_bytes());
        instance_offsets.extend(30f32.to_ne_bytes());
        let instance_offsets_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some(format!("PlaneMesh::{}::Buffer::offsets", self.id()).as_str()),
            contents: instance_offsets.as_slice(),
            usage: BufferUsages::VERTEX,
        });
        instance_buffers.push((offset_buffer_layout, instance_offsets_buffer));

        return (instance_count, instance_buffers);
    }
}
