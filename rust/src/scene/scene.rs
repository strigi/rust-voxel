use super::model::Model;
use super::camera::Camera;

pub struct Scene {
    camera: Camera,
    models: Vec<Box<dyn Model>>,
}

impl Scene {
    pub fn new(camera: Camera) -> Self {
        let models = Vec::new();

        return Self {
            camera,
            models,
        }
    }

    pub fn models_ref(&self) -> &[Box<dyn Model>] {
        return self.models.as_slice()
    }

    pub fn models_mut(&mut self) -> &mut [Box<dyn Model>] {
        return self.models.as_mut_slice()
    }

    pub fn add_model(&mut self, model: Box<dyn Model>) {
        self.models.push(model);
    }

    pub fn model_by_class_ref(&self, class_name: &str) -> &dyn Model {
        return self.models.iter().find(|model| model.has_tag(class_name)).expect(format!("Model with class '{class_name}' does not exist").as_str()).as_ref();
    }

    pub fn model_by_class_mut(&mut self, class_name: &str) -> &mut dyn Model {
        return self.models.iter_mut().find(|model| model.has_tag(class_name)).expect(format!("Model with class '{class_name}' does not exist").as_str()).as_mut();
    }

    pub fn camera_ref(&self) -> &Camera {
        return &self.camera
    }

    pub fn camera_mut(&mut self) -> &mut Camera {
        return &mut self.camera
    }

    /// Replaces the current camera with a new one.
    pub fn set_camera(&mut self, camera: Camera) {
        self.camera = camera;
    }
}
