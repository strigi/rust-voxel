use crate::math::{Color, Point2, Vector3};

#[derive(Debug)]
pub struct Vertex {
    position: Vector3,
    color: Color,
    texture_coordinates: Point2
}

impl Vertex {
    pub fn new(position: Vector3, color: Color, texture_coordinates: Point2) -> Self {
        return Self {
            position,
            color,
            texture_coordinates,
        }
    }

    pub fn position_ref(&self) -> &Vector3 {
        return &self.position;
    }

    pub fn color_ref(&self) -> &Color {
        return &self.color;
    }

    pub fn texture_coordinates(&self) -> &Point2 {
        return &self.texture_coordinates;
    }
}
