use std::ops::Div;
use crate::math::Color;

#[derive(Copy, Clone)]
pub struct Voxel {
    pub material: Material,
}

impl Voxel {
    /// This special voxel is considered to be empty space, vacuum or null.
    /// This special voxel is considered to be empty space, vacuum or null.
    /// It is considered to be a non solid block and can be used (implicitly) to mark the boundaries of a voxel grid.
    /// @see get_or_void
    pub const VOID: Voxel = Voxel { material: Material::Void };
    pub const ROCK: Voxel = Voxel { material: Material::Rock };
    pub const SAND: Voxel = Voxel { material: Material::Sand };
    pub const WATER: Voxel = Voxel { material: Material::Water };
    pub const AIR: Voxel = Voxel { material: Material::Air };
    pub const DIRT: Voxel = Voxel { material: Material::Dirt };
    pub const VEGETATION: Voxel = Voxel { material: Material::Vegetation };
}

#[derive(Copy, Clone, Debug)]
pub enum Material {
    Air,
    Rock,
    Sand,
    Water,
    Void,
    Dirt,
    Vegetation,
}

impl Material {
    pub fn color(&self) -> Color {
        match self {
            Self::Void => Color::black(),
            Self::Air => Color::cyan(),
            Self::Rock => Color::gray(),
            Self::Sand => Color::yellow(),
            Self::Water => Color::blue(),
            Self::Dirt => Color::rgb(0.30, 0.17, 0.03),
            Self::Vegetation => Color::green().div(4.0)
        }
    }

    pub fn solid(&self) -> bool {
        match self {
            Self::Void => false,
            Self::Air => false,
            Self::Rock => true,
            Self::Sand => true,
            Self::Water => true,
            Self::Dirt => true,
            Self::Vegetation => true,
        }
    }

    pub fn empty(&self) -> bool {
        return !self.solid();
    }
}