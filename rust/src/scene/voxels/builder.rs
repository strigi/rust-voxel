use std::io;
use std::io::Write;
use std::time::SystemTime;
use rand::{Rng, thread_rng};

use crate::image::Image;
use crate::math::{Dimensions3f, Dimensions3u, Point3, Vector3};
use crate::scene::voxels::voxel::Material;
use super::voxel_mesh::VoxelMesh;
use super::voxel::Voxel;

pub trait VoxelMeshBuilder {
    fn get_voxel(&self, x: u32, y: u32, z: u32, mesh: &VoxelMesh) -> Voxel;

    fn build(&self, voxel_size: f32, resolution: Dimensions3u) -> VoxelMesh {
        let mut mesh = VoxelMesh::new(voxel_size, resolution.clone());
        log::info!("Populating voxel mesh...");
        for z in 0..mesh.voxel_resolution.depth() {
            for y in 0..mesh.voxel_resolution.height() {
                for x in 0..mesh.voxel_resolution.width() {
                    let voxel = self.get_voxel(x, y, z, &mesh);
                    mesh.set(x, y, z, voxel);
                }
            }
        }

        return mesh;
    }
}

pub struct HeightmapImageVoxelBuilder {
    image: Image,
    inverted: bool,
}

impl HeightmapImageVoxelBuilder {
    pub fn from_heightmap_image(image_path: &str) -> Self {
        let image = Image::read(image_path);

        return Self {
            image,
            inverted: false,
        };
    }

    pub fn invert(mut self) -> Self {
        self.inverted = !self.inverted;
        return self;
    }
}

// TODO: currently DEPTH is the "height" due to the orientation of the axes
//   maybe I should change the handedness so that Z becomes the height and Y becomes the depth
impl VoxelMeshBuilder for HeightmapImageVoxelBuilder {
    fn get_voxel(&self, x: u32, y: u32, z: u32, mesh: &VoxelMesh) -> Voxel {
        let x_ratio = x as f32 / (mesh.voxel_resolution.width() - 1) as f32;
        let x_image = ((self.image.size_ref().width() -1) as f32 * x_ratio).round() as u32;

        let z_ratio = z as f32 / (mesh.voxel_resolution.depth() - 1) as f32;
        let y_image = ((self.image.size_ref().height() - 1) as f32 * z_ratio).round() as u32;

        // By default, black is "lowest" and white is "highest" point, but if you invert, then white will be treated as low.
        let mut color = self.image.get_color_at(x_image, y_image).clone();
        if self.inverted {
            color = color.negative();
        }

        let height = ((color.r() + color.g() + color.b()) / 3.0) * 1.0;
        let height_voxels = ((mesh.voxel_resolution.height() - 1) as f32 * height).round() as u32;
        let mut rng = thread_rng();
        if y > height_voxels {
            return Voxel::AIR;
        }

        if height_voxels <= 1 {
            return Voxel::WATER;
        }

        if height <= 0.25 {
            if rng.gen_bool((height * 10.0).min(1.0) as f64) {
                if rng.gen_bool((0.2 * height).min(1.0) as f64) {
                    return Voxel::DIRT
                } else {
                    return Voxel::VEGETATION;
                }
            } else {
                return Voxel::SAND
            }
        }

        let rocky = rng.gen_bool((height * 0.5).min(1.0) as f64);
        if rocky {
            return Voxel::ROCK
        } else {
            if rng.gen_bool((height * 0.3).min(1.0) as f64) {
                return Voxel::ROCK
            } else {
                return Voxel::VEGETATION
            }
        }
    }
}

pub struct SphereVoxelBuilder {
    radius: f32,
}

impl SphereVoxelBuilder {
    pub fn with_radius(radius: f32) -> Self {
        println!("Creating sphere voxel builder with radius {}", radius);
        return Self {
            radius,
        };
    }
}

impl VoxelMeshBuilder for SphereVoxelBuilder {
    fn get_voxel(&self, x: u32, y: u32, z: u32, mesh: &VoxelMesh) -> Voxel {
        let half_resolution = mesh.voxel_resolution.as_f().div(2.0);
        let half_block_size = mesh.block_size / 2.0;

        let mesh_center = Point3::null();

        let a = (x as f32 - half_resolution.width()) * mesh.block_size + half_block_size;
        let b = (y as f32 - half_resolution.height()) * mesh.block_size + half_block_size;
        let c = (z as f32 - half_resolution.depth()) * mesh.block_size + half_block_size;
        let block_center = Point3::xyz(a, b, c);

        let distance_from_cell_center_to_mesh_center = Vector3::between(&mesh_center, &block_center).length();
        return if distance_from_cell_center_to_mesh_center <= self.radius {
            Voxel { material: Material::Rock }
        } else {
            Voxel { material: Material::Void }
        };
    }
}
