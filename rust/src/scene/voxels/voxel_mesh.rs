use std::fmt::{Display, Formatter, Result};
use std::{fs};
use wgpu::{BindGroup, BindGroupDescriptor, BindGroupEntry, BindGroupLayout, BindGroupLayoutDescriptor, BindGroupLayoutEntry, BindingType, Buffer, BufferBindingType, BufferUsages, Device, ShaderStages, VertexAttribute, VertexBufferLayout, VertexFormat, VertexStepMode};
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use crate::image::Image;
use crate::math::{Color, Dimensions3f, Matrix4, Point2, Point3, Vector3};
use crate::math::Dimensions3u;

use super::super::model::{ModelBase, Model};
use super::voxel::Voxel;

pub struct VoxelMesh {
    pub(super) voxel_resolution: Dimensions3u,
    pub(super) vertex_resolution: Dimensions3u,
    pub(super) mesh_size: Dimensions3f,
    pub(super) block_size: f32,
    pub(super) voxel_count: u32,
    pub(super) voxels: Vec<Voxel>,
    model: ModelBase,
}

pub enum FaceOrientation {
    Left = 0,
    Right = 1,
    Bottom = 2,
    Top = 3,
    Near = 4,
    Far = 5,
}

impl VoxelMesh {
    pub fn new(block_size: f32, resolution: Dimensions3u) -> Self {
        let block_count = resolution.volume();
        let mesh_size = resolution.as_f().mul(block_size);
        let vertex_resolution = resolution.add(1);

        log::debug!("Creating voxel mesh of resolution={} block_count={} block_size={} mesh_size={}", resolution, block_count, block_size, mesh_size);

        let mut voxels = Vec::with_capacity(block_count as usize);
        for _ in 0..block_count {
            voxels.push(Voxel::VOID);
        }

        let mut model = ModelBase::empty();
        model.set_shader_source(fs::read_to_string("../resources/voxels.wgsl").unwrap().as_str());
        model.set_texture(Image::read("../resources/stone.bmp"));

        return Self {
            vertex_resolution,
            voxel_resolution: resolution,
            mesh_size,
            block_size,
            voxel_count: block_count,
            voxels,
            model,
        }
    }

    pub(super) fn model_ref(&self) -> &ModelBase {
        return &self.model;
    }

    pub fn set_shader_source(&mut self, source: &str) {
        self.model.set_shader_source(source);
    }

    pub(super) fn model_mut(&mut self) -> &mut ModelBase {
        return &mut self.model;
    }

    // TODO add all the other corners too, and the center (which is zero?).
    pub fn left_bottom_near(&self) -> Point3 {
        let half_mesh_size = self.mesh_size.div(2.0);
        return Point3::xyz(-half_mesh_size.width(), -half_mesh_size.height(), -half_mesh_size.depth());
    }

    /// The voxels are stored in a contiguous array, so the x y z coordinates have to be calculated as offsets into this array.
    /// Indices start from the left bottom near corner and increase over x first, then y and finally z.
    pub(super) fn voxel_index(&self, x: u32, y: u32, z: u32) -> u32 {
        return Self::index(x, y, z, &self.voxel_resolution);
    }

    pub(super) fn vertex_index(&self, x: u32, y: u32, z: u32) -> u32 {
        return Self::index(x, y, z, &self.vertex_resolution);
    }

    fn index(x: u32, y: u32, z: u32, limits: &Dimensions3u) -> u32 {
        assert!(x < limits.width(), "x coordinate out of bounds");
        assert!(y < limits.height(), "y coordinate out of bounds");
        assert!(y < limits.depth(), "z coordinate out of bounds");
        return x + y * limits.width() + z * limits.width() * limits.height();
    }

    pub fn get(&self, x: u32, y: u32, z: u32) -> &Voxel {
        return &self.voxels_ref()[self.voxel_index(x, y, z) as usize];
    }

    fn get_convolution(&self, x: u32, y: u32, z: u32) -> VoxelConvolution {
        let center = self.get(x, y, z);

        let left = if x == 0 {
            &Voxel::VOID
        } else {
            self.get(x - 1, y, z)
        };

        let right = if x == self.voxel_resolution.width() - 1 {
            &Voxel::VOID
        } else {
            self.get(x + 1, y, z)
        };

        let bottom = if y == 0 {
            &Voxel::VOID
        } else {
            self.get(x, y - 1, z)
        };

        let top = if y == self.voxel_resolution.height() - 1 {
            &Voxel::VOID
        } else {
            self.get(x, y + 1, z)
        };

        let near = if z == 0 {
            &Voxel::VOID
        } else {
            self.get(x, y, z - 1)
        };

        let far = if z == self.voxel_resolution.depth() - 1 {
            &Voxel::VOID
        } else {
            self.get(x, y, z + 1)
        };

        return VoxelConvolution { left, right, bottom, top, near, far, center };
    }

    pub fn set(&mut self, x: u32, y: u32, z: u32, voxel: Voxel) {
        let index = self.voxel_index(x, y, z);
        self.voxels_mut()[index as usize] = voxel;
    }

    fn voxels_ref(&self) -> &[Voxel] {
        return self.voxels.as_slice();
    }

    fn voxels_mut(&mut self) -> &mut [Voxel] {
        return self.voxels.as_mut_slice();
    }
}

impl Model for VoxelMesh {
    fn id(&self) -> u32 {
        return self.model_ref().id();
    }

    fn add_tag(&mut self, tag_name: &str) -> bool {
        return self.model.add_tag(tag_name);
    }

    fn tags(&self) -> Vec<&str> {
        return self.model.tags();
    }

    fn set_label(&mut self, label: &str) {
        self.model.set_label(label);
    }

    fn label(&self) -> Option<&str> {
        return self.model.label();
    }

    fn translate_by(&mut self, amount: &Vector3) {
        return self.model_mut().translate_by(amount);
    }

    fn translate_to(&mut self, new_position: &Point3) {
        self.model_mut().translate_to(new_position);
    }

    fn rotate_by(&mut self, amount: &Vector3) {
        self.model_mut().rotate_by(amount);
    }

    fn rotate_to(&mut self, new_orientation: &Vector3) {
        self.model_mut().rotate_to(new_orientation);
    }

    fn create_model_matrix(&self) -> Matrix4 {
        return self.model_ref().create_model_matrix();
    }

    fn indices_ref(&self) -> &[u32] {
        return &[0, 1, 2, 3];
    }

    fn shader_source_ref(&self) -> &str {
        return self.model_ref().shader_source_ref();
    }

    fn texture_ref(&self) -> Option<&Image> {
        return self.model.texture_ref();
    }

    /// Defines a Quad representing a any of a single voxel's faces.
    /// Instancing will be used to draw it multiple times and position and rotate it correctly.
    fn create_vertex_buffers(&self, device: &Device) -> Vec<(VertexBufferLayout, Buffer)> {
        let vertex_count = 4;

        let positions_buffer_layout = VertexBufferLayout {
            array_stride: Point3::BYTES as u64,
            step_mode: VertexStepMode::Vertex,
            attributes: &[VertexAttribute {
                format: VertexFormat::Float32x3,
                offset: 0,
                shader_location: ModelBase::POSITION_VERTEX_SHADER_LOCATION,
            }],
        };

        let half_block_size = self.block_size / 2.0;

        let mut positions = Vec::with_capacity(vertex_count);
        positions.extend_from_slice(Point3::xyz(-half_block_size, -half_block_size, 0.0).bytes());  // left-bottom
        positions.extend_from_slice(Point3::xyz(half_block_size, -half_block_size, 0.0).bytes());   // right-bottom
        positions.extend_from_slice(Point3::xyz(-half_block_size, half_block_size, 0.0).bytes());   // left-top
        positions.extend_from_slice(Point3::xyz(half_block_size, half_block_size, 0.0).bytes());    // right-top
        let positions_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("VoxelMesh::{}::Buffer::positions"),
            usage: BufferUsages::VERTEX,
            contents: positions.as_slice(),
        });

        let texture_coordinates_buffer_layout = VertexBufferLayout {
            array_stride: Point2::BYTES as u64,
            step_mode: VertexStepMode::Vertex,
            attributes: &[VertexAttribute {
                format: VertexFormat::Float32x2,
                offset: 0,
                shader_location: ModelBase::TEXTURE_COORDINATES_VERTEX_SHADER_LOCATION,
            }],
        };
        let mut texture_coordinates = Vec::with_capacity(vertex_count);
        texture_coordinates.extend_from_slice(Point2::xy(0.0, 0.0).bytes());    // left-bottom
        texture_coordinates.extend_from_slice(Point2::xy(1.0, 0.0).bytes());    // right-bottom
        texture_coordinates.extend_from_slice(Point2::xy(0.0, 1.0).bytes());    // left-top
        texture_coordinates.extend_from_slice(Point2::xy(1.0, 1.0).bytes());    // right-top
        let texture_coordinates_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("VoxelMesh::{}::Buffer::texture coordinates"),
            usage: BufferUsages::VERTEX,
            contents: texture_coordinates.as_slice(),
        });

        return vec![
            (positions_buffer_layout, positions_buffer),
            (texture_coordinates_buffer_layout, texture_coordinates_buffer),
        ];
    }

    fn create_instance_buffers(&self, device: &Device) -> (u32, Vec<(VertexBufferLayout, Buffer)>) {
        let color_buffer_layout = VertexBufferLayout {
            array_stride: Color::BYTES as u64,
            step_mode: VertexStepMode::Instance,
            attributes: &[VertexAttribute {
                format: VertexFormat::Float32x4,
                offset: 0,
                shader_location: ModelBase::INSTANCE_SHADER_LOCATION_BASE + 0,
            }],
        };
        let orientation_buffer_layout = VertexBufferLayout {
            array_stride: std::mem::size_of::<u32>() as u64,
            step_mode: VertexStepMode::Instance,
            attributes: &[VertexAttribute {
                format: VertexFormat::Uint32,
                offset: 0,
                shader_location: ModelBase::INSTANCE_SHADER_LOCATION_BASE + 1,
            }],
        };
        let indexes_buffer_layout = VertexBufferLayout {
            array_stride: std::mem::size_of::<u32>() as u64,
            step_mode: VertexStepMode::Instance,
            attributes: &[VertexAttribute {
                format: VertexFormat::Uint32,
                offset: 0,
                shader_location: ModelBase::INSTANCE_SHADER_LOCATION_BASE + 2,
            }]
        };

        let mut instance_count = 0;
        let mut quad_colors = Vec::with_capacity(instance_count);
        let mut quad_orientations = Vec::with_capacity(instance_count);
        let mut voxel_indices = Vec::with_capacity(instance_count);
        for z in 0..self.voxel_resolution.depth() {
            for y in 0..self.voxel_resolution.height() {
                for x in 0..self.voxel_resolution.width() {
                    let convolution = self.get_convolution(x, y, z);
                    if convolution.center.material.empty() {
                        continue;
                    }

                    let voxel_index = self.voxel_index(x, y, z);


                    if convolution.left.material.empty() {
                        quad_colors.extend_from_slice(convolution.center.material.color().bytes());
                        quad_orientations.extend_from_slice((FaceOrientation::Left as u32).to_ne_bytes().as_ref());
                        voxel_indices.extend_from_slice(voxel_index.to_ne_bytes().as_ref());
                        instance_count += 1;
                    }
                    if convolution.right.material.empty() {
                        quad_colors.extend_from_slice(convolution.center.material.color().bytes());
                        quad_orientations.extend_from_slice((FaceOrientation::Right as u32).to_ne_bytes().as_ref());
                        voxel_indices.extend_from_slice(voxel_index.to_ne_bytes().as_ref());
                        instance_count += 1;
                    }
                    if convolution.bottom.material.empty() {
                        quad_colors.extend_from_slice(convolution.center.material.color().bytes());
                        quad_orientations.extend_from_slice((FaceOrientation::Bottom as u32).to_ne_bytes().as_ref());
                        voxel_indices.extend_from_slice(voxel_index.to_ne_bytes().as_ref());
                        instance_count += 1;
                    }
                    if convolution.top.material.empty() {
                        quad_colors.extend_from_slice(convolution.center.material.color().bytes());
                        quad_orientations.extend_from_slice((FaceOrientation::Top as u32).to_ne_bytes().as_ref());
                        voxel_indices.extend_from_slice(voxel_index.to_ne_bytes().as_ref());
                        instance_count += 1;
                    }
                    if convolution.near.material.empty() {
                        quad_colors.extend_from_slice(convolution.center.material.color().bytes());
                        quad_orientations.extend_from_slice((FaceOrientation::Near as u32).to_ne_bytes().as_ref());
                        voxel_indices.extend_from_slice(voxel_index.to_ne_bytes().as_ref());
                        instance_count += 1;
                    }
                    if convolution.far.material.empty() {
                        quad_colors.extend_from_slice(convolution.center.material.color().bytes());
                        quad_orientations.extend_from_slice((FaceOrientation::Far as u32).to_ne_bytes().as_ref());
                        voxel_indices.extend_from_slice(voxel_index.to_ne_bytes().as_ref());
                        instance_count += 1;
                    }
                }
            }
        }

        let color_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some(format!("VoxelMesh::{}::Buffer::instance colors", self.id()).as_str()),
            usage: BufferUsages::VERTEX,
            contents: quad_colors.as_slice(),
        });
        let orientation_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some(format!("VoxelMesh::{}::Buffer::instance orientations", self.id()).as_str()),
            usage: BufferUsages::VERTEX,
            contents: quad_orientations.as_slice(),
        });
        let voxel_indices_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some(format!("VoxelMesh::{}::Buffer::instance indexes", self.id()).as_str()),
            usage: BufferUsages::VERTEX,
            contents: voxel_indices.as_slice(),
        });

        log::info!("Created instance buffers for {} which has {} quads with information: {} bytes orientation, {} bytes colors and {} voxel indices", self, instance_count, quad_orientations.len(), quad_colors.len(), voxel_indices.len());

        return (instance_count as u32, vec![
            (color_buffer_layout, color_buffer),
            (orientation_buffer_layout, orientation_buffer),
            (indexes_buffer_layout, voxel_indices_buffer),
        ]);
    }

    fn create_uniforms(&self, device: &Device) -> Option<(BindGroupLayout, BindGroup, Vec<Buffer>)> {
        let resolution_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some(format!("Model::{}::Buffer::Uniform::resolution", self.id()).as_str()),
            contents: self.voxel_resolution.bytes(),
            usage: BufferUsages::UNIFORM,
        });

        let block_size_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some(format!("Model::{}::Buffer::Uniform::block_size", self.id()).as_str()),
            contents: &self.block_size.to_ne_bytes(),
            usage: BufferUsages::UNIFORM,
        });

        let bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: Some(format!("Model::{}::BindGroupLayout::Uniform", self.id()).as_str()),
            entries: &[BindGroupLayoutEntry {
                binding: ModelBase::MODEL_UNIFORMS_BINDING_BASE.binding + 0,
                visibility: ShaderStages::VERTEX,
                ty: BindingType::Buffer {
                    ty: BufferBindingType::Uniform,
                    has_dynamic_offset: false,
                    min_binding_size: None,
                },
                count: None,
            }, BindGroupLayoutEntry {
                binding: ModelBase::MODEL_UNIFORMS_BINDING_BASE.binding + 1,
                visibility: ShaderStages::VERTEX,
                ty: BindingType::Buffer {
                    ty: BufferBindingType::Uniform,
                    has_dynamic_offset: false,
                    min_binding_size: None,
                },
                count: None,
            }],
        });

        let bind_group = device.create_bind_group(&BindGroupDescriptor {
            label: Some(format!("Model::{}::BindGroup::{}::Uniform", self.id(), ModelBase::MODEL_UNIFORMS_BINDING_BASE.group).as_str()),
            layout: &bind_group_layout,
            entries: &[BindGroupEntry {
                binding: ModelBase::MODEL_UNIFORMS_BINDING_BASE.binding + 0,
                resource: resolution_buffer.as_entire_binding(),
            }, BindGroupEntry {
                binding: ModelBase::MODEL_UNIFORMS_BINDING_BASE.binding + 1,
                resource: block_size_buffer.as_entire_binding(),
            }],
        });

        return Some((bind_group_layout, bind_group, vec![resolution_buffer, block_size_buffer]));
    }
}

impl Display for VoxelMesh {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        return write!(f, "VoxelMesh id={}, label={:?}, tags={:?}", self.model.id(), self.model.label(), self.model.tags());
    }
}

struct VoxelConvolution<'a> {
    left: &'a Voxel,
    right: &'a Voxel,
    bottom: &'a Voxel,
    top: &'a Voxel,
    near: &'a Voxel,
    far: &'a Voxel,
    center: &'a Voxel
}
