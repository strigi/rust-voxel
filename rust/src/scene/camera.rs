use std::fmt::{Debug, Formatter};
use crate::image::Image;
use crate::math::{Matrix4, Point3, Vector3};
use crate::scene::{Model, ModelBase};
use crate::math::Dimensions2f;

/// https://www.youtube.com/watch?v=U0_ONQQ5ZNM
/// https://www.youtube.com/watch?v=EqNcqBdrNyI
/// https://jsantell.com/model-view-projection/
pub struct Camera {
    near: f32,
    far: f32,
    top: f32,
    bottom: f32,
    left: f32,
    right: f32,

    model: ModelBase
}

impl Camera {
    /// Creates a new camera that faces into the screen (along the +Z direction) and has its view plane parallel with the X-Y plane.
    pub fn new(film_size: Dimensions2f) -> Self {
        let half_width = film_size.width() / 2f32;
        let half_height = film_size.height() / 2f32;

        let left = -half_width;
        let right = half_width;
        let bottom = -half_height;
        let top = half_height;

        // This produces the most "natural" FOV https://complexelepheonix.com/nl/welke-lensbrandpuntsafstand-lijkt-het-meest-op-het-perspectief-van-het-menselijk-oog/
        let near = film_size.diagonal();

        // Does not have impact on the FOV
        // TODO: value arbitrarily chosen
        let far = near + 10f32;

        // Position camera initially so that the origin (0, 0, 0) is precisely in the center of the view frustum
        let z = (near + far) / -2.0;

        let mut model = ModelBase::empty();
        model.add_tag("camera");
        model.translate_to(&Vector3::xyz(0.0, 0.0, z));

        let camera = Self {
            near,
            far,
            top,
            bottom,
            left,
            right,
            model,
        };
        return camera;
    }

    pub fn width(&self) -> f32 {
        return self.right - self.left;
    }

    pub fn height(&self) -> f32 {
        return self.top - self.bottom;
    }

    pub fn depth(&self) -> f32 {
        return self.far - self.near;
    }

    pub fn aspect_ratio(&self) -> f32 {
        return self.width() / self.height();
    }

    /// Experiment: Adding to the the near plane will have the effect of reducing the FOV appear ("zoomed in")
    /// Near must always be < far and > 0, if the new value would violate that, the change is ignored.
    // TODO: should be replaced by set_fov I think
    pub fn add_near(&mut self, value: f32) {
        let old_near = self.near;
        let new_near = old_near + value;

        if new_near <= 0.0 || new_near >= self.far {
            return;
        }

        println!("Changing near plane from {old_near:.3} to {new_near:.3}, which means the FOV has become {:.3}°/{:.3}°", self.fov_x(), self.fov_y());
        self.near = new_near;
    }

    pub fn fov_x(&self) -> f32 {
        return 2f32 * (self.width() / (self.near * 2f32)).atan().to_degrees();
    }

    pub fn fov_y(&self) -> f32 {
        return 2f32 * (self.height() / (self.near * 2f32)).atan().to_degrees();
    }

    /// Sets the aspect ratio (defined as width / height) for the camera's projection plane (aka "near"-plan, or "film sensor") to have.
    /// It will modify the sensor's height and keep the width as is.
    pub fn set_aspect_ratio(&mut self, aspect_ratio: f32) {
        let new_height = self.width() / aspect_ratio;

        println!("Changing camera aspect_ratio to {}, which results in a new height: {} (width is not modified)", aspect_ratio, new_height);

        // Currently the camera is exactly centered around the Z-axis
        // This is often sufficient, but for cases where a render needs to be separated into multiple sub-renderers (as in, the image is very large and needs to be rendered in parallel by several GPUs)
        // this is often achieved by creating a projection plane that is centered around the Z-axis.
        self.top = new_height / 2f32;
        self.bottom = -new_height / 2f32;
    }

    pub fn create_view_matrix(&self) -> Matrix4 {
        // Move the entire world so that the camera is at (0, 0, 0), this is the same as moving all points opposite to the position of the camera, hence the negative values
        // This is what is commonly called going from "world space" to "camera space"
        let translate = Matrix4::translate_xyz(
            -self.model.position_ref().x(),
            -self.model.position_ref().y(),
            -self.model.position_ref().z(),
        );

        // TODO: Rotate the entire world so that the camera is looking down the Z-axis perpendicular to XY-plane
        // This is what is commonly called going from "world space" to "camera space"
        // Note that this is the _rotation of the camera_ that is determined here, not the objects in the world, so depending on the xyz location of the camera, the rotating orb may look "strange"
        // because it does not rotate around the (0, 0, 0) point but around the camera's own location.
        // It also uses eurler angles which are problematic and non-untuitive
        // let mut rotate = Mat4::identity();
        // rotate = rotate.mul(&Mat4::rotate_x(-self.base.get_orientation().x()));
        // rotate = rotate.mul(&Mat4::rotate_y(-self.base.get_orientation().y()));
        // rotate = rotate.mul(&Mat4::rotate_z(-self.base.get_orientation().z()));

        // let view_matrix = rotate.mul(&translate);
        let view_matrix = translate;
        return view_matrix;
    }

    pub fn create_projection_matrix(&self) -> Matrix4 {
        // Scale the entire world so that the view box is a volume of ([-1..1], [-1..1], [0..1])
        // This results in the so called "orthographic view box", the hardware can then just "drop the z value" to project the scene onto the near plane and use that to draw the pixels on the screen
        // However, the Z value is still important, because it will be used to perform depth testing
        let orthogonal_projection = Matrix4::scale_xyz(
            2f32 / self.width(),
            2f32 / self.height(),
            1f32 / self.depth()
        );

        // Apply perspective so that objects with larger Z appear smaller
        let mut perspective_projection = Matrix4::identity();
        perspective_projection.set(0, 0, self.near);
        perspective_projection.set(1, 1, self.near);
        // TODO: Missing row (currently 0, 0, 1) which should loose depth info after perspective division (which is bad, but I have not observed any effects of it)
        perspective_projection.set(3, 2, 1f32);

        let projection_matrix = orthogonal_projection.mul(&perspective_projection);
        return projection_matrix;
    }
}

impl Model for Camera {
    fn id(&self) -> u32 {
        return self.model.id();
    }

    fn add_tag(&mut self, class_name: &str) -> bool {
        return self.model.add_tag(class_name);
    }

    fn tags(&self) -> Vec<&str> {
        return self.model.tags();
    }

    fn set_label(&mut self, label: &str) {
        return self.set_label(label);
    }

    fn label(&self) -> Option<&str> {
        return self.model.label();
    }

    fn translate_by(&mut self, amount: &Vector3) {
        self.model.translate_by(amount);
    }

    fn translate_to(&mut self, new_position: &Point3) {
        self.model.translate_to(new_position);
    }

    fn rotate_by(&mut self, amount: &Vector3) {
        self.model.rotate_by(amount);
    }

    fn rotate_to(&mut self, new_orientation: &Vector3) {
        self.model.rotate_to(new_orientation)
    }

    fn create_model_matrix(&self) -> Matrix4 {
        return self.model.create_model_matrix();
    }

    fn indices_ref(&self) -> &[u32] {
        return self.model.indices_ref();
    }

    fn shader_source_ref(&self) -> &str {
        return self.model.shader_source_ref();
    }

    fn texture_ref(&self) -> Option<&Image> {
        return self.model.texture_ref();
    }
}

impl Debug for Camera {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        return write!(f, "Camera:\n  position: {}\n  orientation: {}\n  box: left={:.3}, right={:.3} bottom={:.3} top={:.3} near={:.3} far={:.3}\n  viewing volume: width={:.3}, height={:.3}, depth={:.3}\n  aspect_ratio={:.3} (w/h), fov_x={:.3}deg, fov_y={:.3}deg",
            self.model.position_ref(),
            Vector3::null(),
            self.left, self.right, self.bottom, self.top, self.near, self.far,
            self.width(), self.height(), self.depth(),
            self.aspect_ratio(), self.fov_x(), self.fov_y()
        )
    }
}


