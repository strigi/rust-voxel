use std::fmt::{Formatter, Result};
use std::num::NonZeroU32;
use wgpu::{AddressMode, BindGroup, BindGroupDescriptor, BindGroupEntry, BindGroupLayout, BindGroupLayoutDescriptor, BindGroupLayoutEntry, BindingResource, BindingType, BlendState, Buffer, BufferUsages, ColorTargetState, ColorWrites, CompareFunction, DepthBiasState, DepthStencilState, Device, Extent3d, Face, FilterMode, FragmentState, FrontFace, ImageCopyTexture, ImageDataLayout, MultisampleState, Origin3d, PipelineLayoutDescriptor, PolygonMode, PrimitiveState, PrimitiveTopology, Queue, RenderPipeline, RenderPipelineDescriptor, SamplerBindingType, SamplerDescriptor, ShaderModule, ShaderModuleDescriptor, ShaderSource, ShaderStages, StencilState, TextureAspect, TextureDescriptor, TextureDimension, TextureFormat, TextureSampleType, TextureUsages, TextureViewDescriptor, TextureViewDimension, VertexAttribute, VertexBufferLayout, VertexFormat, VertexState, VertexStepMode};
use wgpu::util::{BufferInitDescriptor, DeviceExt};

use crate::image::Image;
use crate::math::{Color, Matrix4, Point3, Vector3};
use crate::scene::ModelBase;

pub trait Model {
    fn id(&self) -> u32;
    fn add_tag(&mut self, tag_name: &str) -> bool;
    fn tags(&self) -> Vec<&str>;

    fn has_tag(&self, class_to_search_for: &str) -> bool {
        return self.tags().iter().find(|class| {
            return **class == class_to_search_for;
        }).is_some();
    }

    fn set_label(&mut self, label: &str);
    fn label(&self) -> Option<&str>;

    fn translate_by(&mut self, amount: &Vector3);
    fn translate_to(&mut self, new_position: &Point3);
    fn rotate_by(&mut self, amount: &Vector3);
    fn rotate_to(&mut self, new_orientation: &Vector3);
    fn create_model_matrix(&self) -> Matrix4;

    fn indices_ref(&self) -> &[u32];

    fn index_count(&self) -> u32 {
        return self.indices_ref().len() as u32;
    }

    /// Returns the WGSL shader source code to use in [Self::create_render_pipeline].
    fn shader_source_ref(&self) -> &str;

    fn texture_ref(&self) -> Option<&Image>;

    fn has_texture(&self) -> bool {
        return self.texture_ref().is_some();
    }

    /// Creates a default RenderPipeline
    fn create_render_pipeline(&self, device: &Device, bind_group_layouts: &[&BindGroupLayout], texture_format: TextureFormat, vertex_buffer_layouts: &[VertexBufferLayout]) -> RenderPipeline {
        let render_pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: Some(format!("ModelBase::{}::PipelineLayout", self.id()).as_str()),
            bind_group_layouts,
            push_constant_ranges: &[],
        });

        let shader_module = self.create_shader_module(device, self.shader_source_ref());

        return device.create_render_pipeline(&RenderPipelineDescriptor {
            label: Some(format!("ModelBase::{}::RenderPipeline", self.id()).as_str()),
            layout: Some(&render_pipeline_layout),
            vertex: VertexState {
                module: &shader_module,
                entry_point: "vs_main",
                buffers: vertex_buffer_layouts,
            },
            fragment: Some(FragmentState {
                module: &shader_module,
                entry_point: "fs_main",
                targets: &[Some(ColorTargetState {
                    format: texture_format,
                    blend: Some(BlendState::REPLACE),
                    write_mask: ColorWrites::ALL,
                })],
            }),
            primitive: PrimitiveState {
                topology: PrimitiveTopology::TriangleStrip,
                strip_index_format: None,

                front_face: FrontFace::Ccw,
                cull_mode: Some(Face::Back),
                polygon_mode: PolygonMode::Fill,

                unclipped_depth: false,
                conservative: false,
            },
            depth_stencil: Some(DepthStencilState {
                format: TextureFormat::Depth32Float,
                depth_write_enabled: true,
                depth_compare: CompareFunction::Less,
                stencil: StencilState::default(),
                bias: DepthBiasState::default(),
            }),
            multisample: MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            multiview: None,
        });
    }

    fn create_vertex_buffers(&self, device: &Device) -> Vec<(VertexBufferLayout, Buffer)> {
        let mut vertices_as_bytes = Vec::new();
        vertices_as_bytes.extend_from_slice(Point3::xyz(-1.0, -1.0, 0.0).bytes());
        vertices_as_bytes.extend_from_slice(Color::red().bytes());
        vertices_as_bytes.extend_from_slice(Point3::xyz(1.0, -1.0, 0.0).bytes());
        vertices_as_bytes.extend_from_slice(Color::green().bytes());
        vertices_as_bytes.extend_from_slice(Point3::xyz(0.0, 1.0, 0.0).bytes());
        vertices_as_bytes.extend_from_slice(Color::blue().bytes());

        let buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some(format!("Model::{}::Buffer::vertices", self.id()).as_str()),
            contents: vertices_as_bytes.as_slice(),
            usage: BufferUsages::VERTEX,
        });

        let layout = VertexBufferLayout {
            array_stride: 0,
            step_mode: VertexStepMode::Vertex,
            attributes: &[VertexAttribute {
                format: VertexFormat::Float32x4,
                offset: 0,
                shader_location: ModelBase::VERTEX_SHADER_LOCATION_BASE,
            }, VertexAttribute {
                format: VertexFormat::Float32x4,
                offset: Point3::BYTES as u64,
                shader_location: ModelBase::VERTEX_SHADER_LOCATION_BASE + 1,
            }],
        };

        return vec!((layout, buffer));
    }

    /// Creates the index buffer that contains index positions into the vertex buffers.
    /// The default implementation creates an index buffer from [`indices_ref`]
    fn create_index_buffer(&self, device: &Device) -> Buffer {
        let indices = self.indices_ref();
        log::debug!("Creating default index buffer for model id={} containing {} indices", self.id(), indices.len());
        let indices_as_bytes: Vec<u8> = self.indices_ref().iter().flat_map(|x| { x.to_ne_bytes() }).collect();
        return device.create_buffer_init(&BufferInitDescriptor {
            label: Some(format!("Model::{}::Buffer::indices", self.id()).as_str()),
            usage: BufferUsages::INDEX,
            contents: indices_as_bytes.as_slice(),
        });
    }

    /// Creates vertex buffers for instance variations. The buffers returned by this method should have step mode "instance".
    /// You can add multiple instance buffers, and you can map multiple locations to each using vertex buffer attributes.
    /// By default there is a single instance and no instance specific vertex buffer.
    fn create_instance_buffers(&self, device: &Device) -> (u32, Vec<(VertexBufferLayout, Buffer)>) {
        return (1, Vec::new());
    }

    /// Creates uniform buffers for this model. Uniform buffers are useful if you want to pass once-per-model data to the shader.
    /// Currently only a single bind group is usable for the uniforms, but you can map multiple buffer bindings to this bind group
    /// The default implementation return `None`.
    fn create_uniforms(&self, device: &Device) -> Option<(BindGroupLayout, BindGroup, Vec<Buffer>)> {
        return None;
    }

    fn create_texture_bind_group_and_layout(&self, device: &Device, queue: &Queue) -> (BindGroup, BindGroupLayout) {
        log::info!("Creating texture");

        let texture_image = self.texture_ref().unwrap();
        let size = texture_image.size_ref();

        let extent = Extent3d {
            width: size.width(),
            height: size.height(),
            depth_or_array_layers: 1,
        };

        let texture = device.create_texture(&TextureDescriptor {
            label: Some(format!("ModelBase::{}::Texture", self.id()).as_str()),
            size: extent,
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: TextureFormat::Rgba8Unorm,
            usage: TextureUsages::TEXTURE_BINDING | TextureUsages::COPY_DST,
        });

        const BYTES_PER_PIXEL: u32 = 4;
        queue.write_texture(ImageCopyTexture {
            texture: &texture,
            mip_level: 0,
            origin: Origin3d::ZERO,
            aspect: TextureAspect::All,
        },
                            &texture_image.data_ref(),
                            ImageDataLayout {
                                offset: 0,
                                bytes_per_row: NonZeroU32::new(BYTES_PER_PIXEL * size.width()),
                                rows_per_image: NonZeroU32::new(size.height()),
                            },
                            extent,
        );

        let view = texture.create_view(&TextureViewDescriptor::default());
        let sampler = device.create_sampler(&SamplerDescriptor {
            address_mode_u: AddressMode::Repeat,
            address_mode_v: AddressMode::Repeat,
            address_mode_w: AddressMode::Repeat,
            mag_filter: FilterMode::Nearest,
            min_filter: FilterMode::Nearest,
            mipmap_filter: FilterMode::Nearest,
            ..Default::default()
        });

        let bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: Some("Strigi Texture Bind Group Layout"),
            entries: &[BindGroupLayoutEntry {
                binding: ModelBase::TEXTURE_BINDING.binding,
                visibility: ShaderStages::FRAGMENT,
                ty: BindingType::Texture {
                    multisampled: false,
                    view_dimension: TextureViewDimension::D2,
                    sample_type: TextureSampleType::Float { filterable: true },
                },
                count: None,
            }, BindGroupLayoutEntry {
                binding: ModelBase::TEXTURE_SAMPLER_BINDING.binding,
                visibility: ShaderStages::FRAGMENT,
                ty: BindingType::Sampler(SamplerBindingType::Filtering),
                count: None,
            }],
        });

        let bind_group = device.create_bind_group(
            &BindGroupDescriptor {
                label: Some("Strigi Texture Bind Group"),
                layout: &bind_group_layout,
                entries: &[BindGroupEntry {
                    binding: ModelBase::TEXTURE_BINDING.binding,
                    resource: BindingResource::TextureView(&view),
                }, BindGroupEntry {
                    binding: ModelBase::TEXTURE_SAMPLER_BINDING.binding,
                    resource: BindingResource::Sampler(&sampler),
                }],
            }
        );

        return (bind_group, bind_group_layout);
    }

    /// Creates a WGPU shader module from a string containing the shader program code in WGSL.
    fn create_shader_module(&self, device: &Device, shader_source: &str) -> ShaderModule {
        return device.create_shader_module(ShaderModuleDescriptor {
            label: Some(format!("ModelBase::{}::ShaderModule", self.id()).as_str()),
            source: ShaderSource::Wgsl(shader_source.into()),
        });
    }

    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        return write!(f, "Model id={}, label={:?}, tags={:?}", self.id(), self.label(), self.tags());
    }
}