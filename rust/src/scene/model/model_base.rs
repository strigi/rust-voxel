use std::sync::atomic::{AtomicU32, Ordering};
use crate::image::Image;
use crate::math::{Matrix4, Point3, Vector3};
use crate::scene::Model;

static COUNTER: AtomicU32 = AtomicU32::new(1);

pub fn next_id() -> u32 {
    return COUNTER.fetch_add(1, Ordering::Relaxed);
}

pub struct ModelBase {
    id: u32,
    tags: Vec<String>,
    label: Option<String>,

    position: Point3,
    orientation: Vector3,

    indices: Vec<u32>,
    shader: String,
    texture: Option<Image>,
}

impl ModelBase {
    pub const VERTEX_SHADER_LOCATION_BASE: u32 = 0;
    pub const INSTANCE_SHADER_LOCATION_BASE: u32 = 10;

    pub const POSITION_VERTEX_SHADER_LOCATION: u32 = ModelBase::VERTEX_SHADER_LOCATION_BASE + 0;
    pub const COLOR_VERTEX_SHADER_LOCATION: u32 = ModelBase::VERTEX_SHADER_LOCATION_BASE + 1;
    pub const NORMAL_VERTEX_SHADER_LOCATION: u32 = ModelBase::VERTEX_SHADER_LOCATION_BASE + 2;
    pub const TEXTURE_COORDINATES_VERTEX_SHADER_LOCATION: u32 = ModelBase::VERTEX_SHADER_LOCATION_BASE + 3;

    // TODO: combine model uniforms with model matrix (which is also a model specific uniform)
    pub const MODEL_MATRIX_BINDING: BindGroupId = BindGroupId { group: 1, binding: 1000 };
    pub const MODEL_UNIFORMS_BINDING_BASE: BindGroupId = BindGroupId { group: 2, binding: 2000 };
    pub const TEXTURE_BINDING: BindGroupId = BindGroupId { group: 3, binding: 3000 };
    pub const TEXTURE_SAMPLER_BINDING: BindGroupId = BindGroupId { group: 3, binding: 3001 };

    const DEFAULT_SHADER: &'static str = "
            @group(0) @binding(0) var<uniform> view_matrix: mat4x4<f32>;
            @group(0) @binding(1) var<uniform> projection_matrix: mat4x4<f32>;
            @group(1) @binding(1000) var<uniform> model_matrix: mat4x4<f32>;

            struct VertexInput {
                @location(0) vertex_position: vec3<f32>,
                @location(1) vertex_color: vec4<f32>,
            }

            struct VertexOutput {
                @builtin(position) screen_position: vec4<f32>,
                @location(0) vertex_color: vec4<f32>,
            }

            @vertex
            fn vs_main(input: VertexInput) -> VertexOutput {
                var model_view_matrix: mat4x4<f32> = view_matrix * model_matrix;
                var model_view_projection_matrix: mat4x4<f32> = projection_matrix * model_view_matrix;

                var homogeneous_position = vec4<f32>(input.vertex_position, 1.0);

                var output: VertexOutput;
                output.screen_position = projection_matrix * model_view_matrix * homogeneous_position;
                output.vertex_color = input.vertex_color;
                return output;
            }

            @fragment
            fn fs_main(@location(0) interpolated_vertex_color: vec4<f32>) -> @location(0) vec4<f32> {
                return interpolated_vertex_color;
            }
        ";

    pub fn default() -> Self {
        return Self::new(vec![0, 1, 2], None);
    }

    pub fn new(vertex_indices: Vec<u32>, texture: Option<Image>) -> Self {
        let id = next_id();
        log::debug!("Creating model #{id}");

        return Self {
            id,
            tags: Vec::new(),
            label: None,
            position: Point3::null(),
            orientation: Vector3::null(),
            indices: vertex_indices,
            shader: String::from(Self::DEFAULT_SHADER),
            texture,
        };
    }

    pub fn empty() -> Self {
        return Self::new(Vec::new(), None);
    }

    pub fn position_ref(&self) -> &Point3 {
        return &self.position;
    }

    pub fn orientation_ref(&self) -> &Vector3 {
        return &self.orientation;
    }

    pub fn set_vertex_indices(&mut self, vertex_indices: Vec<u32>) {
        self.indices = vertex_indices;
    }

    pub fn set_shader_source(&mut self, source: &str) {
        self.shader = source.to_string();
    }

    pub fn set_texture(&mut self, image: Image) {
        self.texture = Some(image);
    }
}

impl Model for ModelBase {
    fn id(&self) -> u32 {
        return self.id;
    }

    fn add_tag(&mut self, tag_name: &str) -> bool {
        if self.has_tag(tag_name) {
            return false;
        }

        self.tags.push(tag_name.to_string());
        return true;
    }

    fn tags(&self) -> Vec<&str> {
        return self.tags.iter().map(|tag| { tag.as_str() }).collect();
    }

    fn set_label(&mut self, new_label: &str) {
        self.label = Some(new_label.to_string());
    }

    fn label(&self) -> Option<&str> {
        if self.label.is_none() {
            return None;
        }
        return Some("TODO");
    }

    fn translate_by(&mut self, amount: &Vector3) {
        self.position.add_assign(amount);
    }

    fn translate_to(&mut self, new_position: &Point3) {
        self.position = new_position.clone();
    }

    fn rotate_by(&mut self, amount: &Vector3) {
        self.orientation.add_assign(amount);
    }

    fn rotate_to(&mut self, new_orientation: &Vector3) {
        self.orientation = new_orientation.clone();
    }

    fn create_model_matrix(&self) -> Matrix4 {
        let translation = Matrix4::translate_xyz(self.position.x(), self.position.y(), self.position.z());
        let rotation = Matrix4::rotate_xyz(self.orientation.x(), self.orientation.y(), self.orientation.z());
        return translation.mul(&rotation);
    }

    fn indices_ref(&self) -> &[u32] {
        return self.indices.as_slice();
    }

    fn shader_source_ref(&self) -> &str {
        return self.shader.as_str();
    }

    fn texture_ref(&self) -> Option<&Image> {
        return self.texture.as_ref();
    }
}

pub struct BindGroupId {
    pub group: u32,
    pub binding: u32,
}