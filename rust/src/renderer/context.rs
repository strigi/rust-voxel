use pollster::FutureExt;
use wgpu::{Adapter, Backends, BindGroup, BindGroupDescriptor, BindGroupEntry, BindGroupLayout, BindGroupLayoutDescriptor, BindGroupLayoutEntry, BindingType, Buffer, BufferBindingType, BufferUsages, CommandBuffer, CommandEncoder, CommandEncoderDescriptor, Device, DeviceDescriptor, Extent3d, Features, Instance, Label, Limits, PowerPreference, PresentMode, Queue, RequestAdapterOptions, ShaderStages, Surface, SurfaceConfiguration, SurfaceTexture, Texture, TextureDescriptor, TextureDimension, TextureFormat, TextureUsages};
use wgpu::util::{BufferInitDescriptor, DeviceExt};

use crate::math::{Dimensions2u, Matrix4};
use crate::scene::{Vertex};
use crate::window::Window;

pub(super) struct Context {
    surface: Surface,
    device: Device,
    adapter: Adapter,
    queue: Queue,
}

impl Context {
    pub(super) fn new(window: &dyn Window) -> Self {
        log::info!("Creating WGPU Instance...");
        let instance = Instance::new(Backends::all());

        log::info!("Creating WGPU Surface...");
        let surface = unsafe {
            instance.create_surface(&window)
        };

        // TODO; deal with the future thing block_on (get rid of pollster)
        log::info!("Requesting WGPU Adapter...");
        let adapter = instance.request_adapter(&RequestAdapterOptions {
            power_preference: PowerPreference::HighPerformance,
            compatible_surface: Some(&surface),
            force_fallback_adapter: false
        }).block_on().expect("Unable to find suitable WGPU Aapter");

        // TODO; deal with the future thing block_on (get rid of pollster)
        log::info!("Creating WGPU Device and Queue...");
        let (device, queue) = adapter.request_device(&DeviceDescriptor {
            label: Some("Strigi Device"),
            features: Features::POLYGON_MODE_POINT | Features::POLYGON_MODE_LINE,
            // features: Features::empty(),
            limits: Limits::default(),
        }, None).block_on().expect("Unable to request device");

        return Self {
            device,
            adapter,
            queue,
            surface,
        };
    }

    pub(super) fn configure_surface(&self, size: &Dimensions2u) {
        log::info!("Configuring WGPU surface for resolution {}", size);
        let surface_configuration = SurfaceConfiguration {
            usage: TextureUsages::RENDER_ATTACHMENT,
            format: self.preferred_surface_texture_format(),
            width: size.width(),
            height: size.height(),
            present_mode: PresentMode::Immediate,
        };
        self.surface.configure(&self.device, &surface_configuration);
    }

    pub(super) fn create_bind_group_and_layout(&self, buffers: &[(u32, &Buffer)]) -> (BindGroupLayout, BindGroup) {
        let bind_group_layout_entries: Vec<BindGroupLayoutEntry> = buffers.iter().map(|(bind_id, _)| {
            return Self::create_bind_group_layout_entry(*bind_id);
        }).collect();
        let bind_group_layout = self.create_bind_group_layout(bind_group_layout_entries.as_slice());

        let bind_group = self.create_bind_group(&bind_group_layout, buffers);
        return (bind_group_layout, bind_group);
    }

    pub (super) fn surface_texture(&self) -> SurfaceTexture {
        return self.surface.get_current_texture().expect("Unable to obtain surface texture");
    }

    pub(super) fn preferred_surface_texture_format(&self) -> TextureFormat {
        return self.surface.get_supported_formats(&self.adapter)[0];
    }

    fn create_bind_group(&self, layout: &BindGroupLayout, buffers: &[(u32, &Buffer)]) -> BindGroup {
        let mut group_entries = Vec::with_capacity(buffers.len());
        for (bind_id, buffer) in buffers {
            group_entries.push(Self::create_bind_group_entry(*bind_id, buffer));
        }

        let group = self.device.create_bind_group(&BindGroupDescriptor {
            label: Some("Strigi's BindGroup"),
            layout: &layout,
            entries: &group_entries.as_slice(),
        });

        return group;
    }

    fn create_bind_group_layout(&self, layout_entries: &[BindGroupLayoutEntry]) -> BindGroupLayout {
        return self.device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: Some("Strigi's BindGroupLayout"),
            entries: layout_entries,
        });
    }

    fn create_bind_group_entry(bind_id: u32, buffer: &Buffer) -> BindGroupEntry {
        return BindGroupEntry {
            binding: bind_id,
            resource: buffer.as_entire_binding()
        }
    }

    fn create_bind_group_layout_entry(bind_id: u32) -> BindGroupLayoutEntry {
        BindGroupLayoutEntry {
            binding: bind_id,
            visibility: ShaderStages::VERTEX,
            ty: BindingType::Buffer {
                ty: BufferBindingType::Uniform,
                has_dynamic_offset: false,
                min_binding_size: None,
            },
            count: None,
        }
    }

    pub(super) fn create_index_buffer(&self, indexes: &[u32]) -> Buffer {
        return self.device.create_buffer_init(&BufferInitDescriptor {
            label: Label::from("Strigi Index Buffer"),
            usage: BufferUsages::INDEX,
            contents: bytemuck::cast_slice(indexes),
        });
    }

    pub(super) fn create_vertex_buffer(&self, vertices: &[Vertex]) -> Buffer {
        let mut components: Vec<f32> = Vec::new();
        for vertex in vertices {
            let position = vertex.position_ref();
            components.push(position.x());
            components.push(position.y());
            components.push(position.z());

            let color = vertex.color_ref();
            components.push(color.r());
            components.push(color.g());
            components.push(color.b());

            let texture_coordinates = vertex.texture_coordinates();
            println!("TEX COORDS {:?}", texture_coordinates);
            components.push(texture_coordinates.x());
            components.push(texture_coordinates.y());
        }

        return self.device.create_buffer_init(&BufferInitDescriptor {
            label: Label::from("Strigi Vertex Buffer"),
            usage: BufferUsages::VERTEX,
            contents: bytemuck::cast_slice(components.as_slice()),
        });
    }

    pub(super) fn create_uniform_matrix_buffer(&self, matrix: &Matrix4) -> Buffer {
        return self.device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Strigi's Matrix Buffer"),
            contents: matrix.bytes(),
            usage: BufferUsages::UNIFORM | BufferUsages::COPY_DST,
        });
    }

    pub(super) fn create_depth_texture(&self, size: &Dimensions2u) -> Texture {
        return self.create_texture(&TextureDescriptor {
            label: Some("Strigi Depth Texture"),
            size: Extent3d {
                width: size.width(),
                height: size.height(),
                depth_or_array_layers: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: TextureFormat::Depth32Float,
            usage: TextureUsages::RENDER_ATTACHMENT | TextureUsages::TEXTURE_BINDING,
        });
    }

    fn create_texture(&self, texture_descriptor: &TextureDescriptor) -> Texture {
        return self.device.create_texture(texture_descriptor);
    }

    pub(super) fn create_command_encoder(&self) -> CommandEncoder {
        return  self.device.create_command_encoder(&CommandEncoderDescriptor {
            label: Some("Strigi CommandEncoder"),
        });
    }

    pub(super) fn submit<I: IntoIterator<Item = CommandBuffer>>(&self, command_buffers: I) {
        self.queue.submit(command_buffers);
    }

    pub(super) fn write_matrix_buffer(&self, buffer: &Buffer, matrix: &Matrix4) {
        let bytes = matrix.bytes();
        self.queue.write_buffer(buffer, 0, bytes);
    }

    pub(super) fn device_ref(&self) -> &Device {
        return &self.device;
    }

    pub(super) fn queue_ref(&self) -> &Queue {
        return &self.queue;
    }
}
