use std::collections::HashMap;
use wgpu::{BindGroup, Buffer, CommandEncoder, IndexFormat, Label, LoadOp, Operations, Queue, RenderPass, RenderPassColorAttachment, RenderPassDepthStencilAttachment, RenderPassDescriptor, RenderPipeline, TextureView, TextureViewDescriptor};

use crate::math::{Dimensions2u};
use crate::scene::{Model, ModelBase, Scene};
use crate::window::Window;
use super::context::Context;

pub struct Renderer {
    context: Context,
    depth_texture_view: Option<TextureView>,

    model_cache: HashMap<u32, ModelCache>,
    scene_cache: Option<SceneCache>,
}

impl Renderer {
    /// Initializes a new renderer instance for the given window.
    pub fn new(window: &dyn Window, configure: bool) -> Self {
        let context = Context::new(window);

        let mut this = Self {
            context,
            depth_texture_view: None,
            model_cache: HashMap::new(),
            scene_cache: None,
        };

        if configure {
            this.configure(&window.size());
        }

        return this;
    }

    /// Configures or reconfigures the drawing surface for the given size.
    /// This method should be called every time the window changes size, and at least before the first frame is rendered.
    pub fn configure(&mut self, size: &Dimensions2u) {
        let size = Dimensions2u::size(size.width(), size.height());
        self.context.configure_surface(&size);
        self.depth_texture_view = Some(self.context.create_depth_texture(&size).create_view(&TextureViewDescriptor::default()));
    }

    fn model_cache_ref(&self, id: u32) -> &ModelCache {
        return self.model_cache.get(&id).expect(format!("Unable to find cache for mesh #{}", id).as_str());
    }

    fn scene_cache_ref(&self) -> &SceneCache {
        return self.scene_cache.as_ref().expect("Camera cache not found");
    }

    // TODO: instead of updating all the things, let the models manage their own updates?
    //  most things may not require to be updated often? (except for the camera view matrix?)
    pub fn update(&mut self, scene: &Scene) {
        let models = scene.models_ref();

        let view_matrix_buffer = self.context.create_uniform_matrix_buffer(&scene.camera_ref().create_view_matrix());
        let projection_matrix_buffer = self.context.create_uniform_matrix_buffer(&scene.camera_ref().create_projection_matrix());
        let (view_projection_matrix_bind_group_layout, view_projection_matrix_bind_group) = self.context.create_bind_group_and_layout(&[
            (0, &view_matrix_buffer),
            (1, &projection_matrix_buffer),
        ]);
        self.scene_cache = Some(SceneCache {
            bind_group: BindGroupCache::for_bind_group(view_projection_matrix_bind_group)
                .add_updatable_buffer(view_matrix_buffer, Box::new(|scene: &Scene, buffer, queue| {
                    queue.write_buffer(buffer, 0, scene.camera_ref().create_view_matrix().bytes());
                })).add_updatable_buffer(projection_matrix_buffer, Box::new(|scene: &Scene, buffer, queue| {
                    queue.write_buffer(buffer, 0, scene.camera_ref().create_projection_matrix().bytes());
                })),
        });

        for model in models {
            let mut bind_group_layouts = Vec::with_capacity(4);
            let mut bind_groups = vec![None, None, None];

            // Instance buffers are just vertex buffers that have their layout's vertex step mode attribute set to Instance, the data contained within is also shorter (one per instance instead of one per vertex)
            let mut vertex_buffers_and_layouts = Vec::new();
            vertex_buffers_and_layouts.append(&mut model.create_vertex_buffers(self.context.device_ref()));
            let (instance_count, mut instance_buffers) = model.create_instance_buffers(self.context.device_ref());
            vertex_buffers_and_layouts.append(&mut instance_buffers);

            // Separate the vertex buffers from the vertex buffer layouts
            let mut vertex_buffers = Vec::with_capacity(vertex_buffers_and_layouts.len());
            let mut vertex_buffer_layouts = Vec::with_capacity(vertex_buffers_and_layouts.len());
            for (buffer_layout, buffer) in vertex_buffers_and_layouts {
                vertex_buffers.push(buffer);
                vertex_buffer_layouts.push(buffer_layout);
            }

            let index_buffer = model.create_index_buffer(self.context.device_ref());

            let model_matrix_buffer = self.context.create_uniform_matrix_buffer(&model.create_model_matrix());
            let (model_matrix_bind_group_layout, model_matrix_bind_group) = self.context.create_bind_group_and_layout(&[
                (ModelBase::MODEL_MATRIX_BINDING.binding, &model_matrix_buffer),
            ]);
            bind_groups[(ModelBase::MODEL_MATRIX_BINDING.group - SceneCache::NUMBER_OF_BIND_GROUPS) as usize] = Some(BindGroupCache::for_bind_group(model_matrix_bind_group)
                .add_updatable_buffer(model_matrix_buffer, Box::new(|model: &Box<dyn Model>, buffer, queue| {
                    let model_matrix = model.create_model_matrix();
                    queue.write_buffer(buffer, 0, model_matrix.bytes());
                })
            ));
            bind_group_layouts.push(&view_projection_matrix_bind_group_layout);
            bind_group_layouts.push(&model_matrix_bind_group_layout);

            let uniforms_option = model.create_uniforms(self.context.device_ref());
            let uniform_bind_group_layout;
            if uniforms_option.is_some() {
                let (bind_group_layout, uniform_bind_group, _) = uniforms_option.unwrap();
                uniform_bind_group_layout = bind_group_layout;
                bind_group_layouts.push(&uniform_bind_group_layout);
                bind_groups[(ModelBase::MODEL_UNIFORMS_BINDING_BASE.group - SceneCache::NUMBER_OF_BIND_GROUPS) as usize] = Some(BindGroupCache::for_bind_group(uniform_bind_group));
            }

            // If there is a texture, create a bind group and a layout for it
            let texture_bind_group_layout;
            if model.has_texture() {
                let (texture_bind_group, bind_group_layout) = model.create_texture_bind_group_and_layout(
                    &self.context.device_ref(),
                    self.context.queue_ref(),
                );
                texture_bind_group_layout = bind_group_layout;
                bind_group_layouts.push(&texture_bind_group_layout);
                bind_groups[(ModelBase::TEXTURE_BINDING.group - SceneCache::NUMBER_OF_BIND_GROUPS) as usize] = Some(BindGroupCache::for_bind_group(texture_bind_group))
            }

            let render_pipeline = model.create_render_pipeline(
                self.context.device_ref(),
                bind_group_layouts.as_slice(),
                self.context.preferred_surface_texture_format(),
                vertex_buffer_layouts.as_slice()
            );

            self.model_cache.insert(model.id(), ModelCache {
                vertex_buffers,
                index_buffer,
                instance_count,
                render_pipeline,
                bind_groups,
            });
            log::debug!("Cache has {} models", self.model_cache.len())
        }
    }

    pub fn capture(&self, scene: &Scene) {
        let surface_texture = self.context.surface_texture();
        let view = surface_texture.texture.create_view(&TextureViewDescriptor::default());
        let mut command_encoder = self.context.create_command_encoder();

        {
            let mut render_pass = self.begin_render_pass(&mut command_encoder, &view);
            for model in scene.models_ref() {
                let model_cache = self.model_cache_ref(model.id());
                render_pass.set_pipeline(&model_cache.render_pipeline);

                for (index, vertex_buffer) in model_cache.vertex_buffers.iter().enumerate() {
                    render_pass.set_vertex_buffer(index as u32, vertex_buffer.slice(..));
                }
                render_pass.set_index_buffer(model_cache.index_buffer.slice(..), IndexFormat::Uint32);

                let mut bind_group_index = 0;
                log::trace!("Setting Scene BindGroup {} {:?}", bind_group_index, self.scene_cache_ref().bind_group.bind_group);
                render_pass.set_bind_group(bind_group_index, &self.scene_cache_ref().bind_group.bind_group, &[]);
                for (buffer, buffer_writer) in &self.scene_cache_ref().bind_group.buffers {
                    log::trace!("\tUpdating buffer in Scene BindGroup {}: {:?}: ", bind_group_index, buffer);
                    buffer_writer(scene, &buffer, self.context.queue_ref());
                }
                bind_group_index += 1;

                // The uniform, model and texture bind groups need to be set for all models per frame
                for bind_group_option in model_cache.bind_groups.iter() {
                    if bind_group_option.is_none() {
                        continue;
                    }
                    let bind_group_cache = bind_group_option.as_ref().unwrap();
                    log::trace!("Setting Model BindGroup {} {:?}", bind_group_index, bind_group_cache.bind_group);
                    for (buffer, buffer_writer) in &bind_group_cache.buffers {
                        log::trace!("\tUpdating buffer in Model BindGroup {}: {:?}: ", bind_group_index, buffer);
                        buffer_writer(model, buffer, self.context.queue_ref());
                    }
                    render_pass.set_bind_group(bind_group_index, &bind_group_cache.bind_group, &[]);
                    bind_group_index += 1
                }

                log::trace!("Invoking draw call for model id={} label={} tags={:?}", model.id(), model.label().unwrap_or(""), model.tags());
                render_pass.draw_indexed(0..model.index_count(), 0, 0..model_cache.instance_count);
            }
        }

        let command_buffer = command_encoder.finish();
        self.context.submit([command_buffer]);
        surface_texture.present();
    }

    pub(super) fn begin_render_pass<'b>(&'b self, command_encoder: &'b mut CommandEncoder, texture_view: &'b TextureView) -> RenderPass {
        return command_encoder.begin_render_pass(&RenderPassDescriptor {
            label: Label::from("Strigi RenderPass"),
            color_attachments: &[Some(RenderPassColorAttachment {
                view: texture_view,
                resolve_target: None,
                ops: Operations {
                    load: LoadOp::Clear(wgpu::Color { r: 0.118, g: 0.122, b: 0.133, a: 1.000 }),
                    store: true,
                },
            })],
            depth_stencil_attachment: Some(RenderPassDepthStencilAttachment {
                view: self.depth_texture_view.as_ref().expect("Surface has not been configured yet. Did you forget to call configure_for_size() ?"),
                depth_ops: Some(Operations {
                    load: LoadOp::Clear(1.0),
                    store: true
                }),
                stencil_ops: None,
            }),
        });
    }
}

struct SceneCache {
    bind_group: BindGroupCache<Scene>,
}

impl SceneCache {
    const NUMBER_OF_BIND_GROUPS: u32 = 1;
}

type BufferWriterFn<T> = dyn Fn(&T, &Buffer, &Queue) -> ();

struct ModelCache {
    instance_count: u32,
    render_pipeline: RenderPipeline,
    index_buffer: Buffer,
    vertex_buffers: Vec<Buffer>,
    bind_groups: Vec<Option<BindGroupCache<Box<dyn Model>>>>,

}

struct BindGroupCache<T> {
    bind_group: BindGroup,
    buffers: Vec<(Buffer, Box<BufferWriterFn<T>>)>,
}

impl <T> BindGroupCache<T> {
    fn for_bind_group(bind_group: BindGroup) -> Self {
        return BindGroupCache {
            bind_group,
            buffers: Vec::new(),
        }
    }

    fn add_updatable_buffer(mut self, buffer: Buffer, buffer_writer: Box<BufferWriterFn<T>>) -> Self {
        self.buffers.push((buffer, buffer_writer));
        return self;
    }
}
