use std::process::exit;
use crate::controls::Controls;
use crate::renderer::{Renderer};
use crate::scene::{Model, Scene, Voxel, VoxelMeshBuilder, Camera, VoxelMesh, HeightmapImageVoxelBuilder, PlaneMesh, SphereVoxelBuilder, ModelBase};
use crate::controls::Key;
use crate::math::{Color, Dimensions2f, Dimensions2u, Dimensions3u, Vector3};
use crate::statistics::Statistics;
use crate::timer::Timer;

struct Attributes {
    pub paused: bool,
    pub move_speed: f32,
    pub turn_speed: f32,
}

pub struct Controller {
    controls: Controls<Scene, Attributes>,
    renderer: Renderer,
    ticks: Timer,
    statistics: Statistics,
}

impl Controller {
    pub fn new(mut renderer: Renderer) -> Self {
        let scene = Self::create_scene();

        renderer.update(&scene);

        let controls = Self::create_controls(scene);
        let ticks = Timer::from_frequency(50.0);

        return Self {
            controls,
            renderer,
            ticks,
            statistics: Statistics::new(),
        };
    }

    fn create_scene() -> Scene {
        let mut camera = Camera::new(Dimensions2f::size(0.035, 0.035 * 9.0 / 16.0));
        camera.translate_to(&Vector3::xyz(0.0, 0.0, -100.0));
        let mut scene = Scene::new(camera);

        let mesh_size = 50.0;
        let resolution = 64;
        let block_size = mesh_size / resolution as f32;

        // let radius = mesh_size / 2.0;
        // let mut mesh = SphereVoxelBuilder::with_radius(radius).build(block_size, Dimensions3u::new(resolution, resolution, resolution));

        let mut mesh = HeightmapImageVoxelBuilder::from_heightmap_image("../resources/coast2.bmp").build(block_size, Dimensions3u::new(resolution, resolution / 4, resolution));

        // let mut mesh = ModelBase::default();
        mesh.rotate_by(&Vector3::xyz(-45.0, 0.0, 0.0));
        scene.add_model(Box::new(mesh));
        return scene;
    }

    fn create_controls(scene: Scene) -> Controls<Scene, Attributes> {
        let mut controls = Controls::new(scene, Attributes {
            paused: false,
            move_speed: 1.0,  // meters per tick
            turn_speed: 0.75, // deg per tick
        });

        controls.once(Key::Escape, |_| {
            exit(0);
        });

        controls.once(Key::Pause, |e| {
            if e.attributes.paused {
                log::info!("Resume animation")
            } else {
                log::info!("Pause animation")
            }
            e.attributes.paused = !e.attributes.paused;
        });

        controls.times(Key::W, |e| {
            e.target.camera_mut().translate_by(&Vector3::xyz(0.0, 0.0, e.attributes.move_speed))
        });

        controls.times(Key::S, |e| {
            e.target.camera_mut().translate_by(&Vector3::xyz(0.0, 0.0, -e.attributes.move_speed))
        });

        controls.times(Key::A, |e| {
            e.target.camera_mut().translate_by(&Vector3::xyz(-e.attributes.move_speed, 0.0, 0.0))
        });

        controls.times(Key::D, |e| {
            e.target.camera_mut().translate_by(&Vector3::xyz(e.attributes.move_speed, 0.0, 0.0))
        });

        controls.times(Key::Space, |e| {
            e.target.camera_mut().translate_by(&Vector3::xyz(0.0, e.attributes.move_speed, 0.0))
        });

        controls.times(Key::LeftShift, |e| {
            e.target.camera_mut().translate_by(&Vector3::xyz(0.0, -e.attributes.move_speed, 0.0))
        });

        controls.once(Key::KeypadPlus, |e| {
            e.target.camera_mut().add_near(0.001)
        });

        controls.once(Key::KeypadMinus, |e| {
            e.target.camera_mut().add_near(-0.001)
        });

        controls.times(Key::F1, |e| {
            optionally_rotate_by(e.target.models_mut(), 0, &Vector3::xyz(e.attributes.turn_speed, 0.0, 0.0));
        });

        controls.times(Key::F2, |e| {
            optionally_rotate_by(e.target.models_mut(), 0, &Vector3::xyz(-e.attributes.turn_speed, 0.0, 0.0));
        });

        controls.times(Key::F3, |e| {
            optionally_rotate_by(e.target.models_mut(), 0, &Vector3::xyz(0.0, e.attributes.turn_speed, 0.0));
        });

        controls.times(Key::F4, |e| {
            optionally_rotate_by(e.target.models_mut(), 0, &Vector3::xyz(0.0, -e.attributes.turn_speed, 0.0))
        });

        controls.times(Key::F5, |e| {
            optionally_rotate_by(e.target.models_mut(), 0, &Vector3::xyz(0.0, 0.0, e.attributes.turn_speed))
        });

        controls.times(Key::F6, |e| {
            optionally_rotate_by(e.target.models_mut(), 0, &Vector3::xyz(0.0, 0.0, -e.attributes.turn_speed))
        });

        controls.times(Key::F7, |e| {
            optionally_rotate_by(e.target.models_mut(), 0, &Vector3::xyz(e.attributes.turn_speed, 0.0, 0.0))
        });

        controls.times(Key::F8, |e| {
            optionally_rotate_by(e.target.models_mut(), 1, &Vector3::xyz(-e.attributes.turn_speed, 0.0, 0.0))
        });

        controls.times(Key::F9, |e| {
            optionally_rotate_by(e.target.models_mut(), 1, &Vector3::xyz(0.0, e.attributes.turn_speed, 0.0))
        });

        controls.times(Key::F10, |e| {
            optionally_rotate_by(e.target.models_mut(), 1, &Vector3::xyz(0.0, -e.attributes.turn_speed, 0.0))
        });

        controls.times(Key::F11, |e| {
            optionally_rotate_by(e.target.models_mut(), 1, &Vector3::xyz(0.0, 0.0, e.attributes.turn_speed))
        });

        controls.times(Key::F12, |e| {
            optionally_rotate_by(e.target.models_mut(), 1, &Vector3::xyz(0.0, 0.0, -e.attributes.turn_speed))
        });

        return controls;

        fn optionally_rotate_by(models: &mut [Box<dyn Model>], index: usize, vector: &Vector3) {
            let model_option = models.get_mut(index);
            if model_option.is_some() {
                model_option.unwrap().rotate_by(vector)
            }
        }
    }

    fn renderer_ref(&self) -> &Renderer {
        return &self.renderer;
    }

    fn renderer_mut(&mut self) -> &mut Renderer {
        return &mut self.renderer
    }

    fn scene_ref(&self) -> &Scene {
        return self.controls.target_ref();
    }

    fn scene_mut(&mut self) -> &mut Scene {
        return self.controls.target_mut();
    }

    pub fn render(&self) {
        self.renderer_ref().capture(self.scene_ref());
    }

    pub fn resize(&mut self, size: &Dimensions2u) {
        self.renderer.configure(size);
    }

    pub fn input(&mut self, key: Key, value: f32) {
        self.controls.update(key.clone(), value);
    }

    pub fn is_paused(&self) -> bool {
        return self.controls.attributes_ref().paused;
    }

    pub fn interact(&mut self) -> bool {
        if !self.ticks.check_expired_and_mark() {
            return false;
        }

        self.controls.dispatch();
        if !self.is_paused() {
            for model in self.scene_mut().models_mut().iter_mut() {
                model.rotate_by(&Vector3::xyz(0.0, 0.1, 0.0));
            }
        }
        return true;
    }

    pub fn iteration(&mut self) {
        self.statistics.update();
        self.interact();
        self.render();
    }
}
