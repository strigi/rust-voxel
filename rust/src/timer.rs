use std::time::SystemTime;

pub struct Timer {
    start_time: SystemTime,
    period: f32,
    frequency: f32,
    last_marker: f32,
    counter: u32,
}

impl Timer {
    pub fn from_period(period: f32) -> Self {
        let timer = Self {
            start_time: SystemTime::now(),
            period,
            frequency: 1.0 / period,
            last_marker: 0.0,
            counter: 0,
        };
        log::info!("Creating f={:.3} Hz, T={:.3} s timer", timer.frequency(), timer.period());
        return timer;
    }

    pub fn from_frequency(frequency: f32) -> Self {
        return Self::from_period(1.0 / frequency);
    }

    pub fn frequency(&self) -> f32 {
        return self.frequency;
    }

    pub fn period(&self) -> f32 {
        return self.period;
    }

    /// Returns the amount of time in seconds since the timer was created.
    /// This value is never affected by any markers.
    pub fn elapsed(&self) -> f32 {
        return self.start_time.elapsed().unwrap().as_secs_f32()
    }

    /// Returns the difference in seconds since the last marked time.
    pub fn delta(&self) -> f32 {
        return self.elapsed() - self.last_marker;
    }

    /// Return true if the time of the last mark has passed the period (interval) of the timer.
    pub fn is_expired(&self) -> bool {
        return self.delta() > self.period;
    }

    /// Returns the number times a mark was placed.
    pub fn count(&self) -> u32 {
        return self.counter;
    }

    /// Marks the current time as a reference for `delta()`.
    pub fn mark(&mut self) {
        self.last_marker = self.elapsed();
        self.counter += 1;
    }

    /// Checks if the timer has expired and if so places a mark for a new cycle to start.
    pub fn check_expired_and_mark(&mut self) -> bool {
        return if self.is_expired() {
            self.mark();
            true
        } else {
            false
        }
    }
}
