use ::raw_window_handle::{HasRawWindowHandle, RawWindowHandle};
use ::sdl2::EventPump;
use ::sdl2::event::WindowEvent;
use sdl2::keyboard::{Keycode, Scancode};
use crate::controls::Key;
use crate::math::Dimensions2u;
use crate::window::{Window, Event};

pub struct SDLWindow {
    window: sdl2::video::Window,
    event_pump: EventPump,
}

impl SDLWindow {
    pub fn new(title: &str, width: u32, height: u32) -> Self {
        log::info!("Initializing SDL Context...");
        let sdl_context = sdl2::init().expect("Unable to initialize SDL Context");

        log::info!("Initializing SDL Video Subsystem...");
        let video_subsystem = sdl_context.video().expect("Unable to initialize SDL Video Subsystem");

        log::info!("Creating SDL Window...");
        let window = video_subsystem.window(title, width, height)
            .position_centered()
            .resizable()
            .hidden()
            // .vulkan()
            // .fullscreen()
            .build()
            .expect("Unable to create SDL Window");

        let event_pump = sdl_context.event_pump().expect("Unable to obtain SDL2 Event Pump");

        let result = Self {
            window,
            event_pump,
        };

        return result;
    }

    fn handle_sdl_event(&self, event: sdl2::event::Event) -> Option<Event> {
        return match event {
            sdl2::event::Event::Quit { .. } => {
                Some(Event::Close)
            }

            sdl2::event::Event::Window { win_event: WindowEvent::Shown, .. } => {
                log::debug!("Emitting Resize event upon initial window shown");
                let size = self.size();
                Some(Event::Resize(size))
            }

            sdl2::event::Event::Window { win_event: WindowEvent::Resized(width, height), .. } => {
                log::debug!("Emitting Resize event upon window resize");
                Some(Event::Resize(Dimensions2u::size(width as u32, height as u32)))
            }

            sdl2::event::Event::KeyDown { keycode, scancode, repeat, .. } => {
                self.create_input_key(keycode, scancode, repeat, 1.0)
            }

            sdl2::event::Event::KeyUp { keycode, scancode, repeat, .. } => {
                self.create_input_key(keycode, scancode, repeat, 0.0)
            }

            _ => {
                None
            }
        }
    }

    fn create_input_key(&self, keycode: Option<Keycode>, scancode: Option<Scancode>, repeat: bool, value: f32) -> Option<Event> {
        if repeat {
            return None;
        }

        let mut input_key = None;
        if scancode.is_some() {
            input_key = match scancode.unwrap() {
                // First row
                Scancode::Escape => Some(Event::Input(Key::Escape, value)),
                Scancode::F1 => Some(Event::Input(Key::F1, value)),
                Scancode::F2 => Some(Event::Input(Key::F2, value)),
                Scancode::F3 => Some(Event::Input(Key::F3, value)),
                Scancode::F4 => Some(Event::Input(Key::F4, value)),
                Scancode::F5 => Some(Event::Input(Key::F5, value)),
                Scancode::F6 => Some(Event::Input(Key::F6, value)),
                Scancode::F7 => Some(Event::Input(Key::F7, value)),
                Scancode::F8 => Some(Event::Input(Key::F8, value)),
                Scancode::F9 => Some(Event::Input(Key::F9, value)),
                Scancode::F10 => Some(Event::Input(Key::F10, value)),
                Scancode::F11 => Some(Event::Input(Key::F11, value)),
                Scancode::F12 => Some(Event::Input(Key::F12, value)),
                Scancode::PrintScreen => Some(Event::Input(Key::PrintScreen, value)),
                Scancode::ScrollLock => Some(Event::Input(Key::ScrollLock, value)),
                Scancode::Pause => Some(Event::Input(Key::Pause, value)),

                // Second row
                Scancode::Grave => Some(Event::Input(Key::Grave, value)),
                Scancode::Num1 => Some(Event::Input(Key::Num1, value)),
                Scancode::Num2 => Some(Event::Input(Key::Num2, value)),
                Scancode::Num3 => Some(Event::Input(Key::Num3, value)),
                Scancode::Num4 => Some(Event::Input(Key::Num4, value)),
                Scancode::Num5 => Some(Event::Input(Key::Num5, value)),
                Scancode::Num6 => Some(Event::Input(Key::Num6, value)),
                Scancode::Num7 => Some(Event::Input(Key::Num7, value)),
                Scancode::Num8 => Some(Event::Input(Key::Num8, value)),
                Scancode::Num9 => Some(Event::Input(Key::Num9, value)),
                Scancode::Num0 => Some(Event::Input(Key::Num0, value)),
                Scancode::Minus => Some(Event::Input(Key::Minus, value)),
                Scancode::Equals => Some(Event::Input(Key::Equals, value)),
                Scancode::Backspace => Some(Event::Input(Key::Backspace, value)),
                Scancode::Insert => Some(Event::Input(Key::Insert, value)),
                Scancode::Home => Some(Event::Input(Key::Home, value)),
                Scancode::PageUp => Some(Event::Input(Key::PageUp, value)),
                Scancode::NumLockClear => Some(Event::Input(Key::NumLock, value)),
                Scancode::KpDivide => Some(Event::Input(Key::KeypadDivide, value)),
                Scancode::KpMultiply => Some(Event::Input(Key::KeypadMultiply, value)),
                Scancode::KpMinus => Some(Event::Input(Key::KeypadMinus, value)),

                // Third row
                Scancode::Tab => Some(Event::Input(Key::Tab, value)),
                Scancode::Q => Some(Event::Input(Key::Q, value)),
                Scancode::W => Some(Event::Input(Key::W, value)),
                Scancode::E => Some(Event::Input(Key::E, value)),
                Scancode::R => Some(Event::Input(Key::R, value)),
                Scancode::T => Some(Event::Input(Key::T, value)),
                Scancode::Y => Some(Event::Input(Key::Y, value)),
                Scancode::U => Some(Event::Input(Key::U, value)),
                Scancode::I => Some(Event::Input(Key::I, value)),
                Scancode::O => Some(Event::Input(Key::O, value)),
                Scancode::P => Some(Event::Input(Key::P, value)),
                Scancode::LeftBracket => Some(Event::Input(Key::LeftBracket, value)),
                Scancode::RightBracket => Some(Event::Input(Key::RightBracket, value)),
                Scancode::Return => Some(Event::Input(Key::Return, value)),
                Scancode::Delete => Some(Event::Input(Key::Delete, value)),
                Scancode::End => Some(Event::Input(Key::End, value)),
                Scancode::PageDown => Some(Event::Input(Key::PageDown, value)),
                Scancode::Kp7 => Some(Event::Input(Key::Keypad7, value)),
                Scancode::Kp8 => Some(Event::Input(Key::Keypad8, value)),
                Scancode::Kp9 => Some(Event::Input(Key::Keypad9, value)),
                Scancode::KpPlus => Some(Event::Input(Key::KeypadPlus, value)),

                // Fourth row
                Scancode::CapsLock => Some(Event::Input(Key::CapsLock, value)),
                Scancode::A => Some(Event::Input(Key::A, value)),
                Scancode::S => Some(Event::Input(Key::S, value)),
                Scancode::D => Some(Event::Input(Key::D, value)),
                Scancode::F => Some(Event::Input(Key::F, value)),
                Scancode::G => Some(Event::Input(Key::G, value)),
                Scancode::H => Some(Event::Input(Key::H, value)),
                Scancode::J => Some(Event::Input(Key::J, value)),
                Scancode::K => Some(Event::Input(Key::K, value)),
                Scancode::L => Some(Event::Input(Key::L, value)),
                Scancode::Semicolon => Some(Event::Input(Key::Semicolon, value)),
                Scancode::Apostrophe => Some(Event::Input(Key::Apostrophe, value)),
                Scancode::Backslash => Some(Event::Input(Key::Backslash, value)),
                Scancode::Kp4 => Some(Event::Input(Key::Keypad4, value)),
                Scancode::Kp5 => Some(Event::Input(Key::Keypad5, value)),
                Scancode::Kp6 => Some(Event::Input(Key::Keypad6, value)),

                // Fifth row
                Scancode::LShift => Some(Event::Input(Key::LeftShift, value)),
                Scancode::NonUsBackslash => Some(Event::Input(Key::NonUsBackslash, value)),
                Scancode::Z => Some(Event::Input(Key::Z, value)),
                Scancode::X => Some(Event::Input(Key::X, value)),
                Scancode::C => Some(Event::Input(Key::C, value)),
                Scancode::V => Some(Event::Input(Key::V, value)),
                Scancode::B => Some(Event::Input(Key::B, value)),
                Scancode::N => Some(Event::Input(Key::N, value)),
                Scancode::M => Some(Event::Input(Key::M, value)),
                Scancode::Comma => Some(Event::Input(Key::Comma, value)),
                Scancode::Period => Some(Event::Input(Key::Period, value)),
                Scancode::Slash => Some(Event::Input(Key::Slash, value)),
                Scancode::RShift => Some(Event::Input(Key::RightShift, value)),
                Scancode::Up => Some(Event::Input(Key::Up, value)),
                Scancode::Kp1 => Some(Event::Input(Key::Keypad1, value)),
                Scancode::Kp2 => Some(Event::Input(Key::Keypad2, value)),
                Scancode::Kp3 => Some(Event::Input(Key::Keypad3, value)),
                Scancode::KpEnter => Some(Event::Input(Key::KeypadEnter, value)),

                // Sixth row
                Scancode::LCtrl => Some(Event::Input(Key::LeftControl, value)),
                Scancode::LAlt => Some(Event::Input(Key::LeftAlt, value)),
                Scancode::Space => Some(Event::Input(Key::Space, value)),
                Scancode::RAlt => Some(Event::Input(Key::RightAlt, value)),
                Scancode::Application => Some(Event::Input(Key::Application, value)),
                Scancode::RCtrl => Some(Event::Input(Key::RightControl, value)),
                Scancode::Left => Some(Event::Input(Key::Left, value)),
                Scancode::Down => Some(Event::Input(Key::Down, value)),
                Scancode::Right => Some(Event::Input(Key::Right, value)),
                Scancode::Kp0 => Some(Event::Input(Key::Keypad0, value)),
                Scancode::KpPeriod => Some(Event::Input(Key::KeypadPeriod, value)),

                _ => None
            }
        }
        if input_key.is_none() {
            println!("Unknown SDL keycode='{keycode:?}' scancode={scancode:?} will be ignored");
        }

        return input_key;
    }
}

unsafe impl HasRawWindowHandle for SDLWindow {
    fn raw_window_handle(&self) -> RawWindowHandle {
        return self.window.raw_window_handle();
    }
}

impl Window for SDLWindow {
    fn size(&self) -> Dimensions2u{
        let window_size = self.window.size();
        return Dimensions2u::size(window_size.0, window_size.1);
    }

    fn poll_event(&mut self) -> Option<Event> {
        // Ensure the window is visible, but only make it visible the first time the events are pulled,
        // which gives time to initialize the window upon first start to the client.
        self.window.show();

        loop {
            let sdl_event = self.event_pump.poll_event();
            if sdl_event.is_none() {
                return None;
            }
            let event = self.handle_sdl_event(sdl_event.unwrap());
            if event.is_some() {
                return event;
            }
        }
    }

    fn wait_event(&mut self) -> Event {
        // Ensure the window is visible, but only make it visible the first time the events are pulled,
        // which gives time to initialize the window upon first start to the client.
        self.window.show();

        loop {
            let sdl_event = self.event_pump.wait_event();
            let result = self.handle_sdl_event(sdl_event);
            if result.is_some() {
                return result.unwrap();
            }
        }
    }
}
