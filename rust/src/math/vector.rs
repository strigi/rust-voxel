use std::fmt::Formatter;
use std::fmt::Result;
use std::fmt::Display;
use std::ops::Add;

#[derive(Clone, Debug)]
pub struct Vector<const DIMENSION: usize> {
    entries: [f32; DIMENSION],
}

pub type Vector2 = Vector<2>;
pub type Point2 = Vector2;

pub type Vector3 = Vector<3>;
pub type Point3 = Vector3;

pub type Vector4 = Vector<4>;


impl <const DIMENSION: usize> Vector<DIMENSION> {
    /// Returns the number of bytes an instance of Vector will take.
    pub const BYTES: u32 = (std::mem::size_of::<f32>() * DIMENSION) as u32;

    pub fn null() -> Self {
        return Self {
            entries: [0f32; DIMENSION]
        }
    }

    pub fn new(entries: [f32; DIMENSION]) -> Self {
        return Self {
            entries,
        }
    }

    pub fn get(&self, index: u8) -> f32 {
        return self.entries[index as usize];
    }

    pub fn set(&mut self, index: u8, entry: f32) {
        self.entries[index as usize] = entry;
    }

    /// Calculates the dot product of two vectors.
    pub fn dot(&self, other: &Vector<DIMENSION>) -> f32 {
        let mut result = 0f32;
        for i in 0..DIMENSION {
            result += self.entries[i] * other.entries[i];
        }
        return result;
    }

    pub fn add_assign(&mut self, other: &Self) -> &Self {
        for i in 0..DIMENSION {
            self.entries[i] += other.entries[i];
        }
        return self;
    }

    pub fn mul(&self, factor: f32) -> Self {
        let mut entries = [0f32; DIMENSION];
        for i in 0..DIMENSION {
            entries[i] = self.entries[i] * factor;
        }
        return Self::new(entries);
    }

    pub fn div(&self, divisor: f32) -> Self {
        return self.mul(1.0 / divisor);
    }

    pub fn array_ref(&self) -> &[f32; DIMENSION] {
        return &self.entries;
    }
}

impl <const DIMENSION: usize> Display for Vector<DIMENSION> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        let mut result = Ok(());
        for i in 0..DIMENSION {
            let separator = if i != DIMENSION { " " } else { "" };
            result = result.and(write!(f, "{:.3}{}", self.entries[i], separator));
        }
        return result;
    }
}

impl <const DIMENSION: usize> Add for Vector<DIMENSION> {
    type Output = Vector<DIMENSION>;

    // Old version with no operator overloading
    // pub fn add(&self, other: &Vector3) -> Self {
    //     let mut sum_entries = [0f32; DIMENSION];
    //     for i in 0..DIMENSION {
    //         sum_entries[i] = self.entries[i] + other.entries[i];
    //     }
    //     return Self::new(sum_entries);
    // }

    // TODO: I'm not sure if this is correct, but since both the self and rhs variables are moved
    //   they can't be used afterwards anymore, so I think it's safe to reuse one of them and return that.
    //   that's why self has been made mut (which the compiler seems to be okay with even though it's not defined as mut in the trait.
    fn add(mut self, rhs: Self) -> Self::Output {
        for i in 0..DIMENSION as u8 {
            self.set(i, self.get(i) + rhs.get(i));
        }
        return self
    }
}


// impl Vector<2> {
//     pub fn xy(x: f32, y: f32) -> Self {
//         Self {
//             entries: [x, y]
//         }
//     }
//
//     pub fn x(&self) -> f32 {
//         return self.entries[0]
//     }
//
//     pub fn y(&self) -> f32 {
//         return self.entries[1]
//     }
// }

impl Vector2 {
    pub fn xy(x: f32, y: f32) -> Self {
        Self {
            entries: [x, y]
        }
    }

    pub fn x(&self) -> f32 {
        return self.entries[0]
    }

    pub fn y(&self) -> f32 {
        return self.entries[1]
    }

    pub fn bytes(&self) -> &[u8] {
        unsafe {
            let (_, bytes, _) = self.entries.align_to::<u8>();
            return bytes;
        }
    }
}

impl Vector3 {
    pub fn xyz(x: f32, y: f32, z: f32) -> Self {
        Self {
            entries: [x, y, z]
        }
    }

    pub fn between(a: &Point3, b: &Point3) -> Self {
        return b.sub(&a)
    }

    pub fn normal(a: &Point3, b: &Point3, c: &Point3) -> Self {
        let ab = Vector3::between(a, b);
        let ac = Vector3::between(a, c);
        return ab.cross(&ac).normalize();
    }

    pub fn sub(&self, other: &Vector3) -> Self {
        return Vector3::xyz(
            self.x() - other.x(),
            self.y() - other.y(),
            self.z() - other.z(),
        );
    }

    pub fn length(&self) -> f32 {
        return (self.x() * self.x() + self.y() * self.y() + self.z() * self.z()).sqrt();
    }

    pub fn x(&self) -> f32 {
        return self.entries[0]
    }

    pub fn y(&self) -> f32 {
        return self.entries[1]
    }

    pub fn z(&self) -> f32 {
        return self.entries[2]
    }

    pub fn expand(&self) -> Vector4 {
        return Vector4::from_vec3(self, 1.0);
    }

    pub fn cross(&self, other: &Vector3) -> Self {
        return Vector3::xyz(
            self.y() * other.z() - other.y() * self.z(),
            other.x() * self.z() - self.x() * other.z(),
            self.x() * other.y() - other.x() * self.y()
        );
    }

    /// Returns a new vector that is the normalized version of this vector.
    /// A normalized vector is scaled to length 1
    /// self is not modified.
    pub fn normalize(&self) -> Self {
        return self.div(self.length());
    }

    pub fn bytes(&self) -> &[u8] {
        unsafe {
            let (_, bytes, _) = self.entries.align_to::<u8>();
            return bytes;
        }
    }
}

impl Vector4 {
    pub fn xyzw(x: f32, y: f32, z: f32, w: f32) -> Self {
        Self {
            entries: [x, y, z, w]
        }
    }

    pub fn from_vec3(v: &Vector3, w: f32) -> Self {
        return Self::xyzw(v.x(), v.y(), v.z(), w);
    }

    pub fn x(&self) -> f32 {
        return self.entries[0]
    }

    pub fn y(&self) -> f32 {
        return self.entries[1]
    }

    pub fn z(&self) -> f32 {
        return self.entries[2]
    }

    pub fn w(&self) -> f32 {
        return self.entries[3]
    }

    pub fn contract(&self) -> Vector3 {
        return Vector3::xyz(self.x(), self.y(), self.z());
    }
}
