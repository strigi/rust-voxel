use std::fmt::{Display, Formatter, Result};
use crate::math::{Tuple2f, Tuple2u, Tuple3f, Tuple3u};

#[derive(Clone, Debug)]
pub struct Dimensions2u {
    elements: Tuple2u
}

impl Dimensions2u {
    pub fn size(width: u32, height: u32) -> Self {
        return Self::new(Tuple2u::ab(width, height));
    }

    fn new(elements: Tuple2u) -> Self {
        return Self {
            elements,
        }
    }

    pub fn width(&self) -> u32 {
        return self.elements.a()
    }

    pub fn height(&self) -> u32 {
        return self.elements.b();
    }

    pub fn min(&self) -> u32 {
        return self.elements.a().min(self.elements.b());
    }

    pub fn max(&self) -> u32 {
        return self.elements.a().max(self.elements.b());
    }

    pub fn area(&self) -> u32 {
        return self.elements.mul_ab();
    }

    pub fn mul_scalar(&self, factor: u32) -> Self {
        return Self::new(self.elements.mul_scalar(factor))
    }

    pub fn div_scalar(&self, factor: u32) -> Self {
        return Self::new(self.elements.div_scalar(factor))
    }
}

impl Display for Dimensions2u {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        return write!(f, "{}x{}", self.width(), self.height())
    }
}

#[derive(Clone, Debug)]
pub struct Dimensions2f {
    elements: Tuple2f,
}

impl Dimensions2f {
    fn new(elements: Tuple2f) -> Self {
        assert!(elements.a() >= 0.0);
        assert!(elements.b() >= 0.0);
        return Self {
            elements,
        }
    }

    pub fn size(width: f32, height: f32) -> Self {
        return Self::new(Tuple2f::ab(width, height))
    }

    pub fn width(&self) -> f32 {
        return self.elements.a()
    }

    pub fn height(&self) -> f32 {
        return self.elements.b();
    }

    pub fn min(&self) -> f32 {
        return self.elements.a().min(self.elements.b());
    }

    pub fn max(&self) -> f32 {
        return self.elements.a().max(self.elements.b());
    }

    pub fn area(&self) -> f32 {
        return self.elements.mul_ab();
    }

    pub fn diagonal(&self) -> f32 {
        return (self.width() * self.width() + self.height() * self.height()).sqrt();
    }

    pub fn mul_scalar(&self, factor: f32) -> Self {
        assert!(factor >= 0.0);
        return Self::new(self.elements.mul_scalar(factor));
    }

    pub fn div_scalar(&self, divisor: f32) -> Self {
        assert!(divisor >= 0.0);
        return Self::new(self.elements.div_scalar(divisor));
    }
}

impl Display for Dimensions2f {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        return write!(f, "{:.3}x{:.3}", self.width(), self.height())
    }
}

#[derive(Clone, Debug)]
pub struct Dimensions3u {
    elements: Tuple3u
}

impl Dimensions3u {
    pub fn new(width: u32, height: u32, depth: u32) -> Self {
        return Self {
            elements: Tuple3u::abc(width, height, depth),
        }
    }

    pub fn width(&self) -> u32 {
        return self.elements.a()
    }

    pub fn height(&self) -> u32 {
        return self.elements.b();
    }

    pub fn depth(&self) -> u32 {
        return self.elements.c();
    }

    pub fn volume(&self) -> u32 {
        return self.elements.mul_abc();
    }

    pub fn min(&self) -> u32 {
        return self.elements.a().min(self.elements.b()).min(self.elements.c());
    }

    pub fn max(&self) -> u32 {
        return self.elements.a().max(self.elements.b()).max(self.elements.c());
    }

    pub fn as_f(&self) -> Dimensions3f {
        return Dimensions3f {
            elements: self.elements.as_f()
        }
    }

    pub fn add(&self, term: u32) -> Self {
        return Self {
            elements: self.elements.add_scalar(term)
        }
    }

    pub fn mul(&self, factor: u32) -> Self {
        return Self {
            elements: self.elements.mul_scalar(factor),
        }
    }

    pub fn div(&self, divisor: u32) -> Self {
        return Self {
            elements: self.elements.div_scalar(divisor),
        }
    }

    pub fn bytes(&self) -> &[u8] {
        return self.elements.bytes();
    }
}

impl Display for Dimensions3u {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        return write!(f, "{}x{}x{}", self.width(), self.height(), self.depth())
    }
}

#[derive(Clone, Debug)]
pub struct Dimensions3f {
    elements: Tuple3f,
}

impl Dimensions3f {
    pub fn new(width: f32, height: f32, depth: f32) -> Self {
        return Self {
            elements: Tuple3f::abc(width, height, depth),
        }
    }

    pub fn width(&self) -> f32 {
        return self.elements.a()
    }

    pub fn height(&self) -> f32 {
        return self.elements.b();
    }

    pub fn depth(&self) -> f32 {
        return self.elements.c();
    }

    pub fn as_u(&self) -> Dimensions3u {
        return Dimensions3u {
            elements: self.elements.as_u()
        }
    }

    pub fn volume(&self) -> f32 {
        return self.elements.mul_abc();
    }

    pub fn diagonal(&self) -> f32 {
        return (self.width() * self.width() + self.height() * self.height() + self.depth() * self.depth()).sqrt();
    }

    pub fn min(&self) -> f32 {
        return self.elements.a().min(self.elements.b()).min(self.elements.c());
    }

    pub fn max(&self) -> f32 {
        return self.elements.a().max(self.elements.b()).max(self.elements.c());
    }

    pub fn mul(&self, factor: f32) -> Self {
        return Self {
            elements: self.elements.mul_scalar(factor),
        }
    }

    pub fn div(&self, divisor: f32) -> Self {
        return Self {
            elements: self.elements.div_scalar(divisor)
        }
    }
}

impl Display for Dimensions3f {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        return write!(f, "{:.3}x{:.3}x{:.3}", self.width(), self.height(), self.depth())
    }
}