use super::vector::Vector3;

pub struct Triangle {
    pub a: Vector3,
    pub b: Vector3,
    pub c: Vector3,
    pub color: Vector3,
}

impl Triangle {
    pub fn new(a: Vector3, b: Vector3, c: Vector3, color: Vector3) -> Self {
        return Self {
            a,
            b,
            c,
            color,
        }
    }

    pub fn normal(&self) -> Vector3 {
        return Vector3::normal(&self.a, &self.b, &self.c);
    }
}
