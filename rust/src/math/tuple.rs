const A: usize = 0;
const B: usize = 1;
const C: usize = 2;
const D: usize = 3;

#[derive(Clone, Debug)]
pub struct Tuple2u {
    elements: [u32; 2],
}

impl Tuple2u {
    pub(super) fn new(elements: [u32; 2]) -> Self {
        return Self {
            elements,
        }
    }

    pub fn ab(a: u32, b: u32) -> Self {
        return Self::new([a, b]);
    }

    pub fn a(&self) -> u32 {
        return self.elements[A];
    }

    pub fn set_a(&mut self, value: u32) {
        self.elements[A] = value;
    }

    pub fn b(&self) -> u32 {
        return self.elements[B];
    }

    pub fn set_b(&mut self, value: u32) {
        self.elements[B] = value;
    }

    pub fn as_i(&self) -> Tuple2i {
        return Tuple2i::ab(self.a() as i32, self.b() as i32);
    }

    pub fn as_f(&self) -> Tuple2f {
        return Tuple2f::ab(self.a() as f32, self.b() as f32);
    }

    pub fn add_scalar(&self, scalar: u32) -> Self {
        return Self::ab(self.a() + scalar, self.b() + scalar);
    }

    pub fn mul_ab(&self) -> u32 {
        return self.a() * self.b()
    }

    pub fn mul_scalar(&self, scalar: u32) -> Self {
        return Self::ab(
            self.a() * scalar,
            self.b() * scalar,
        );
    }

    pub fn div_scalar(&self, scalar: u32) -> Self {
        return Self::ab(
            self.a() / scalar,
            self.b() / scalar,
        );
    }
}

#[derive(Clone, Debug)]
pub struct Tuple2i {
    elements: [i32; 2],
}

impl Tuple2i {
    pub(super) fn new(elements: [i32; 2]) -> Self {
        return Self {
            elements,
        }
    }

    pub fn ab(a: i32, b: i32) -> Self {
        return Self::new([a, b]);
    }

    pub fn a(&self) -> i32 {
        return self.elements[A];
    }

    pub fn set_a(&mut self, value: i32) {
        self.elements[A] = value;
    }

    pub fn b(&self) -> i32 {
        return self.elements[B];
    }

    pub fn set_b(&mut self, value: i32) {
        self.elements[B] = value;
    }

    pub fn as_u(&self) -> Tuple2u {
        return Tuple2u::ab(self.a() as u32, self.b() as u32);
    }

    pub fn as_f(&self) -> Tuple2f {
        return Tuple2f::ab(self.a() as f32, self.b() as f32);
    }

    pub fn add_scalar(&self, scalar: i32) -> Self {
        return Self::ab(self.a() + scalar, self.b() + scalar);
    }

    pub fn mul_ab(&self) -> i32 {
        return self.a() * self.b()
    }

    pub fn mul_scalar(&self, scalar: i32) -> Self {
        return Self::ab(
            self.a() * scalar,
            self.b() * scalar,
        );
    }

    pub fn div_scalar(&self, scalar: i32) -> Self {
        return Self::ab(
            self.a() / scalar,
            self.b() / scalar,
        );
    }
}

#[derive(Clone, Debug)]
pub struct Tuple2f {
    elements: [f32; 2],
}

impl Tuple2f {
    pub(super) fn new(elements: [f32; 2]) -> Self {
        return Self {
            elements,
        }
    }

    pub fn ab(a: f32, b: f32) -> Self {
        return Self::new([a, b]);
    }

    pub fn a(&self) -> f32 {
        return self.elements[A];
    }

    pub fn set_a(&mut self, value: f32) {
        self.elements[A] = value;
    }

    pub fn b(&self) -> f32 {
        return self.elements[B];
    }

    pub fn set_b(&mut self, value: f32) {
        self.elements[B] = value;
    }

    pub fn as_u(&self) -> Tuple2u {
        return Tuple2u::ab(self.a() as u32, self.b() as u32);
    }

    pub fn as_i(&self) -> Tuple2i {
        return Tuple2i::ab(self.a() as i32, self.b() as i32);
    }

    pub fn add_scalar(&self, scalar: f32) -> Self {
        return Self::ab(self.a() + scalar, self.b() + scalar);
    }

    pub fn mul_ab(&self) -> f32 {
        return self.a() * self.b()
    }

    pub fn mul_scalar(&self, scalar: f32) -> Self {
        return Self::ab(
            self.a() * scalar,
            self.b() * scalar,
        );
    }

    pub fn div_scalar(&self, scalar: f32) -> Self {
        return Self::ab(
            self.a() / scalar,
            self.b() / scalar,
        );
    }
}

#[derive(Clone, Debug)]
pub struct Tuple3u {
    elements: [u32; 3],
}

impl Tuple3u {
    pub(super) fn new(elements: [u32; 3]) -> Self {
        return Self {
            elements,
        }
    }

    pub fn abc(a: u32, b: u32, c: u32) -> Self {
        return Self::new([a, b, c]);
    }

    pub fn a(&self) -> u32 {
        return self.elements[A];
    }

    pub fn set_a(&mut self, value: u32) {
        self.elements[A] = value;
    }

    pub fn b(&self) -> u32 {
        return self.elements[B];
    }

    pub fn set_b(&mut self, value: u32) {
        self.elements[B] = value;
    }

    pub fn c(&self) -> u32 {
        return self.elements[C];
    }

    pub fn set_c(&mut self, value: u32) {
        self.elements[C] = value;
    }

    pub fn as_i(&self) -> Tuple3i {
        return Tuple3i::abc(self.a() as i32, self.b() as i32, self.c() as i32);
    }

    pub fn as_f(&self) -> Tuple3f {
        return Tuple3f::abc(self.a() as f32, self.b() as f32, self.c() as f32);
    }

    pub fn add_scalar(&self, scalar: u32) -> Self {
        return Self::abc(self.a() + scalar, self.b() + scalar, self.c() + scalar);
    }

    pub fn mul_abc(&self) -> u32 {
        return self.a() * self.b() * self.c();
    }

    pub fn mul_scalar(&self, scalar: u32) -> Self {
        return Self::abc(
            self.a() * scalar,
            self.b() * scalar,
            self.c() * scalar,
        );
    }

    pub fn div_scalar(&self, scalar: u32) -> Self {
        return Self::abc(
            self.a() / scalar,
            self.b() / scalar,
            self.c() / scalar,
        );
    }

    /// Returns an array slice of u32 * abc = 12 bytes
    pub fn bytes(&self) -> &[u8] {
        unsafe {
            let (_, bytes, _) = self.elements.align_to::<u8>();
            return bytes;
        }
    }
}

#[derive(Clone, Debug)]
pub struct Tuple3i {
    elements: [i32; 3],
}

impl Tuple3i {
    pub(super) fn new(elements: [i32; 3]) -> Self {
        return Self {
            elements,
        }
    }

    pub fn abc(a: i32, b: i32, c: i32) -> Self {
        return Self::new([a, c, c]);
    }

    pub fn a(&self) -> i32 {
        return self.elements[A];
    }

    pub fn set_a(&mut self, value: i32) {
        self.elements[A] = value;
    }

    pub fn b(&self) -> i32 {
        return self.elements[B];
    }

    pub fn set_b(&mut self, value: i32) {
        self.elements[B] = value;
    }

    pub fn c(&self) -> i32 {
        return self.elements[C];
    }

    pub fn set_c(&mut self, value: i32) {
        self.elements[C] = value;
    }

    pub fn as_u(&self) -> Tuple3u {
        return Tuple3u::abc(self.a() as u32, self.b() as u32, self.c() as u32);
    }

    pub fn as_f(&self) -> Tuple3f {
        return Tuple3f::abc(self.a() as f32, self.b() as f32, self.c() as f32);
    }

    pub fn add_scalar(&self, scalar: i32) -> Self {
        return Self::abc(self.a() + scalar, self.b() + scalar, self.c() + scalar);
    }

    pub fn mul_abc(&self) -> i32 {
        return self.a() * self.b() * self.c();
    }

    pub fn mul_scalar(&self, scalar: i32) -> Self {
        return Self::abc(
            self.a() * scalar,
            self.b() * scalar,
            self.c() * scalar,
        );
    }

    pub fn div_scalar(&self, scalar: i32) -> Self {
        return Self::abc(
            self.a() / scalar,
            self.b() / scalar,
            self.c() / scalar,
        );
    }
}

#[derive(Clone, Debug)]
pub struct Tuple3f {
    elements: [f32; 3],
}

impl Tuple3f {
    pub(super) fn new(elements: [f32; 3]) -> Self {
        return Self {
            elements,
        }
    }

    pub fn abc(a: f32, b: f32, c: f32) -> Self {
        return Self::new([a, b, c]);
    }

    pub fn a(&self) -> f32 {
        return self.elements[A];
    }

    pub fn set_a(&mut self, value: f32) {
        self.elements[A] = value;
    }

    pub fn b(&self) -> f32 {
        return self.elements[B];
    }

    pub fn set_b(&mut self, value: f32) {
        self.elements[B] = value;
    }

    pub fn c(&self) -> f32 {
        return self.elements[C];
    }

    pub fn set_c(&mut self, value: f32) {
        self.elements[C] = value;
    }

    pub fn as_u(&self) -> Tuple3u {
        return Tuple3u::abc(self.a() as u32, self.b() as u32, self.c() as u32);
    }

    pub fn as_i(&self) -> Tuple3i {
        return Tuple3i::abc(self.a() as i32, self.b() as i32, self.c() as i32);
    }

    pub fn add_scalar(&self, scalar: f32) -> Self {
        return Self::abc(self.a() + scalar, self.b() + scalar, self.c() + scalar);
    }

    pub fn mul_abc(&self) -> f32 {
        return self.a() * self.b() * self.c();
    }

    pub fn mul_scalar(&self, scalar: f32) -> Self {
        return Self::abc(
            self.a() * scalar,
            self.b() * scalar,
            self.c() * scalar,
        );
    }

    pub fn div_scalar(&self, scalar: f32) -> Self {
        return Self::abc(
            self.a() / scalar,
            self.b() / scalar,
            self.c() / scalar,
        );
    }

    /// Returns an array slice of f32 * abc = 12 bytes
    pub fn bytes(&self) -> &[u8] {
        unsafe {
            let (_, bytes, _) = self.elements.align_to::<u8>();
            return bytes;
        }
    }
}

#[derive(Clone, Debug)]
pub struct Tuple4u {
    elements: [u32; 4],
}

impl Tuple4u {
    pub(super) fn new(elements: [u32; 4]) -> Self {
        return Self {
            elements,
        }
    }

    pub fn abcd(a: u32, b: u32, c: u32, d: u32) -> Self {
        return Self::new([a, b, c, d]);
    }

    pub fn a(&self) -> u32 {
        return self.elements[A];
    }

    pub fn set_a(&mut self, value: u32) {
        self.elements[A] = value;
    }

    pub fn b(&self) -> u32 {
        return self.elements[B];
    }

    pub fn set_b(&mut self, value: u32) {
        self.elements[B] = value;
    }

    pub fn c(&self) -> u32 {
        return self.elements[C];
    }

    pub fn set_c(&mut self, value: u32) {
        self.elements[C] = value;
    }

    pub fn d(&self) -> u32 {
        return self.elements[D];
    }

    pub fn set_d(&mut self, value: u32) {
        self.elements[D] = value;
    }

    pub fn as_i(&self) -> Tuple4i {
        return Tuple4i::abcd(self.a() as i32, self.b() as i32, self.c() as i32, self.d() as i32);
    }

    pub fn as_f(&self) -> Tuple4f {
        return Tuple4f::abcd(self.a() as f32, self.b() as f32, self.c() as f32, self.d() as f32);
    }

    pub fn add_scalar(&self, scalar: u32) -> Self {
        return Self::abcd(self.a() + scalar, self.b() + scalar, self.c() + scalar, self.d() + scalar);
    }

    pub fn mul_abcd(&self) -> u32 {
        return self.a() * self.b() * self.c() * self.d();
    }

    pub fn mul_scalar(&self, scalar: u32) -> Self {
        return Self::abcd(
            self.a() * scalar,
            self.b() * scalar,
            self.c() * scalar,
            self.d() * scalar,
        );
    }

    pub fn div_scalar(&self, scalar: u32) -> Self {
        return Self::abcd(
            self.a() / scalar,
            self.b() / scalar,
            self.c() / scalar,
            self.d() / scalar,
        );
    }
}

#[derive(Clone, Debug)]
pub struct Tuple4i {
    elements: [i32; 4],
}

impl Tuple4i {
    pub(super) fn new(elements: [i32; 4]) -> Self {
        return Self {
            elements,
        }
    }

    pub fn abcd(a: i32, b: i32, c: i32, d: i32) -> Self {
        return Self::new([a, b, c, d]);
    }

    pub fn a(&self) -> i32 {
        return self.elements[A];
    }

    pub fn set_a(&mut self, value: i32) {
        self.elements[A] = value;
    }

    pub fn b(&self) -> i32 {
        return self.elements[B];
    }

    pub fn set_b(&mut self, value: i32) {
        self.elements[B] = value;
    }

    pub fn c(&self) -> i32 {
        return self.elements[C];
    }

    pub fn set_c(&mut self, value: i32) {
        self.elements[C] = value;
    }

    pub fn d(&self) -> i32 {
        return self.elements[D];
    }

    pub fn set_d(&mut self, value: i32) {
        self.elements[D] = value;
    }

    pub fn as_u(&self) -> Tuple4u {
        return Tuple4u::abcd(self.a() as u32, self.b() as u32, self.c() as u32, self.d() as u32);
    }

    pub fn as_f(&self) -> Tuple4f {
        return Tuple4f::abcd(self.a() as f32, self.b() as f32, self.c() as f32, self.d() as f32);
    }

    pub fn add_scalar(&self, scalar: i32) -> Self {
        return Self::abcd(self.a() + scalar, self.b() + scalar, self.c() + scalar, self.d() + scalar);
    }

    pub fn mul_abcd(&self) -> i32 {
        return self.a() * self.b() * self.c() * self.d();
    }

    pub fn mul_scalar(&self, scalar: i32) -> Self {
        return Self::abcd(
            self.a() * scalar,
            self.b() * scalar,
            self.c() * scalar,
            self.d() * scalar,
        );
    }

    pub fn div_scalar(&self, scalar: i32) -> Self {
        return Self::abcd(
            self.a() / scalar,
            self.b() / scalar,
            self.c() / scalar,
            self.d() / scalar,
        );
    }
}

#[derive(Clone, Debug)]
pub struct Tuple4f {
    elements: [f32; 4],
}

impl Tuple4f {
    pub const BYTES: u32 = (std::mem::size_of::<f32>() * 4) as u32;

    pub(super) fn new(elements: [f32; 4]) -> Self {
        return Self {
            elements,
        }
    }

    pub fn abcd(a: f32, b: f32, c: f32, d: f32) -> Self {
        return Self::new([a, b, c, d]);
    }

    pub fn a(&self) -> f32 {
        return self.elements[A];
    }

    pub fn set_a(&mut self, value: f32) {
        self.elements[A] = value;
    }

    pub fn b(&self) -> f32 {
        return self.elements[B];
    }

    pub fn set_b(&mut self, value: f32) {
        self.elements[B] = value;
    }

    pub fn c(&self) -> f32 {
        return self.elements[C];
    }

    pub fn set_c(&mut self, value: f32) {
        self.elements[C] = value;
    }

    pub fn d(&self) -> f32 {
        return self.elements[D];
    }

    pub fn set_d(&mut self, value: f32) {
        self.elements[D] = value;
    }

    pub fn as_u(&self) -> Tuple4u {
        return Tuple4u::abcd(self.a() as u32, self.b() as u32, self.c() as u32, self.d() as u32);
    }

    pub fn as_i(&self) -> Tuple4i {
        return Tuple4i::abcd(self.a() as i32, self.b() as i32, self.c() as i32, self.d() as i32);
    }

    pub fn add_scalar(&self, scalar: f32) -> Self {
        return Self::abcd(self.a() + scalar, self.b() + scalar, self.c() + scalar, self.d() + scalar);
    }

    pub fn mul_abcd(&self) -> f32 {
        return self.a() * self.b() * self.c() * self.d();
    }

    pub fn mul_scalar(&self, scalar: f32) -> Self {
        return Self::abcd(
            self.a() * scalar,
            self.b() * scalar,
            self.c() * scalar,
            self.d() * scalar,
        );
    }

    pub fn div_scalar(&self, scalar: f32) -> Self {
        return Self::abcd(
            self.a() / scalar,
            self.b() / scalar,
            self.c() / scalar,
            self.d() / scalar,
        );
    }

    /// Returns an array slice of f32 * rgba = 16 bytes
    pub fn bytes(&self) -> &[u8] {
        unsafe {
            let (_, bytes, _) = self.elements.align_to::<u8>();
            return bytes;
        }
    }
}
