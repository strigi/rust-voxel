use std::fmt::{Display, Formatter, Result};
use std::ops::{Add, Sub, Mul, Div};

use super::tuple::Tuple4f;

#[derive(Debug, Clone)]
pub struct Color {
    components: Tuple4f,
}

#[allow(dead_code)]
impl Color {
    pub const BYTES: u32 = Tuple4f::BYTES;

    const MAX: f32 = 1.0;
    const MIN: f32 = 0.0;

    pub fn red() -> Self { Self::rgb(Color::MAX, Color::MIN, Color::MIN) }
    pub fn green() -> Self { Self::rgb(Color::MIN, Color::MAX, Color::MIN) }
    pub fn blue() -> Self { Self::rgb(Color::MIN, Color::MIN, Color::MAX) }
    pub fn cyan() -> Self { Self::rgb(Color::MIN, Color::MAX, Color::MAX) }
    pub fn magenta() -> Self { Self::rgb(Color::MAX, Color::MIN, Color::MAX) }
    pub fn yellow() -> Self { Self::rgb(Color::MAX, Color::MAX, Color::MIN) }
    pub fn white() -> Self { Self::rgb(Color::MAX, Color::MAX, Color::MAX) }
    pub fn black() -> Self { Self::rgb(Color::MIN, Color::MIN, Color::MIN) }
    pub fn gray() -> Self { Self::rgb(Color::MAX / 2.0, Color::MAX / 2.0, Color::MAX / 2.0) }

    pub fn rgba(red: f32, green: f32, blue: f32, alpha: f32) -> Self {
        assert!(red >= Color::MIN && red <= Color::MAX);
        assert!(green >= Color::MIN && green <= Color::MAX);
        assert!(blue >= Color::MIN && blue <= Color::MAX);
        assert!(alpha >= Color::MIN && alpha <= Color::MAX);

        return Self::from_array([red, green, blue, alpha]);
    }

    pub fn rgb(red: f32, green: f32, blue: f32) -> Self {
        return Self::rgba(red, green, blue, Color::MAX);
    }

    pub fn hsl(red: f32, green: f32, blue: f32) -> Self {
        return Self::hsla(red, green, blue, 1.0);
    }

    pub fn hsla(hue: f32, saturation: f32, lightness: f32, alpha: f32) -> Self {
        Color::assert_range(saturation);
        Color::assert_range(lightness);

        // Ensure 0 <= hue < 360
        const DEGREES_PER_CIRCLE: f32 = 360.0;
        let mut clamped_hue = hue;
        while clamped_hue >= DEGREES_PER_CIRCLE {
            clamped_hue -= DEGREES_PER_CIRCLE
        }
        while clamped_hue < 0.0 {
            clamped_hue += DEGREES_PER_CIRCLE
        }

        let c = 1.0 - (2.0 * lightness - 1.0).abs() * saturation;
        let x = c * ((1 - (hue / 60.0) as i32 % 2 - 1).abs()) as f32;
        let m = lightness - c / 2.0;

        let cm = c + m;
        let xm = x + m;

        if clamped_hue < 60.0 {
            return Self::rgba(cm, xm, 0.0, alpha);
        } else if clamped_hue < 120.0 {
            return Self::rgba(xm, cm, 0.0, alpha);
        } else if clamped_hue < 180.0 {
            return Self::rgba(0.0, cm, xm, alpha);
        } else if clamped_hue < 240.0 {
            return Self::rgba(0.0, xm, cm, alpha);
        } else if clamped_hue < 300.0 {
            return Self::rgba(xm, 0.0, cm, alpha);
        } else { // < 360.0
            return Self::rgba(cm, 0.0, xm, alpha);
        }
    }

    fn from_array(components: [f32; 4]) -> Self {
        for component in components {
            assert!(component >= Color::MIN && component <= Color::MAX);
        }
        return Self {
            components: Tuple4f::new(components)
        };
    }

    fn clamp_down(value: f32) -> f32 {
        return value.min(Color::MAX);
    }

    fn clamp_up(value: f32) -> f32 {
        return value.max(Color::MIN);
    }

    fn assert_range(value: f32) {
        Color::assert_min(value);
        Color::assert_max(value);
    }

    fn assert_min(value: f32) {
        assert!(value >= Color::MIN);
    }

    fn assert_max(value: f32) {
        assert!(value <= Color::MAX);
    }

    pub fn r(&self) -> f32 {
        return self.components.a();
    }

    pub fn g(&self) -> f32 {
        return self.components.b();
    }

    pub fn b(&self) -> f32 {
        return self.components.c();
    }

    pub fn a(&self) -> f32 {
        return self.components.d();
    }

    pub fn average(&self, rhs: &Color) -> Color {
        let weight = 1.0; // any two values that are the same will do.
        return self.weighted_average(weight, rhs, weight);
    }

    pub fn weighted_average(&self, self_weight: f32, rhs: &Color, rhs_weight: f32) -> Color {
        let denominator = self_weight + rhs_weight;
        return &(self * (self_weight / denominator)) + &(rhs * (rhs_weight / denominator));
    }

    pub fn negative(&self) -> Self {
        let red = (self.r() - 1.0).abs();
        let green = (self.g() - 1.0).abs();
        let blue = (self.b() - 1.0).abs();
        return Self::rgba(red, green, blue, self.a())
    }

    pub fn bytes(&self) -> &[u8] {
        return self.components.bytes();
    }
}

impl Add<&Color> for &Color {
    type Output = Color;

    fn add(self, rhs: &Color) -> Self::Output {
        return Color::rgba(
            Color::clamp_down(self.r() + rhs.r()),
            Color::clamp_down(self.g() + rhs.g()),
            Color::clamp_down(self.b() + rhs.b()),
            Color::clamp_down(self.a() + rhs.a())
        );
    }
}

impl Sub<&Color> for &Color {
    type Output = Color;

    fn sub(self, rhs: &Color) -> Self::Output {
        return Color::rgba(
            Color::clamp_up(self.r() - rhs.r()),
            Color::clamp_up(self.g() - rhs.g()),
            Color::clamp_up(self.b() - rhs.b()),
            Color::clamp_up(self.a() - rhs.a()),
        );
    }
}

impl Mul<f32> for &Color {
    type Output = Color;

    fn mul(self, rhs: f32) -> Self::Output {
        assert!(rhs >= 0.0);
        return Color::rgba(
            Color::clamp_down(self.r() * rhs),
            Color::clamp_down(self.g() * rhs),
            Color::clamp_down(self.b() * rhs),
            // Color::clamp_down(self.a() * rhs),
            self.a()
        );
    }
}

impl Div<f32> for &Color {
    type Output = Color;

    fn div(self, rhs: f32) -> Self::Output {
        return self * (1.0 / rhs); // Implement division as the inverse of multiplication
    }
}

impl Div for &Color {
    type Output = Color;

    fn div(self, rhs: Self) -> Self::Output {
        return Color::red();
    }
}

impl Display for Color {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        const PRECISION: usize = 3;
        let red = self.r();
        let green = self.g();
        let blue = self.b();
        let alpha = self.a();
        write!(f, "rgba({red:.PRECISION$}, {green:.PRECISION$}, {blue:.PRECISION$}, {alpha:.PRECISION$})")
    }
}
