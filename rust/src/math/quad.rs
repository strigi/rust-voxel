use crate::math::Matrix4;
use crate::math::vector::Vector3;

#[derive(Clone)]
pub struct Quad {
    pub a: Vector3,
    pub b: Vector3,
    pub c: Vector3,
    pub d: Vector3,
    pub color: Vector3,
}

impl Quad {
    pub fn new(a: Vector3, b: Vector3, c: Vector3, d: Vector3, color: Vector3) -> Self {
        return Self { a, b, c, d, color };
    }

    pub fn transform(&self, matrix: &Matrix4) -> Self {
        let ta = matrix.mul_vec(&self.a.expand()).contract();
        let tb = matrix.mul_vec(&self.b.expand()).contract();
        let tc = matrix.mul_vec(&self.c.expand()).contract();
        let td = matrix.mul_vec(&self.d.expand()).contract();
        return Self { a: ta, b: tb, c: tc, d: td, color: self.color.clone() };
    }

    pub fn normal(&self) -> Vector3 {
        return Vector3::normal(&self.a, &self.b, &self.c);
    }
}