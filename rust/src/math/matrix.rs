use std::fmt::Result;
use std::fmt::Display;
use std::fmt::Formatter;
use std::mem::size_of;

use super::vector::Vector;


pub struct Matrix<const AREA: usize, const DIMENSION: usize> {
    entries: [f32; AREA],
}

pub type Matrix4 = Matrix<16, 4>;
pub type Matrix3 = Matrix<9, 3>;

impl <const AREA: usize, const DIMENSION: usize> Matrix<AREA, DIMENSION> {
    pub const LENGTH_BYTES: usize = DIMENSION * DIMENSION * size_of::<f32>();

    pub fn new(entries: [f32; AREA]) -> Self {
        return Self {
            entries
        }
    }

    pub fn null() -> Self {
        return Self {
            entries: ([0.0; AREA]),
        }
    }

    pub fn identity() -> Self {
        let mut matrix = Self::null();
        for i in 0..DIMENSION as u8 {
            matrix.set(i, i, 1.0);
        }
        return matrix;
    }

    pub fn get(&self, row: u8, column: u8) -> f32 {
        return self.entries[self.index(row, column)];
    }

    pub fn set(&mut self, row: u8, column: u8, value: f32) {
        self.entries[self.index(row, column)] = value;
    }

    fn index(&self, row: u8, column: u8) -> usize {
        // WGPU's Buffer layout to map to shader's matAxB datatypes assumes matrices are in column major form
        // when using row major form all matrices would effectively be seen in transposed from from the shader.
        return self.index_column_major(row, column);
    }

    fn index_row_major(&self, row: u8, column: u8) -> usize {
        return row as usize * DIMENSION + column as usize
    }

    fn index_column_major(&self, row: u8, column: u8) -> usize {
        return row as usize + column as usize * DIMENSION;
    }

    pub fn get_row(&self, row: u8) -> Vector<DIMENSION> {
        let mut entries = [0f32; DIMENSION];
        for i in 0..DIMENSION {
            entries[i] = self.get(row, i as u8);
        }
        return Vector::new(entries);
    }

    pub fn get_column(&self, column: u8) -> Vector<DIMENSION> {
        let mut entries = [0f32; DIMENSION];
        for i in 0..DIMENSION {
            entries[i] = self.get(i as u8, column);
        }
        return Vector::new(entries);
    }

    pub fn mul_vec(&self, vector: &Vector<DIMENSION>) -> Vector<DIMENSION> {
        let mut result = Vector::null();
        for i in 0..DIMENSION as u8 {
            result.set(i, self.get_row(i).dot(vector));
        }
        return result;
    }

    pub fn mul(&self, other: &Matrix<AREA, DIMENSION>) -> Matrix<AREA, DIMENSION> {
        let mut result = Self::null();
        for row in 0..DIMENSION as u8 {
            for column in 0..DIMENSION as u8 {
                result.set(row, column, self.get_row(row).dot(&other.get_column(column)))
            }
        }
        return result
    }

    pub fn mul_scalar(&self, scalar: f32) -> Matrix<AREA, DIMENSION> {
        let mut result = Self::null();
        for row in 0..DIMENSION as u8 {
            for column in 0..DIMENSION as u8 {
                result.set(row, column, self.get(row, column) * scalar)
            }
        }
        return result;
    }
}

impl <const AREA: usize, const DIMENSION: usize> Display for Matrix<AREA, DIMENSION> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        let mut result = Ok(());
        for row in 0..DIMENSION as u8 {
            for column in 0..DIMENSION as u8 {
                let entry = self.get(row, column);
                result = result.and(write!(f, "{:7.3}", entry))
            }
            result = result.and(writeln!(f));
        }
        return result;
    }
}

impl Matrix4 {
    pub fn bytes(&self) -> &[u8] {
        unsafe {
            let (_, bytes, _) = self.entries.align_to::<u8>();
            return bytes;
        }
    }

    pub fn translate_xyz(x: f32, y: f32, z: f32) -> Self {
        let mut matrix = Matrix4::identity();
        matrix.set(0, 3, x);
        matrix.set(1, 3, y);
        matrix.set(2, 3, z);
        return matrix;
    }

    pub fn rotate_x(angle_deg: f32) -> Self {
        let mut matrix = Self::identity();
        let cos = angle_deg.to_radians().cos();
        let sin = angle_deg.to_radians().sin();
        matrix.set(1, 1, cos);
        matrix.set(1, 2, -sin);
        matrix.set(2, 1, sin);
        matrix.set(2, 2, cos);
        return matrix;
    }

    pub fn rotate_y(angle_deg: f32) -> Self {
        let mut matrix = Self::identity();
        let cos = angle_deg.to_radians().cos();
        let sin = angle_deg.to_radians().sin();
        matrix.set(0, 0, cos);
        matrix.set(0, 2, sin);
        matrix.set(2, 0, -sin);
        matrix.set(2, 2, cos);
        return matrix;
    }

    pub fn rotate_z(angle_deg: f32) -> Self {
        let mut matrix = Self::identity();
        let cos = angle_deg.to_radians().cos();
        let sin = angle_deg.to_radians().sin();
        matrix.set(0, 0, cos);
        matrix.set(0, 1, -sin);
        matrix.set(1, 0, sin);
        matrix.set(1, 1, cos);
        return matrix;
    }

    pub fn rotate_xyz(x_angle_deg: f32, y_angle_deg: f32, z_angle_deg: f32) -> Self {
        return Self::rotate_x(x_angle_deg).mul(&Self::rotate_y(y_angle_deg)).mul(&Self::rotate_z(z_angle_deg));
    }

    pub fn scale_xyz(scale_x: f32, scale_y: f32, scale_z: f32) -> Self {
        let mut m = Matrix4::identity();
        m.set(0, 0, scale_x);
        m.set(1, 1, scale_y);
        m.set(2, 2, scale_z);
        return m;
    }
}

#[test]
fn foo() {
    println!("LEFT\n{}", Matrix4::rotate_xyz(90.0, 0.0, 0.0));
    println!("LEFT MOVE\n{}", Matrix4::rotate_xyz(90.0, 0.0, 0.0).mul(&Matrix4::translate_xyz(10.0, 0.0, 0.0)));
}