mod matrix;
pub use matrix::Matrix4;

mod vector;
pub use vector::Point2;
pub use vector::Vector3;
pub use vector::Point3;
pub use vector::Vector4;

mod color;
pub use color::Color;

mod tuple;
pub use tuple::Tuple4u;
pub use tuple::Tuple3u;
pub use tuple::Tuple2u;
pub use tuple::Tuple4i;
pub use tuple::Tuple3i;
pub use tuple::Tuple2i;
pub use tuple::Tuple4f;
pub use tuple::Tuple3f;
pub use tuple::Tuple2f;

pub mod triangle;
pub mod quad;

mod dimensions;
pub use dimensions::Dimensions3u;
pub use dimensions::Dimensions3f;
pub use dimensions::Dimensions2u;
pub use dimensions::Dimensions2f;
