@group(0) @binding(0) var<uniform> perspective: mat4x4<f32>;
@group(1) @binding(0) var<uniform> modelView: mat4x4<f32>;

struct VertexOutput {
    @builtin(position) screenPosition: vec4<f32>,
    @location(0) worldPosition: vec3<f32>,
    @location(1) color: vec4<f32>,
}

@vertex
fn vmain(@location(0) vertex: vec3<f32>, @location(1) color: vec4<f32>) -> VertexOutput {
    var homogeneousVertex = vec4<f32>(vertex, 1);

    var output: VertexOutput;
    output.screenPosition = perspective * modelView * homogeneousVertex;
    output.worldPosition = (modelView * homogeneousVertex).xyz / homogeneousVertex.w;
    output.color = color;
    return output;
}

struct FragmentInput {
    @location(0) worldPosition: vec3<f32>,
    @location(1) color: vec4<f32>,
}

@fragment
fn fmain(input: FragmentInput) -> @location(0) vec4<f32> {
    var red = vec4<f32>(1, 0, 0, 1);
    var green = vec4<f32>(0, 1, 0, 1);
    var blue = vec4<f32>(0, 0, 1, 1);
    var white = vec4<f32>(1, 1, 1, 1);

    var x = dpdx(input.worldPosition);
    var y = dpdy(input.worldPosition);
    var c = cross(x, y);
    var surfaceNormal = normalize(c);

    // https://en.wikipedia.org/wiki/Lambertian_reflectance#Use_in_computer_graphics
    var lightPosition = vec3<f32>(0, 0, -10);
    var lightDirection = normalize(lightPosition - input.worldPosition);
    var lightColor = input.color;
    var intensity = max(dot(lightDirection, surfaceNormal), 0.0);

    var diffuseColor = lightColor * intensity;
    var ambientColor = input.color * 0.25;

    return diffuseColor + ambientColor;
}
