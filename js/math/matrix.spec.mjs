import {beforeEach, describe, expect, test} from 'vitest'
import {Matrix4} from './matrix.mjs'
import {Vector4} from './vector.mjs'

describe('Matrix4', () => {
    describe('static SIZE', () => {
        test('equals 4', () => {
            expect(Matrix4.SIZE).toBe(4)
        })
    })

    describe('static LENGTH', () => {
        test('equals 16', () => {
            expect(Matrix4.LENGTH).toBe(16)
        })
    })

    describe('static BYTES', () => {
        test('equals 64', () => {
            expect(Matrix4.BYTES).toBe(64)
        })
    })

    describe('static identity()', () => {
        test('initialized correctly', () => {
            expect(Matrix4.identity().toArray()).toEqual([
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1,
            ])
        })
    })

    describe('static scale()', () => {
        test('initialized correctly', () => {
            expect(Matrix4.scale(1, 2, 3).toArray()).toEqual([
                1, 0, 0, 0,
                0, 2, 0, 0,
                0, 0, 3, 0,
                0, 0, 0, 1,
            ])
        })
    })

    describe('static translate()', () => {
        test('initialized correctly', () => {
            expect(Matrix4.translate(5, 6, 7).toArray({ mode: 'row-major' })).toEqual([
                1, 0, 0, 5,
                0, 1, 0, 6,
                0, 0, 1, 7,
                0, 0, 0, 1
            ])
        })
    })

    describe('static rotate()', () => {
        const X_UNIT = new Vector4(1, 0, 0, 0)
        const Y_UNIT = new Vector4(0, 1, 0, 0)
        const Z_UNIT = new Vector4(0, 0, 1, 0)

        test('rotating x axis 90 degrees over z axis results in y axis', () => {
            const v = Matrix4.rotate(0, 0, 90).mul(X_UNIT)
            expect(v[0]).toBeCloseTo(0)
            expect(v[1]).toBeCloseTo(1)
            expect(v[2]).toBeCloseTo(0)
            expect(v[3]).toBeCloseTo(0)
        })

        test('rotating x axis 90 degrees over y axis results in z axis', () => {
            const v = Matrix4.rotate(0, 90, 0).mul(X_UNIT)
            expect(v[0]).toBeCloseTo(0)
            expect(v[1]).toBeCloseTo(0)
            expect(v[2]).toBeCloseTo(1)
            expect(v[3]).toBeCloseTo(0)
        })

        test('rotating y axis 90 degrees over x axis results in z axis', () => {
            const v = Matrix4.rotate(90, 0, 0).mul(Y_UNIT)
            expect(v[0]).toBeCloseTo(0)
            expect(v[1]).toBeCloseTo(0)
            expect(v[2]).toBeCloseTo(1)
            expect(v[3]).toBeCloseTo(0)
        })

        test('rotating y axis 90 degrees over z axis results in x axis', () => {
            const v = Matrix4.rotate(0, 0, 90).mul(Y_UNIT)
            expect(v[0]).toBeCloseTo(1)
            expect(v[1]).toBeCloseTo(0)
            expect(v[2]).toBeCloseTo(0)
            expect(v[3]).toBeCloseTo(0)
        })

        test('rotating z axis 90 degrees over x axis results in y axis', () => {
            const v = Matrix4.rotate(90, 0, 0).mul(Z_UNIT)
            expect(v[0]).toBeCloseTo(0)
            expect(v[1]).toBeCloseTo(1)
            expect(v[2]).toBeCloseTo(0)
            expect(v[3]).toBeCloseTo(0)
        })

        test('rotating z axis 90 degrees over y axis results in x axis', () => {
            const v = Matrix4.rotate(0, 90, 0).mul(Z_UNIT)
            expect(v[0]).toBeCloseTo(1)
            expect(v[1]).toBeCloseTo(0)
            expect(v[2]).toBeCloseTo(0)
            expect(v[3]).toBeCloseTo(0)
        })
    })

    describe('constructor()', () => {
        test('initializes with zeros if data is not defined', () => {
            const m = new Matrix4()
            for(let r = 0; r < Matrix4.SIZE; r++) {
                for(let c = 0; c < Matrix4.SIZE; c++) {
                    expect(m.get(r, c)).toBe(0)
                }
            }
        })

        test('throws if data is not an array', () => {
            expect(() => new Matrix4('Hello')).toThrow(`Field 'data' with value 'Hello' of type 'string' must be an Array`)
        })

        test('throws if data does not have length 16', () => {
            expect(() => new Matrix4([0, 1, 2, 3])).toThrow(`Field 'data' with value '0,1,2,3' instance of 'Array' length '4' must be '16'`)
        })

        test('throws if data does not have numbers', () => {
            const a = new Array(16).fill('a')
            expect(() => new Matrix4(a)).toThrow(`Field 'values[0]' with value 'a' of type 'string' must be of type 'number'`)
        })

        test('initializes with zeros if data is not defined', () => {
            const m = new Matrix4()
            for(let r = 0; r < Matrix4.SIZE; r++) {
                for(let c = 0; c < Matrix4.SIZE; c++) {
                    expect(m.get(r, c)).toBe(0)
                }
            }
        })
    })

    describe('mul()', () => {
        let a
        let b

        beforeEach(() => {
            a = Matrix4.fromRowMajor([
                5, 7, 9, 10,
                2, 3, 3, 8,
                8, 10, 2, 3,
                3, 3, 4, 8
            ])

            b = Matrix4.fromRowMajor([
                3, 10, 12, 18,
                12, 1, 4, 9,
                9, 10, 12, 2,
                3, 12, 4, 10
            ])
        })

        test('returns matrix multiplied with matrix if given a matrix', () => {
            const p = a.mul(b)
            expect(p.get(0, 0)).toBe(210)
            expect(p.get(0, 1)).toBe(267)
            expect(p.get(0, 2)).toBe(236)
            expect(p.get(0, 3)).toBe(271)
            expect(p.get(1, 0)).toBe(93)
            expect(p.get(1, 1)).toBe(149)
            expect(p.get(1, 2)).toBe(104)
            expect(p.get(1, 3)).toBe(149)
            expect(p.get(2, 0)).toBe(171)
            expect(p.get(2, 1)).toBe(146)
            expect(p.get(2, 2)).toBe(172)
            expect(p.get(2, 3)).toBe(268)
            expect(p.get(3, 0)).toBe(105)
            expect(p.get(3, 1)).toBe(169)
            expect(p.get(3, 2)).toBe(128)
            expect(p.get(3, 3)).toBe(169)
        })

        test('returns vector multiplied with matrix if given a vector', () => {
            const m = Matrix4.fromRowMajor([
                1, 2, 1, 2,
                2, 1, 2, 1,
                1, 2, 1, 2,
                2, 1, 2, 1
            ])
            const vin = new Vector4(1, 2, 3, 4)
            const vout = m.mul(vin)
            expect(vout).toHaveProperty('x', 16)
            expect(vout).toHaveProperty('y', 14)
            expect(vout).toHaveProperty('z', 16)
            expect(vout).toHaveProperty('w', 14)
        })

        test('returns matrix multiplied with scalar if given a number', () => {
            const m = Matrix4.fromRowMajor([
                1, 2, 1, 2,
                2, 1, 2, 1,
                1, 2, 1, 2,
                2, 1, 2, 1
            ])
            const s = 2
            expect(m.mul(s).toArray({ mode: 'row-major' })).toEqual([
                2, 4, 2, 4,
                4, 2, 4, 2,
                2, 4, 2, 4,
                4, 2, 4, 2,
            ])
        })

        test('does not modify this matrix if given a matrix', () => {
            a.mul(b)
            expect(a.toArray({ mode: 'row-major' })).toEqual([
                5, 7, 9, 10,
                2, 3, 3, 8,
                8, 10, 2, 3,
                3, 3, 4, 8
            ])
        })

        test('does not modify that matrix if given a matrix', () => {
            a.mul(b)
            expect(b.toArray({ mode: 'row-major' })).toEqual([
                3, 10, 12, 18,
                12, 1, 4, 9,
                9, 10, 12, 2,
                3, 12, 4, 10
            ])
        })
    })

    describe('toArray()', () => {
        test('returns array of all elements in column major form by default', () => {
            expect(Matrix4.fromRowMajor([
                11, 12, 13, 14,
                21, 22, 23, 24,
                31, 32, 33, 34,
                41, 42, 43, 44,
            ]).toArray()).toEqual([11, 21, 31, 41, 12, 22, 32, 42, 13, 23, 33, 43, 14, 24, 34, 44])
        })

        test('returns array of all elements in column major form if mode is column-major', () => {
            expect(Matrix4.fromRowMajor([
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 8, 7, 6,
                5, 4, 3, 2
            ]).toArray({mode: 'column-major'})).toEqual([1, 5, 9, 5, 2, 6, 8, 4, 3, 7, 7, 3, 4, 8, 6, 2])
        })

        test('returns array of all elements in row major form if mode is row-major', () => {
            expect(Matrix4.fromRowMajor([
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 8, 7, 6,
                5, 4, 3, 2
            ]).toArray({ mode: 'row-major'})).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2])
        })
    })

    describe('entries()', () => {
        test('produces iterator with entry value, row and column', () => {
            const m = Matrix4.fromRowMajor([
                10, 11, 12, 13,
                14, 15, 16, 17,
                18, 19, 20, 21,
                22, 23, 24, 25
            ])
            const iterator = m.entries()
            expect(iterator.next().value).toEqual([10, 0, 0])
            expect(iterator.next().value).toEqual([14, 1, 0])
            expect(iterator.next().value).toEqual([18, 2, 0])
            expect(iterator.next().value).toEqual([22, 3, 0])
            
            expect(iterator.next().value).toEqual([11, 0, 1])
            expect(iterator.next().value).toEqual([15, 1, 1])
            expect(iterator.next().value).toEqual([19, 2, 1])
            expect(iterator.next().value).toEqual([23, 3, 1])
            
            expect(iterator.next().value).toEqual([12, 0, 2])
            expect(iterator.next().value).toEqual([16, 1, 2])
            expect(iterator.next().value).toEqual([20, 2, 2])
            expect(iterator.next().value).toEqual([24, 3, 2])
            
            expect(iterator.next().value).toEqual([13, 0, 3])
            expect(iterator.next().value).toEqual([17, 1, 3])
            expect(iterator.next().value).toEqual([21, 2, 3])
            expect(iterator.next().value).toEqual([25, 3, 3])
            expect(iterator.next().done).toBe(true)
        })
    })

    describe('transpose()', () => {
        test('does not modify this', () => {
            const m = Matrix4.fromRowMajor([
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 10, 11, 12,
                13, 14, 15, 16
            ])
            m.transpose()

            expect(m.get(0, 0)).toBe(1)
            expect(m.get(0, 1)).toBe(2)
            expect(m.get(0, 2)).toBe(3)
            expect(m.get(0, 3)).toBe(4)
            expect(m.get(1, 0)).toBe(5)
            expect(m.get(1, 1)).toBe(6)
            expect(m.get(1, 2)).toBe(7)
            expect(m.get(1, 3)).toBe(8)
            expect(m.get(2, 0)).toBe(9)
            expect(m.get(2, 1)).toBe(10)
            expect(m.get(2, 2)).toBe(11)
            expect(m.get(2, 3)).toBe(12)
            expect(m.get(3, 0)).toBe(13)
            expect(m.get(3, 1)).toBe(14)
            expect(m.get(3, 2)).toBe(15)
            expect(m.get(3, 3)).toBe(16)
        })

        test('returns transposed matrix (rows become columns and visa versa)', () => {
            const t = Matrix4.fromRowMajor([
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 10, 11, 12,
                13, 14, 15, 16
            ]).transpose()

            expect(t.get(0, 0)).toBe(1)
            expect(t.get(0, 1)).toBe(5)
            expect(t.get(0, 2)).toBe(9)
            expect(t.get(0, 3)).toBe(13)
            expect(t.get(1, 0)).toBe(2)
            expect(t.get(1, 1)).toBe(6)
            expect(t.get(1, 2)).toBe(10)
            expect(t.get(1, 3)).toBe(14)
            expect(t.get(2, 0)).toBe(3)
            expect(t.get(2, 1)).toBe(7)
            expect(t.get(2, 2)).toBe(11)
            expect(t.get(2, 3)).toBe(15)
            expect(t.get(3, 0)).toBe(4)
            expect(t.get(3, 1)).toBe(8)
            expect(t.get(3, 2)).toBe(12)
            expect(t.get(3, 3)).toBe(16)
        })
    })

    describe('rows()', () => {
        test('returns an iterator of the rows of this matrix', () => {
            const m = Matrix4.fromRowMajor([
                11, 12, 13, 14,
                21, 22, 23, 24,
                31, 32, 33, 34,
                41, 42, 43, 44,
            ])

            const rows = [...m.rows()]
            expect(rows[0]).toEqual(new Vector4(11, 12, 13, 14))
            expect(rows[1]).toEqual(new Vector4(21, 22, 23, 24))
            expect(rows[2]).toEqual(new Vector4(31, 32, 33, 34))
            expect(rows[3]).toEqual(new Vector4(41, 42, 43, 44))
            expect(rows.length).toBe(4)
        })
    })

    describe('columns()', () => {
        test('returns an iterator of the columns of this matrix', () => {
            const m = Matrix4.fromRowMajor([
                11, 12, 13, 14,
                21, 22, 23, 24,
                31, 32, 33, 34,
                41, 42, 43, 44,
            ])

            const columns = [...m.columns()]
            expect(columns[0]).toEqual(new Vector4(11, 21, 31, 41))
            expect(columns[1]).toEqual(new Vector4(12, 22, 32, 42))
            expect(columns[2]).toEqual(new Vector4(13, 23, 33, 43))
            expect(columns[3]).toEqual(new Vector4(14, 24, 34, 44))
            expect(columns.length).toBe(4)
        })
    })
})