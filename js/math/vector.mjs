import field from '../utils/validate.mjs'

class VectorBase {
    /**
     * Aliases for getters to be generated for each component.
     * For example components x,y,z,w are the same as r,g,b,a respectively
     * This allows more semantic referencing to the vector's components depending on the usecase (point, vector, color, textures)
     * @type {string[]}
     */
    static COMPONENT_ALIASESES = ['0xrs', '1ygt', '2zbp', '3waq']

    #components

    constructor(...values) {
        this.#components = new Float32Array(field('values').assert_array()(values).length)
        for(let i = 0; i < values.length; i++) {
            this.#components[i] = field(`values[${i}]`).assert_number()(values.map(v => v ?? 0)[i])
            const aliases = VectorBase.COMPONENT_ALIASESES[i]
            for(const alias of aliases) {
                createAccessor(this, alias, i)
            }
        }

        function createAccessor(object, name, index) {
            Object.defineProperty(object, name, {
                get: () => object.#components[index]
            })
        }
    }

    get length() {
        return this.constructor.LENGTH
    }

    get magnitude() {
        let sum = 0
        for(let i = 0; i < this.#components.length; i++) {
            sum += this.#components[i] * this.#components[i]
        }
        return Math.sqrt(sum)
    }

    [Symbol.iterator]() {
        return this.#components[Symbol.iterator]()
    }

    toString() {
        return '(' + [...this].map(e => e.toFixed(3)).join(', ') + ')'
    }

    /**
     * @param that {Vector3 | number}
     * @return {Vector3}
     */
    add(that) {
        if(typeof that === 'number') {
            return this.#add_number(that)
        } else if(typeof that === 'object' && that instanceof this.constructor) {
            return this.#add_vector(that)
        } else {
            throw new Error(`Unable to add ${that}`)
        }
    }

    /**
     * @param that {Vector3 | number}
     * @return {Vector3}
     */
    subtract(that) {
        if(typeof that === 'number') {
            return this.#add_number(-that)
        } else if( typeof that === 'object' && that instanceof this.constructor) {
            return this.#add_vector(that.scale(-1))
        } else {
            throw new Error(`Unable to add ${that}`)
        }
    }

    #add_vector(vector) {
        const other = new this.constructor()
        for(let i = 0; i < this.length; i++) {
            other.#components[i] = this[i] + vector[i]
        }
        return other
    }

    #add_number(number) {
        const other = new this.constructor()
        for(let i = 0; i < this.length; i++) {
            other.#components[i] = this[i] + number
        }
        return other
    }

    dot(that) {
        field().assert_instance(this.constructor)(that)
        let sum = 0
        for(let i = 0; i < this.length; i++) {
            sum += this[i] * that[i]
        }
        return sum
    }

    /**
     * Multiplies this vector with a scalar.
     * @param scalar {number}
     * @return {VectorBase}
     */
    scale(scalar) {
        field('scalar').assert_number()(scalar)

        const vector = new this.constructor()
        for(let i = 0; i < this.constructor.LENGTH; i += 1) {
            vector.#components[i] = this[i] * scalar
        }

        return vector;
    }
}

export class Vector2 extends VectorBase {
    static LENGTH = 2

    static BYTES = Float32Array.BYTES_PER_ELEMENT * Vector2.LENGTH

    constructor(x, y) {
        super(x, y)
    }
}

export class Vector3 extends VectorBase {
    static LENGTH = 3

    static BYTES = Float32Array.BYTES_PER_ELEMENT * Vector3.LENGTH

    constructor(x, y, z) {
        super(x, y, z)
    }

    cross(that) {
        return new Vector3(
            this.y * that.z - this.z * that.y,
            this.z * that.x - this.x * that.z,
            this.x * that.y - this.y * that.x
        )
    }
}

export class Vector4 extends VectorBase {
    static LENGTH = 4

    static BYTES = Float32Array.BYTES_PER_ELEMENT * Vector4.LENGTH

    constructor(x, y, z, w) {
        super(x, y, z, w)
    }
}
