import {beforeEach, describe, expect, test} from 'vitest'

import {Vector2, Vector3, Vector4} from './vector.mjs'

describe('Vector2', () => {
    describe('constructor()', () => {
        test('initializes coordinates', () => {
            const v = new Vector2(1, 2)

            expect(v[0]).toBe(1)
            expect(v.x).toBe(1)
            expect(v.s).toBe(1)

            expect(v[1]).toBe(2)
            expect(v.y).toBe(2)
            expect(v.t).toBe(2)
        })

        test('initializes defaults to zero', () => {
            const v = new Vector2()
            expect(v[0]).toBe(0)
            expect(v[1]).toBe(0)
        })
    })

    describe('static SIZE', () => {
        test('should be one for each component = 2', () => {
            expect(Vector2.LENGTH).toBe(2)
        })
    })

    describe('static BYTES', () => {
        test('should be SIZE * 4 bytes per 32bit float = 12', () => {
            expect(Vector2.BYTES).toBe(8)
        })
    })

    describe('length', () => {
        test('is 2', () => {
            expect(new Vector2()).toHaveLength(2)
        })

        test('is read-only', () => {
            expect(() => new Vector2().length = 5).toThrow('has only a getter')
        })
    })

    describe('magnitude', () => {
        test('is the square root of the square of each component', () => {
            expect(new Vector2(2, 3).magnitude).toBeCloseTo(3.606)
        })

        test('is zero when all components are zero', () => {
            expect(new Vector3(0, 0).magnitude).toBe(0)
        })

        test('is read-only', () => {
            expect(() => new Vector2().magnitude = 5).toThrow('has only a getter')
        })
    })

    describe('toString()', () => {
        test('returns (x, y)', () => {
            expect(new Vector2(1.2345, 2.3456, 3.4567).toString()).toBe('(1.235, 2.346)')
        })
    })

    describe('[Symbol.iterator]', () => {
        test('can be iterated over', () => {
            const v = new Vector2(10, 20)
            const results = []
            for (const e of v) {
                results.push(e)
            }
            expect(results).toEqual([10, 20])
        })
    })

    describe('add()', () => {
        let a, b

        beforeEach(() => {
            a = new Vector2(1, 2)
            b = new Vector2(3, 4)
        })

        test('does not modify this', () => {
            a.add(b)
            expect([...a]).toEqual([1, 2])
        })

        test('does not modify that', () => {
            a.add(b)
            expect([...b]).toEqual([3, 4])
        })

        test('returns vector as sum of the two if given a vector', () => {
            expect([...a.add(b)]).toEqual([4, 6])
        })

        test('returns vector with components added by value when given a number', () => {
            expect([...a.add(3)]).toEqual([4, 5])
        })
    })

    describe('scale()', () => {
        test('does not modify this', () => {
            const v = new Vector2(2, 4)
            v.scale(0.5)
            expect([...v]).toEqual([2, 4])
        })

        test('returns vector multiplied with each component multiplied by the scalar', () => {
            expect([...new Vector2(2, 4).scale(0.5)]).toEqual([1, 2])
        })

        test('throws if not given a number', () => {
            expect(() => new Vector2().scale('hello')).toThrow(`must be of type 'number'`)
        })
    })

    describe('dot()', () => {
        test('does not modify this', () => {
            const thiz = new Vector2(2, 4)
            const that = new Vector2(1, -3)
            thiz.dot(that)
            expect([...thiz]).toEqual([2, 4])
        })

        test('does not modify this', () => {
            const thiz = new Vector2(2, 4)
            const that = new Vector2(1, -3)
            thiz.dot(that)
            expect([...that]).toEqual([1, -3])
        })

        test('throws if not given a vector', () => {
            expect(() => new Vector2().dot('hello')).toThrow(`must be an instance of 'Vector2'`)
        })

        test('returns scalar representing the dot product of the two vectors', () => {
            const thiz = new Vector2(2, 4)
            const that = new Vector2(3, -3)
            const scalar = thiz.dot(that)
            expect(scalar).toBe(-6)
        })
    })
})

describe('Vector3', () => {
    describe('constructor()', () => {
        test('initializes coordinates', () => {
            const v = new Vector3(1, 2, 3)
            expect(v[0]).toBe(1)
            expect(v.x).toBe(1)
            expect(v.s).toBe(1)
            expect(v.r).toBe(1)

            expect(v[1]).toBe(2)
            expect(v.y).toBe(2)
            expect(v.t).toBe(2)
            expect(v.g).toBe(2)

            expect(v[2]).toBe(3)
            expect(v.z).toBe(3)
            expect(v.p).toBe(3)
            expect(v.b).toBe(3)
        })

        test('initializes defaults to zero', () => {
            const v = new Vector3()
            expect(v[0]).toBe(0)
            expect(v[1]).toBe(0)
            expect(v[2]).toBe(0)
        })
    })

    describe('static SIZE', () => {
        test('should be one for each component = 4', () => {
            expect(Vector3.LENGTH).toBe(3)
        })
    })

    describe('static BYTES', () => {
        test('should be SIZE * 4 bytes per 32bit float = 12', () => {
            expect(Vector3.BYTES).toBe(12)
        })
    })

    describe('length', () => {
        test('is 3', () => {
            expect(new Vector3().length).toBe(3)
        })

        test('is read-only', () => {
            expect(() => new Vector2().length = 5).toThrow('has only a getter')
        })
    })

    describe('length', () => {
        test('is 3', () => {
            expect(new Vector3().length).toBe(3)
        })

        test('is read-only', () => {
            expect(() => new Vector2().length = 5).toThrow('has only a getter')
        })
    })

    describe('magnitude', () => {
        test('is the square root of the square of each component', () => {
            expect(new Vector3(2, 3, 4).magnitude).toBeCloseTo(5.385)
        })

        test('is zero when all components are zero', () => {
            expect(new Vector3(0, 0, 0).magnitude).toBe(0)
        })

        test('is read-only', () => {
            expect(() => new Vector3().magnitude = 5).toThrow('has only a getter')
        })
    })

    describe('toString()', () => {
        test('returns (x, y, z)', () => {
            expect(new Vector3(1.2345, 2.3456, 3.4567).toString()).toBe('(1.235, 2.346, 3.457)')
        })
    })

    describe('[Symbol.iterator]', () => {
        test('can be iterated over', () => {
            const v = new Vector3(10, 20, 30)
            const results = []
            for (const e of v) {
                results.push(e)
            }
            expect(results).toEqual([10, 20, 30])
        })
    })

    describe('add()', () => {
        let a, b

        beforeEach(() => {
            a = new Vector3(1, 2, 3)
            b = new Vector3(4, 5, 6)
        })

        test('does not modify this', () => {
            a.add(b)
            expect([...a]).toEqual([1, 2, 3])
        })

        test('does not modify that', () => {
            a.add(b)
            expect([...b]).toEqual([4, 5, 6])
        })

        test('returns vector as sum of two vectors when given a vector', () => {
            expect([...a.add(b)]).toEqual([5, 7, 9])
        })

        test('returns vector with components added by value when given a number', () => {
            expect([...a.add(3)]).toEqual([4, 5, 6])
        })
    })

    describe('scale()', () => {
        test('does not modify this', () => {
            const v = new Vector3(2, 4, 6)
            v.scale(0.5)
            expect([...v]).toEqual([2, 4, 6])
        })

        test('returns vector multiplied with each component multiplied by the scalar', () => {
            expect([...new Vector3(2, 4, 6).scale(0.5)]).toEqual([1, 2, 3])
        })

        test('throws if not given a number', () => {
            expect(() => new Vector3().scale('hello')).toThrow(`must be of type 'number'`)
        })
    })

    describe('dot()', () => {
        test('does not modify this', () => {
            const thiz = new Vector3(2, 4, 5)
            const that = new Vector3(1, -3, -2)
            thiz.dot(that)
            expect([...thiz]).toEqual([2, 4, 5])
        })

        test('does not modify this', () => {
            const thiz = new Vector3(2, 4, 5)
            const that = new Vector3(1, -3, -2)
            thiz.dot(that)
            expect([...that]).toEqual([1, -3, -2])
        })

        test('throws if not given a vector', () => {
            expect(() => new Vector3().dot('hello')).toThrow(`must be an instance of 'Vector3'`)
        })

        test('returns scalar representing the dot product of the two vectors', () => {
            const thiz = new Vector3(2, 4, 5)
            const that = new Vector3(3, -3, -2)
            const scalar = thiz.dot(that)
            expect(scalar).toBe(-16)
        })
    })

    describe('cross()', () => {
        const a = new Vector3(-1, 3, 2)
        const b = new Vector3(5, -2, 7)

        test('does not modify this', () => {
            a.cross(b)
            expect([...a]).toEqual([-1, 3, 2])
        })

        test('does not modify that', () => {
            a.cross(b)
            expect([...b]).toEqual([5, -2, 7])
        })

        test('returns a vector', () => {
            expect(a.cross(b)).toBeInstanceOf(Vector3)
        })

        test('returns the cross product of the two vectors', () => {
            expect([...a.cross(b)]).toEqual([25, 17, -13])
        })

        /**
         * https://en.wikipedia.org/wiki/Cross_product#Lagrange's_identity
         */
        test(`conforms to Lagrange's identity`, () => {
            const squareMagnitudeOfCrossProduct = Math.pow(a.cross(b).magnitude, 2)
            const productOfSquareMagnitudesMinusSquareOfDotProduct = Math.pow(a.magnitude, 2) * Math.pow(b.magnitude, 2) - Math.pow(a.dot(b), 2)
            expect(squareMagnitudeOfCrossProduct).toBeCloseTo(productOfSquareMagnitudesMinusSquareOfDotProduct)
        })

        test('is zero for orthogonal vectors', () => {
            const a = new Vector3(1, 2, 3)
            const b = new Vector3(-1, -2, -3)
            const c = a.cross(b)
            expect(c[0]).toBe(0)
            expect(c[1]).toBe(0)
            expect(c[2]).toBe(0)
        })
    })
})

describe('Vector4', () => {
    describe('constructor()', () => {
        test('initializes coordinates', () => {
            const v = new Vector4(1, 2, 3, 4)
            expect(v[0]).toBe(1)
            expect(v.x).toBe(1)
            expect(v.r).toBe(1)
            expect(v.s).toBe(1)

            expect(v[1]).toBe(2)
            expect(v.y).toBe(2)
            expect(v.g).toBe(2)
            expect(v.t).toBe(2)

            expect(v[2]).toBe(3)
            expect(v.z).toBe(3)
            expect(v.b).toBe(3)
            expect(v.p).toBe(3)

            expect(v[3]).toBe(4)
            expect(v.w).toBe(4)
            expect(v.a).toBe(4)
            expect(v.q).toBe(4)
        })

        test('initializes defaults to zero', () => {
            const v = new Vector4()
            expect(v[0]).toBe(0)
            expect(v[1]).toBe(0)
            expect(v[2]).toBe(0)
            expect(v[3]).toBe(0)
        })
    })

    describe('static SIZE', () => {
        test('should be one for each component = 4', () => {
            expect(Vector4.LENGTH).toBe(4)
        })
    })

    describe('static BYTES', () => {
        test('should be SIZE * 4 bytes per 32bit float = 16', () => {
            expect(Vector4.BYTES).toBe(16)
        })
    })

    describe('length', () => {
        test('is 4', () => {
            expect(new Vector4().length).toBe(4)
        })

        test('is read-only', () => {
            expect(() => new Vector2().length = 5).toThrow('has only a getter')
        })
    })

    describe('magnitude', () => {
        test('is the square root of the square of each component', () => {
            expect(new Vector4(2, 3, 4, 5).magnitude).toBeCloseTo(7.348)
        })

        test('is zero when all components are zero', () => {
            expect(new Vector4(0, 0, 0, 0).magnitude).toBe(0)
        })

        test('is read-only', () => {
            expect(() => new Vector4().magnitude = 5).toThrow('has only a getter')
        })
    })

    describe('toString()', () => {
        test('returns (x, y, z, w)', () => {
            expect(new Vector4(1.2345, 2.3456, 3.4567, 4.5678).toString()).toBe('(1.235, 2.346, 3.457, 4.568)')
        })
    })

    describe('[Symbol.iterator]', () => {
        test('can be iterated over', () => {
            const v = new Vector4(10, 20, 30, 40)
            const results = []
            for (const e of v) {
                results.push(e)
            }
            expect(results).toEqual([10, 20, 30, 40])
        })
    })

    describe('add()', () => {
        let a, b

        beforeEach(() => {
            a = new Vector4(1, 2, 3, 4)
            b = new Vector4(5, 6, 7, 8)
        })

        test('does not modify this', () => {
            a.add(b)
            expect([...a]).toEqual([1, 2, 3, 4])
        })

        test('does not modify that', () => {
            a.add(b)
            expect([...b]).toEqual([5, 6, 7, 8])
        })

        test('returns vector as sum of two vectors when given a vector', () => {
            expect([...a.add(b)]).toEqual([6, 8, 10, 12])
        })

        test('returns vector with components added by value when given a number', () => {
            expect([...a.add(3)]).toEqual([4, 5, 6, 7])
        })
    })

    describe('scale()', () => {
        test('does not modify this', () => {
            const v = new Vector4(2, 4, 6, 8)
            v.scale(0.5)
            expect([...v]).toEqual([2, 4, 6, 8])
        })

        test('returns vector multiplied with each component multiplied by the scalar', () => {
            expect([...new Vector4(2, 4, 6, 8).scale(0.5)]).toEqual([1, 2, 3, 4])
        })

        test('throws if not given a number', () => {
            expect(() => new Vector4().scale('hello')).toThrow(`must be of type 'number'`)
        })
    })

    describe('dot()', () => {
        test('does not modify this', () => {
            const thiz = new Vector4(2, 4, 8, 3)
            const that = new Vector4(1, -3, 2, 6)
            thiz.dot(that)
            expect([...thiz]).toEqual([2, 4, 8, 3])
        })

        test('does not modify this', () => {
            const thiz = new Vector4(2, 4, 8, 3)
            const that = new Vector4(1, -3, 2, 6)
            thiz.dot(that)
            expect([...that]).toEqual([1, -3, 2, 6])
        })

        test('throws if not given a vector', () => {
            expect(() => new Vector4().dot('hello')).toThrow(`must be an instance of 'Vector4'`)
        })

        test('returns scalar representing the dot product of the two vectors', () => {
            const thiz = new Vector4(2, 4, 8, 3)
            const that = new Vector4(3, -3, 2, 6)
            const scalar = thiz.dot(that)
            expect(scalar).toBe(28)
        })
    })
})

