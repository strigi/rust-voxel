import field from '../utils/validate.mjs'
import {Vector4} from './vector.mjs'

export class Matrix4 {
    #values

    static get SIZE() {
        return 4
    }

    static get LENGTH() {
        return Matrix4.SIZE * Matrix4.SIZE
    }

    static get BYTES() {
        return Matrix4.LENGTH * Float32Array.BYTES_PER_ELEMENT
    }

    /**
     * Accesses the buffer's elements in column-major mode, which stores the matrix's entries top to bottom then left to right
     * @param row {number}
     * @param column {number}
     * @return {number}
     */
    static #index_column_major(row, column) {
        return row + column * Matrix4.SIZE
    }

    /**
     * Accesses the buffer's elements in row-major mode, which stores the matrix's entries left to right then top to bottom
     * @param row {number}
     * @param column {number}
     * @return {number}
     */
    static #index_row_major(row, column) {
        return row * Matrix4.SIZE + column
    }

    static identity() {
        // Actually row or column major does not matter for diagonally symmetric matrices
        return Matrix4.fromRowMajor([
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1,
        ])
    }

    static scale(x, y, z) {
        // Actually row or column major does not matter for diagonally symmetric matrices
        return Matrix4.fromRowMajor([
            x, 0, 0, 0,
            0, y, 0, 0,
            0, 0, z, 0,
            0, 0, 0, 1
        ])
    }

    static translate(x, y, z) {
        return Matrix4.fromRowMajor([
            1, 0, 0, x,
            0, 1, 0, y,
            0, 0, 1, z,
            0, 0, 0, 1
        ])
    }

    /**
     * Return a rotation matrix (I think this is for Euler angles)
     * Effectively return the result of three rotation matrices multiplied together. One for each axis representing "yaw" "pitch" and "roll".
     * @param xdegs {number} Amount of degrees to rotate on the x-axis
     * @param ydegs {number} Amount of degrees to rotate on the y-axis
     * @param zdegs {number} Amount of degrees to rotate on the z-axis
     * return {Matrix4}
     */
    static rotate(xdegs, ydegs, zdegs) {
        // Rotation across the x-axis
        const alphaRads = xdegs * Math.PI / 180
        const ca = Math.cos(alphaRads)
        const sa = Math.sin(alphaRads)
        const pitch = Matrix4.fromRowMajor([
            1, 0, 0, 0,
            0, ca, -sa, 0,
            0, sa, ca, 0,
            0, 0, 0, 1
        ])

        // Rotation across the y-axis
        const betaRads = ydegs * Math.PI / 180
        const cb = Math.cos(betaRads)
        const sb = Math.sin(betaRads)
        const yaw = Matrix4.fromRowMajor([
            cb, 0, sb, 0,
            0, 1, 0, 0,
            -sb, 0, cb, 0,
            0, 0, 0, 1,
        ])

        // Rotation across the x-axis
        const gammaRads = zdegs * Math.PI / 180
        const cg = Math.cos(gammaRads)
        const sg = Math.sin(gammaRads)
        const roll = Matrix4.fromRowMajor([
            cg, -sg, 0, 0,
            sg, cg, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
        ])

        return pitch.mul(yaw).mul(roll)
    }

    static fromRowMajor(values) {
        return new Matrix4(values).transpose()
    }

    static fromColumnMajor(values) {
        return new Matrix4(values)
    }

    /**
     * @param [values] {[number]} Array of exactly 16 floats that defined the entries of this matrix.
     */
    constructor(values) {
        this.#values = new Float32Array(Matrix4.LENGTH)
        if(values) {
            field('data').assert_array({ length: Matrix4.LENGTH })(values)
            values.forEach((v, i) => field(`values[${i}]`).assert_number()(v))
            this.#values.set(values)
        }
    }

    get values() {
        return this.#values
    }

    #index(row, column) {
        return Matrix4.#index_column_major(row, column)
    }

    #set(row, column, value) {
        this.#values[this.#index(row, column)] = value
    }

    get(row, column) {
        return this.#values[this.#index(row, column)]
    }

    mul(that) {
        if(typeof that === 'number') {
            return this.#multiply_scalar(that)
        } else if(that instanceof Matrix4) {
            return this.#multiply_matrix(that)
        } if(that instanceof Vector4) {
            return this.#multiply_vector(that)
        }
        throw new Error(`Unable to multiply Matrix4 with '${that}'`)
    }

    #multiply_vector(vector) {
        return new Vector4(
            this.get(0, 0) * vector.x + this.get(0, 1) * vector.y + this.get(0, 2) * vector.z + this.get(0, 3) * vector.w,
            this.get(1, 0) * vector.x + this.get(1, 1) * vector.y + this.get(1, 2) * vector.z + this.get(1, 3) * vector.w,
            this.get(2, 0) * vector.x + this.get(2, 1) * vector.y + this.get(2, 2) * vector.z + this.get(2, 3) * vector.w,
            this.get(3, 0) * vector.x + this.get(3, 1) * vector.y + this.get(3, 2) * vector.z + this.get(3, 3) * vector.w,
        )
    }

    #multiply_scalar(number) {
        const m = new Matrix4()
        for(const [value, row, column] of this.entries()) {
            m.#set(row, column, value * number)
        }
        return m
    }

    #multiply_matrix(that) {
        const product = new Matrix4()

        const rows = [...this.rows()]
        const columns = [...that.columns()]
        for(let r = 0; r < Matrix4.SIZE; r++) {
            for(let c = 0; c < Matrix4.SIZE; c++) {
                product.#set(r, c, rows[r].dot(columns[c]))
            }
        }
        return product
    }

    toString() {
        let string = ''
        for(let r = 0; r < Matrix4.SIZE; r++) {
            for(let c = 0; c < Matrix4.SIZE; c++) {
                let v = this.get(r, c)
                let s = (v === 0 && ' ') || (v < 0 && '-') || (v > 0 && '+')

                string += `${s}${Math.abs(v).toFixed(3)}\t`
            }
            string += '\n'
        }

        return string
    }

    toArray(opts) {
        const options = {
            mode: 'column-major',
            ...opts
        }
        field('opts.mode').assert_string({ enum: 'row-major' | 'column-major'})
        if(options.mode === 'column-major') {
            return [...this.#values]
        } else {
            return this.transpose().toArray()
        }
    }

    transpose() {
        const transposed = new Matrix4()
        for(let r = 0; r < Matrix4.SIZE; r += 1) {
            for(let c = 0; c < Matrix4.SIZE; c += 1) {
                transposed.#set(c, r, this.get(r, c))
            }
        }
        return transposed;
    }

    *entries() {
        for(let c = 0; c < Matrix4.SIZE; c += 1) {
            for(let r = 0; r < Matrix4.SIZE; r += 1) {
                yield [this.get(r, c), r, c]
            }
        }
    }

    /**
     * Returns an iterator of four Vector4 representing the 4 rows.
     * Since the internal storage of the Matrix is in column-major mode (favouring columns as vectors)
     * this iterator needs to internally transpose the matrix first to be able to take slices of the underlying buffer.
     */
    *rows() {
        const t = this.transpose()
        for(let i = 0; i < Matrix4.LENGTH; i += 4) {
            yield new Vector4(...t.#values.slice(i, i + 4))
        }
    }

    /**
     * Returns an iterator of four Vector4 representing the 4 columns.
     * Since the internal storage of the Matrix is in column-major mode (favouring columns as vectors)
     * this iterator can take slices of the underlying buffer and does not need to rearrange things.
     */
    *columns() {
        for(let i = 0; i < Matrix4.LENGTH; i += 4) {
            yield new Vector4(...this.#values.slice(i, i + 4))
        }
    }
}
