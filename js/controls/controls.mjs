import logger from '../utils/logger.mjs'
import { Key } from './key.mjs'

const log = logger.get(import.meta)

export class Controls {
    #keys = []
    #lastGamepadActivityTimestamp = 0

    constructor(element) {
        if(!element) {
            element = document.body
        }

        element.addEventListener('keydown', event => {
            this.#on(event.code)
        })

        element.addEventListener('keyup', event => {
            this.#off(event.code)
        })

        window.addEventListener("gamepadconnected", e => {
            log.info(`Gamepad connected '%s'`, e.gamepad.id)
        })
        window.addEventListener("gamepaddisconnected", e => {
            log.info(`Gamepad disconnected '%s'`, e.gamepad.id)
        })
    }

    #on(code) {
        this.poll(code)
        this.poll(code).on()
    }

    #off(code) {
        this.poll(code).off()
    }

    #set(code, value) {
        this.poll(code).set(value)
    }

    poll(code) {
        let key = this.#keys.find(key => key.code === code)
        if(!key) {
            key = new Key(code)
            this.#keys.push(key)
        }
        return key
    }

    bind(code, listenerFn, opts) {
        this.poll(code).registerListener(listenerFn, opts)
    }

    control() {
        this.#handleKeyboard()

        this.#handleGamepad()
    }

    #handleKeyboard() {
        for (const key of this.boundKeys()) {
            if (key.active) {
                key.notify({once: false})
            }
        }
    }

    #handleGamepad() {
        const gamepad = this.#fetchGamepadState()
        if (!gamepad || gamepad.timestamp === this.#lastGamepadActivityTimestamp) {
            return
        }
        this.#lastGamepadActivityTimestamp = gamepad.timestamp

        log.debug(`Gamepad activity detected for '%s'`, gamepad.id)
        for(let i = 0; i < gamepad.buttons.length; i += 1) {
            const button = gamepad.buttons[i]
            const code = `GamepadButton${(i + 1)}`
            if(button.pressed === true) {
                this.#on(code)
            } else {
                this.#off(code)
            }
        }

        for(let i = 0; i < gamepad.axes.length; i += 1) {
            const value = gamepad.axes[i]
            const code = `GamepadAxis${(i + 1)}`
            this.#set(code, value)
        }
    }

    #fetchGamepadState() {
        const gamepads = navigator.getGamepads()
        if(!Array.isArray(gamepads)) {
            return null
        }
        return gamepads.filter(v => !!v)[0]
    }

    boundKeys() {
        return this.keys(k => k.bound)
    }

    *keys(filterFn) {
        for(const key of this.#keys.filter(filterFn)) {
            yield key
        }
    }

    activeKeys() {
        return this.keys(k => k.active)
    }

    toString() {
        return this.#keys.map(k => k.toString()).join(',\n')
    }
}
