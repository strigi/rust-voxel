import logger from '../utils/logger.mjs'

const log = logger.getFromMeta(import.meta)

export class Key {
    #code

    #value = 0
    #listeners = []

    constructor(code) {
        this.#code = code
    }

    registerListener(fn, opts) {
        const options = {
            once: false,
            ...opts
        }
        log.verbose(`Registering listener for key '${this.#code}'`)
        this.#listeners.push({
            ...options,
            fn,
        })
    }

    on() {
        this.set(1)
    }

    off() {
        this.set(0)
    }

    set(value) {
        if(!this.bound) {
            log.warn(`Unbound key activated '${this.code}'`)
        }
        const shouldNotifyOnce = this.active
        this.#value = value
        if(shouldNotifyOnce) {
            this.notify({once: true})
        }
    }

    get active() {
        const DEAD_ZONE = 0.25
        return Math.abs(this.#value) > DEAD_ZONE
    }

    get analog() {
        return this.#value !== 1 && this.#value !== 0
    }

    get value() {
        return this.#value
    }

    get code() {
        return this.#code
    }

    get bound() {
        return this.#listeners.length > 0
    }

    notify(filter) {
        filter = {
            once: false,
            ...filter
        }
        const filteredListeners = this.#listeners.filter(l => l.once === filter.once)
        for (const listener of filteredListeners) {
            listener.fn(this)
        }
    }

    toString() {
        return `${this.#code}=${this.active}`
    }
}