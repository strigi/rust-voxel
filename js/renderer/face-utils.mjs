import {Matrix4, Vector3, Vector4} from '../math/index.mjs'
import {Vertex} from './vertex.mjs'
import field from '../utils/validate.mjs'

export const RED = new Vector3(1, 0, 0)
export const GREEN = new Vector3(0, 1, 0)
export const BLUE = new Vector3(0, 0, 1)
export const WHITE = new Vector3(1, 1, 1)
export const CYAN = RED.add(-1).scale(-1)
export const MAGENTA = GREEN.add(-1).scale(-1)
export const YELLOW = BLUE.add(-1).scale(-1)
export const BLACK = WHITE.add(-1).scale(-1)
export const DARK_RED = RED.scale(0.5)
export const DARK_GREEN = GREEN.scale(0.5)
export const DARK_BLUE = BLUE.scale(0.5)
export const DARK_CYAN = CYAN.scale(0.5)
export const DARK_MAGENTA = MAGENTA.scale(0.5)
export const DARK_YELLOW = YELLOW.scale(0.5)
export const GREY = WHITE.scale(0.5)

export function face(matrix, color, size= 1) {
    matrix ??= Matrix4.identity()
    color ??= GREY
    const half_size = size / 2
    return [
        new Vector3(-half_size, -half_size, 0),
        new Vector3(half_size, -half_size, 0),
        new Vector3(-half_size, half_size, 0),

        new Vector3(half_size, half_size, 0),
        new Vector3(-half_size, half_size, 0),
        new Vector3(half_size, -half_size, 0),
    ].map(p => new Vertex(shrink(matrix.mul(grow(p))), color))

    function grow(vector) {
        return new Vector4(vector.x, vector.y, vector.z, 1)
    }

    function shrink(vector) {
        return new Vector3(vector.x, vector.y, vector.z)
    }
}

export function quad(a, b, c, d, color = GREY) {
    field('a').assert_instance(Vector3)(a)
    field('b').assert_instance(Vector3)(b)
    field('c').assert_instance(Vector3)(c)
    field('d').assert_instance(Vector3)(d)

    return [
        new Vertex(a, color),
        new Vertex(b, color),
        new Vertex(c, color),
        new Vertex(a, color),
        new Vertex(c, color),
        new Vertex(d, color),

    ]
}

export function nearQuad(size= 1) {
    return face(Matrix4.identity(), DARK_BLUE, size)
}

export function farQuad(size= 1) {
    const tm = Matrix4.rotate(0, 180, 0)
    return face(tm, BLUE, size)
}

export function leftQuad(size= 1) {
    const tm = Matrix4.rotate(0, 90, 0)
    return face(tm, DARK_RED, size)
}

export function rightQuad(size= 1) {
    const tm = Matrix4.rotate(0, -90, 0)
    return face(tm, RED, size)
}

export function topQuad(size= 1) {
    const tm = Matrix4.rotate(90, 0, 0)
    return face(tm, GREEN, size)
}

export function bottomQuad(size= 1) {
    const tm = Matrix4.rotate(-90, 0, 0)
    return face(tm, DARK_GREEN, size)
}
