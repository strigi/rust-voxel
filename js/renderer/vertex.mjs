import field from '../utils/validate.mjs'
import {Vector3} from '../math/index.mjs'

export class Vertex {
    constructor(position, color) {
        this.position = position
        this.color = color
    }

    toArray() {
        return [...this.position, ...this.color]
    }

    add(vector) {
        return new Vertex(this.position.add(vector), this.color)
    }
}
