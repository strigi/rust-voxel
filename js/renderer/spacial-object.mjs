import {Matrix4, Vector3} from '../math/index.mjs'
import {createBuffer} from './web-gpu-utils.mjs'
import logger from '../utils/logger.mjs'
import field from '../utils/validate.mjs'

const log = logger.getFromMeta(import.meta)

export class SpacialObject {
    static nextId = 1

    name = null
    id = SpacialObject.nextId++

    position = new Vector3()
    orientation = new Vector3()

    #scale = new Vector3(1, 1, 1)

    vertexBuffer = new Float32Array(0)

    vertexCount = 0

    constructor(name) {
        this.name = name
    }

    scale(vectorOrNumber) {
        if(typeof vectorOrNumber === 'number') {
            const number = vectorOrNumber
            this.#scale = new Vector3(number, number, number)
        } else if (vectorOrNumber instanceof Vector3) {
            const vector = vectorOrNumber
            this.#scale = vector
        } else {
            throw new Error('Unknown scale parameter')
        }
        return this
    }

    initialize(vertices) {
        field('vertices').assert_array()(vertices)
        log.verbose(`Initializing Mesh of ${vertices.length} vertices`)
        this.vertexBuffer = createBuffer(GPUBufferUsage.VERTEX, new Float32Array(vertices.flatMap(v => v.toArray())))
        this.vertexCount = vertices.length
    }

    translate(vector) {
        const oldPosition = this.position
        const newPosition = oldPosition.add(vector)
        // log.verbose(`Moving object from ${oldPosition} by ${vector} to ${newPosition}`)
        this.position = newPosition
        return this
    }

    rotate(vector) {
        const oldOrientation = this.orientation
        const newOrientation = oldOrientation.add(vector)
        // log.verbose(`Moving object from ${oldOrientation} by ${vector} to ${newOrientation}`)
        this.orientation = newOrientation
        return this
    }

    modelMatrix() {
        const r = Matrix4.rotate(this.orientation[0], this.orientation[1], this.orientation[2])
        const t = Matrix4.translate(this.position[0], this.position[1], this.position[2])
        const s = Matrix4.scale(this.#scale[0], this.#scale[1], this.#scale[2])
        return t.mul(r).mul(s)
    }
}