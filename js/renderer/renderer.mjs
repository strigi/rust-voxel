import logger from '../utils/logger.mjs'
import {createPipeline, createWritableBufferBindGroupsForUniformMatrix, device} from './web-gpu-utils.mjs'
import {Vector4} from '../math/index.mjs'

const log = logger.getFromMeta(import.meta)

export default async function renderer(elementOrSelectorOrNull, opts) {
    const options = {
        ...opts,
    }

    const self = {}

    let preparation = null;

    const { context, canvas } = initializeRenderingContext(elementOrSelectorOrNull, {
        contextId: 'webgpu',
        width: options.width,
        height: options.height,
    })

    self.context = context
    self.canvas = canvas

    const canvasFormat = navigator.gpu.getPreferredCanvasFormat();
    log.verbose(`Configuring WebGPU context using preferred format '%s'`, canvasFormat)
    context.configure({
        device,
        format: canvasFormat,
        alphaMode: 'opaque'
    });

    const depthTexture = device.createTexture({
        size: {
            width: canvas.width,
            height: canvas.height,
        },
        format: 'depth24plus',
        usage: GPUTextureUsage.RENDER_ATTACHMENT,
    });

    self.prepare = async (scene) => {
        if(!scene) {
            throw new Error('Scene is required')
        }

        // Allocate a buffer to contain n+1 matrices. One for each mesh for a modelView matrix and one extra for the perspective matrix
        const perspective = createWritableBufferBindGroupsForUniformMatrix(1)
        const modelView = createWritableBufferBindGroupsForUniformMatrix(scene.objects.length)

        preparation = {
            scene,
            perspective,
            modelView,
            pipeline: await createPipeline(scene.shaderSource, [perspective.layout, modelView.layout])
        }
    }

    Object.defineProperties(self, {
        isPrepared: {
            get() {
                return preparation !== null
            }
        },

        width: {
            get() {
                return self.canvas.width
            }
        },

        height: {
            get() {
                return self.canvas.height
            }
        }
    })

    self.render = (camera) => {
        if(!self.isPrepared) {
            throw new Error('Renderer is not prepared for this scene')
        }

        camera ??= preparation.scene.camera

        const encoder = device.createCommandEncoder()
        const pass = encoder.beginRenderPass({
            colorAttachments: [{
                view: context.getCurrentTexture().createView(),
                //clearValue: { r: 0.0, g: 0.1, b: 0.2, a: 1.0 },
                clearValue: new Vector4(0.5, 0.5, 0.5, 1.0),
                loadOp: 'clear',
                storeOp: 'store',
            }],
            depthStencilAttachment: {
                view: depthTexture.createView(),
                depthClearValue: 1.0,
                depthLoadOp: 'clear',
                depthStoreOp: 'store',
            },
        })

        pass.setPipeline(preparation.pipeline)

        const projectionMatrix = camera.projectionMatrix()
        preparation.perspective.write(0, projectionMatrix)
        pass.setBindGroup(0, preparation.perspective.groups[0]);

        for(let i = 0; i < preparation.scene.objects.length; i += 1) {
            const mesh = preparation.scene.objects[i]
            //const mvpMatrix = preparation.scene.camera.projectionMatrix().mul(preparation.scene.camera.viewMatrix().mul(mesh.modelMatrix()))
            const modelViewMatrix = camera.viewMatrix().mul(mesh.modelMatrix())
            preparation.modelView.write(i, modelViewMatrix)
        }

        for(let i = 0; i < preparation.scene.objects.length; i += 1) {
            const { vertexBuffer, vertexCount } = preparation.scene.objects[i]
            pass.setBindGroup(1, preparation.modelView.groups[i]);
            pass.setVertexBuffer(0, vertexBuffer);
            pass.draw(vertexCount, 1, 0, 0)
        }
        pass.end()

        device.queue.submit([encoder.finish()])
    }

    return Object.seal(self);
}

function initializeRenderingContext(el, opts) {
    log.verbose('Initializing rendering context')

    const options = {
        width: null,
        height: null,
        contextId: '2d',
        ...opts,
    }

    let canvas
    if(el === null || el === undefined) {
        canvas = document.createElement('canvas')
        log.verbose('Creating new detached canvas element')
    } else if(typeof el === 'string') {
        const selector  = el;
        log.verbose(`Using existing attached canvas element by selector '%s'`, selector)
        canvas = document.querySelector(selector)
        if(canvas === null) {
            throw new Error(`Unable to find element matching selector '${selector}'`)
        }
        if(!(canvas instanceof HTMLCanvasElement)) {
            throw new Error(`Element must be a canvas but was '${canvas}'`)
        }
    }

    if(options.width || options.height) {
        log.verbose('Changing canvas size from %dx%d to %dx%d pixels', canvas.width, canvas.height, options.width, options.height)
        if (options.width) {
            canvas.width = options.width
        }
        if (options.height) {
            canvas.height = options.height
        }
    } else {
        log.verbose(`Keeping existing canvas element size of %dx%d pixels`, canvas.width, canvas.height)
    }

    log.verbose(`Creating '%s' context`, options.contextId)
    const context = canvas.getContext(options.contextId)
    if(context === null) {
        throw new Error(`Unable to create a '${options.contextId}' rendering context`)
    }
    log.verbose(`Context supporting '${options.contextId}' successfully created`)
    return { canvas: canvas, context }
}
