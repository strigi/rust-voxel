import logger from '../utils/logger.mjs'
import {Matrix4} from '../math/index.mjs'
import field from '../utils/validate.mjs'

const log = logger.getFromMeta(import.meta)

export const device = await createDevice()

async function createDevice() {
    log.info('Initializing WebGPU')

    log.verbose('Creating WebGPU adapter')
    if(!navigator.gpu?.requestAdapter) {
        throw new Error('Browser does not seem to support WebGPU')
    }
    const adapter = await navigator.gpu.requestAdapter()

    log.verbose('Creating WebGPU device')
    return await adapter.requestDevice()
}

export function createBuffer(usage, dataOrSize) {
    let size
    let mapped
    let data
    if(typeof dataOrSize === 'number') {
        size = dataOrSize
        mapped = false
        data = null
    } else if(dataOrSize instanceof Float32Array) {
        size = dataOrSize.byteLength
        mapped = true
        data = dataOrSize
    } else {
        throw new Error(`Argument dataOrSize must be a number of a Float32 array but was '${dataOrSize}'`)
    }

    log.verbose(`Creating GPU buffer for usage '%s' of size %d bytes`, demangleBitflags(GPUBufferUsage, usage), size)
    const buffer = device.createBuffer({
        size,
        usage,
        mappedAtCreation: mapped,
    });

    if(mapped) {
        new Float32Array(buffer.getMappedRange()).set(data);
        buffer.unmap()
    }

    return buffer
}

export function createWritableBufferBindGroupsForUniformMatrix(count) {
    field().assert_number({ min: 1 })(count)

    // Apparently, uniform bind groups must be aligned to 256 bytes
    const ALIGNMENT_BOUNDARY = 256
    const size = Math.max(ALIGNMENT_BOUNDARY, Matrix4.BYTES) * (count - 1) + Matrix4.BYTES
    log.verbose(`Creating writable uniform Matrix bind group for ${count} matrices of ${Matrix4.SIZE}x${Matrix4.SIZE}=${Matrix4.LENGTH} values each being a float of ${Float32Array.BYTES_PER_ELEMENT} bytes totalling ${Matrix4.BYTES * count} bytes but since each matrix has to start on a multiple of ${ALIGNMENT_BOUNDARY} bytes, the real size is ${size} bytes`)
    const buffer = createBuffer(GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST, size)

    const layout = device.createBindGroupLayout({
        entries: [{
            binding: 0,
            visibility: GPUShaderStage.VERTEX,
            buffer: {
                type: 'uniform'
            }
        }]
    })

    const groups = []
    for(let i = 0; i < count; i += 1) {
        groups.push(device.createBindGroup({
            layout: layout,
            entries: [{
                binding: 0,
                resource: {
                    buffer: buffer,
                    offset: i * ALIGNMENT_BOUNDARY,
                    size: Matrix4.BYTES
                }
            }]
        }))
    }

    return {
        buffer,
        layout,
        groups,

        write(matrixIndex, matrix) {
            field('matrixIndex').assert_number({min: 0, max: count - 1})(matrixIndex)
            device.queue.writeBuffer(buffer, matrixIndex * 256, matrix.values)
        }
    }
}

export async function createPipeline(shaderCode, bindGroupLayouts) {
    log.info(`Creating rendering pipeline`)

    // TODO we can parse the float32x3 string and combine the format with size
    const attributes = [{
        //name: 'position',
        size: Float32Array.BYTES_PER_ELEMENT * 3,
        format: 'float32x3'
    }, {
        //name: 'color',
        size: Float32Array.BYTES_PER_ELEMENT * 3,
        format: 'float32x3'
    }]

    let stride = 0
    for (const attribute of attributes) {
        stride += attribute.size
    }


    const bufferDescriptor = {
        arrayStride: stride,
        attributes: [],
    }

    let offset = 0
    let shaderLocation = 0
    for(const attribute of attributes) {
        bufferDescriptor.attributes.push({
            shaderLocation: shaderLocation++,
            offset: offset,
            format: attribute.format
        })
        offset += attribute.size
    }

    const layout = device.createPipelineLayout({
        label: 'Strigi Pipeline Layout',
        bindGroupLayouts: bindGroupLayouts
    })

    const vertexShader = await compileShader(shaderCode)
    const fragmentShader = await compileShader(shaderCode)

    return device.createRenderPipeline({
        layout: layout,
        vertex: {
            module: vertexShader,
            entryPoint: 'vmain',
            buffers: [bufferDescriptor],
        },
        fragment: {
            module: fragmentShader,
            entryPoint: 'fmain',
            targets: [{
                format: navigator.gpu.getPreferredCanvasFormat()
            }]
        },
        primitive: {
            topology: 'triangle-list',
            frontFace: 'ccw', // 'cw' 'ccw (default: 'ccw'?)
            cullMode: 'back' // 'front' 'back' 'none' (default: 'none'?)
        },
        depthStencil: {
            depthWriteEnabled: true,
            depthCompare: 'less',
            format: 'depth24plus',
        },
    })
}

function demangleBitflags(options, value) {
    const names = []
    for(const [name, code] of Object.entries(options)) {
        if(!!(value & code) === true) {
            names.push(name)
        }
    }
    return names
}

async function compileShader(code, opts) {
    const options = {
        ...opts,
        label: 'Strigi Shader'
    }
    const shader = device.createShaderModule({
        label: options.label,
        code,
    })

    const info = await shader.getCompilationInfo()
    if(info.messages.length === 0) {
        return shader
    }

    const message = `Shader compilation resulted in ${info.messages.length} errors`
    const error = new Error(message)
    error.messages = info.messages
    throw error
}