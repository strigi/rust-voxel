import {Vector3} from '../math/index.mjs'
import {Matrix4} from '../math/index.mjs'
import logger from '../utils/logger.mjs'
import {SpacialObject} from './spacial-object.mjs'
import {Vertex} from './vertex.mjs'
import {BLACK, BLUE, CYAN, DARK_CYAN, GREEN, GREY, MAGENTA, RED, WHITE, YELLOW} from './face-utils.mjs'

const log = logger.get('camera')
export class Camera extends SpacialObject {
    static count = 0

    constructor(width = 1, height = 9 / 16, opts) {
        super(`camera-${++Camera.count}`)

        Object.assign(this, {
            perspective: true,
            ...opts
        })

        this.width = width
        this.height = height

        // Determines the FOV, using the film diameter produces the most "natural" FOV https://complexelepheonix.com/nl/welke-lensbrandpuntsafstand-lijkt-het-meest-op-het-perspectief-van-het-menselijk-oog/
        this.near = this.diagonal

        // Does not have impact on the FOV
        this.far = this.near + 10

        // Position camera so that (0, 0, 0) is in the middle of the viewing frustum
        const z = (this.near + this.far) / -2
        this.position = new Vector3(0.0, 0.0, z)
        this.orientation = new Vector3(0, 0, 0)

        this.initialize(this.createVertices())
    }

    createVertices() {
        const origin = new Vertex(new Vector3(0, 0, 0), YELLOW);

        const half_width = this.width / 2
        const half_height = this.height / 2

        return [
            new Vertex(new Vector3(-half_width, -half_height, this.near), WHITE),
            new Vertex(new Vector3(+half_width, -half_height, this.near), WHITE),
            new Vertex(new Vector3(+half_width, +half_height, this.near), WHITE),

            new Vertex(new Vector3(-half_width, -half_height, this.near), WHITE),
            new Vertex(new Vector3(+half_width, +half_height, this.near), WHITE),
            new Vertex(new Vector3(-half_width, +half_height, this.near), WHITE),

            origin,
            new Vertex(new Vector3(-half_width, +half_height, this.near), RED),
            new Vertex(new Vector3(+half_width, +half_height, this.near), RED),

            origin,
            new Vertex(new Vector3(+half_width, -half_height, this.near), GREEN),
            new Vertex(new Vector3(-half_width, -half_height, this.near), GREEN),

            origin,
            new Vertex(new Vector3(-half_width, -half_height, this.near), BLUE),
            new Vertex(new Vector3(-half_width, +half_height, this.near), BLUE),

            origin,
            new Vertex(new Vector3(+half_width, +half_height, this.near), MAGENTA),
            new Vertex(new Vector3(+half_width, -half_height, this.near), MAGENTA),

        ]
    }

    get diagonal() {
        return Math.sqrt(this.width * this.width + this.height * this.height)
    }

    get fovx() {
        if(this.perspective === false) {
            return 0
        }

        return 360 * Math.atan(this.width / (2 * this.near)) / Math.PI
    }

    get fovy() {
        if(this.perspective === false) {
            return 0
        }

        return 360 * Math.atan(this.height / (2 * this.near)) / Math.PI
    }

    get depth() {
        return this.far - this.near
    }

    get aspect() {
        return this.width / this.height
    }

    addFov(amount) {
        let newNear = Math.max(this.near + amount, 0)
        let depth = this.depth

        this.near = newNear
        this.far = this.near + depth
    }

    projectionMatrix() {
        const orthographic = Matrix4.scale(
            2 / this.width,
            2 / this.height,
            1 / this.depth, // TODO: what to do with this value???
        )

        let projection = orthographic

        if(this.perspective === true) {
            let n = this.near
            let f = this.far

            // https://people.eecs.berkeley.edu/~sequin/CS184/IMGS/Persp_Matrix.jpg
            // https://people.eecs.berkeley.edu/~sequin/CS184/IMGS/Persp_Transform.jpg
            // This is setup to have the Z axis pointing into the screen, this is (I think) why the third row has values inverted
            const perspective = Matrix4.fromRowMajor([
                n, 0, 0, 0,
                0, n, 0, 0,
                0, 0, f-n, -n*f,
                0, 0, 1, 0
            ])
            projection = projection.mul(perspective)
        }

        return projection
    }

    /**
     * The job of the view matrix is to transform all objects in the world coordinates such that the camera would end up precisely at the origin
     * and rotated to be aligned with (e.g. to look in the direction along) the Z axis.
     * This makes it possible to the projection matrix to then scale the view coordinates into WebGPU's view space of (-1..1, -1..1, 0..1)
     * TODO: this is explanation accurate?
     */
    viewMatrix() {
        const r = Matrix4.rotate(this.orientation[0], this.orientation[1], this.orientation[2])
        const t = Matrix4.translate(-this.position[0], -this.position[1], -this.position[2])

        // TODO figure out if it's not supposed to be r*t (it's the camera that should rotate, not the cube
        return r.mul(t)
    }

    toString() {
        return 'Camera: \nView matrix:\n' + this.viewMatrix().toString() + '\nProjection matrix:\n' + this.projectionMatrix().toString()
    }

    moveForward(amount) {
        this.position

        const half_width = this.width / 2
        const half_height = this.height / 2

        new Vector3(0, 0, this.near)
        new Vector3(0, 0, this.near)

        new Vector3()
    }

    moveBackward(amount) {

    }
}
