import {SpacialObject} from './spacial-object.mjs'
import {Vector3} from '../math/index.mjs'
import {Vertex} from './vertex.mjs'
import {BLUE, DARK_RED, GREEN, RED, YELLOW, leftQuad, rightQuad, bottomQuad, topQuad, nearQuad, farQuad, quad, DARK_GREEN, DARK_BLUE} from './face-utils.mjs'
import logger from '../utils/logger.mjs'
import field from '../utils/validate.mjs'

const DIMENSIONS = 3

const log = logger.getFromMeta(import.meta)

export class VoxelMesh extends SpacialObject {
    static count = 0

    #properties = {}

    constructor({width = 1, resolution = 10} = {}) {
        super(`voxel-grid-${++VoxelMesh.count}`)

        this.width = width
        this.resolution = resolution

        this.#properties.material = new MaterialProperty(resolution)
        this.#properties.heat = new HeatProperty(resolution)

        const vertices = []
        const half_grid_width = this.width / 2
        const gridLeftBottomNear = new Vector3(this.width / -2, this.width / -2, this.width / -2)
        const grid_center = gridLeftBottomNear.add(half_grid_width)
        const voxel_width = this.width / this.resolution
        const half_voxel_width = voxel_width / 2
        log.debug('bottom left near of voxel grid is %s', gridLeftBottomNear)
        log.debug('center of voxel grid is %s', grid_center)

        log.info('Generating voxel pattern')
        const start = performance.now()
        for(let z = 0; z < this.resolution; z += 1) {
            for (let y = 0; y < this.resolution; y += 1) {
                for (let x = 0; x < this.resolution; x += 1) {
                    const left = gridLeftBottomNear.x + x * voxel_width
                    const right = left + voxel_width
                    const bottom = gridLeftBottomNear.y + y * voxel_width
                    const top = bottom + voxel_width
                    const near = gridLeftBottomNear.z + z * voxel_width
                    const far = near + voxel_width
                    const voxel_center = new Vector3((left + right) / 2, (bottom + top) / 2, (near + far) / 2)
                    const distance_from_center = voxel_center.subtract(grid_center).magnitude

                    // const solid = distance_from_center >= half_grid_width * 1.2 && distance_from_center < half_grid_width * 1.3
                    const solid = distance_from_center <= half_grid_width && distance_from_center >= half_grid_width - 0.1 && y <= this.resolution / 2

                    // const material = Math.round(1 + Math.random() * MaterialProperty.STONE - 1)
                    // const material = solid ? 1 : 0
                    // this.set(x, y, z, 'material', material) // If not
                    this.set(x, y, z, 'material', Number(solid)) // If not

                    // const heat = solid ? Math.random() * (273.15 + 100) : 0 // 0..100°C
                    this.set(x, y, z, 'heat', 1)
                }
            }
        }
        const end = performance.now()
        log.verbose('Finished generating voxel pattern %fms', (end - start).toFixed(2))

        log.info('Generating visible faces for voxel grid')
        const startTime = performance.now()

        const max = this.resolution - 1
        const min = 0

        const leftFacing = leftQuad(voxel_width)
        const rightFacing = rightQuad(voxel_width)
        const bottomFacing = bottomQuad(voxel_width)
        const topFacing = topQuad(voxel_width)
        const nearFacing = nearQuad(voxel_width)
        const farFacing = farQuad(voxel_width)

        for(let z = min; z <= max; z += 1) {
            const near = gridLeftBottomNear.z + z * voxel_width
            const far = near + voxel_width

            for (let y = min; y <= max; y += 1) {
                const bottom = gridLeftBottomNear.y + y * voxel_width
                const top = bottom + voxel_width

                for (let x = min; x <= max; x += 1) {
                    const left = gridLeftBottomNear.x + x * voxel_width
                    const right = left + voxel_width

                    const voxel_center = new Vector3((left + right) / 2, (bottom + top) / 2, (near + far) / 2)

                    // If x y or z are at their max, there is no voxel to their far side, but we pretend it's an air block
                    // so that the algorithm renders a wall cap on the far side, which allows the grid to be viewed from outside the grid.
                    const current = Number(this.get(x, y, z, 'material'))
                    const right_neighbor = x === max ? 0 : Number(this.get(x + 1, y, z, 'material'))
                    const top_neighbor = y === max ? 0 : Number(this.get(x, y + 1, z, 'material'))
                    const far_neighbor = z === max ? 0 : Number(this.get(x, y, z + 1, 'material'))

                    if(x === min && current > 0) {
                        const leftCap = leftFacing.map(v => v.add(voxel_center.add(new Vector3(-half_voxel_width, 0, 0))))
                        vertices.push(...leftCap)
                    }

                    if(y === min && current > 0) {
                        const bottomCap = bottomFacing.map(v => v.add(voxel_center.add(new Vector3(0, -half_voxel_width, 0))))
                        vertices.push(...bottomCap)
                    }

                    if(z === min && current > 0) {
                        const nearCap = nearFacing.map(v => v.add(voxel_center.add(new Vector3(0, 0, -half_voxel_width))))
                        vertices.push(...nearCap)
                    }

                    if(current > 0 && right_neighbor === 0) {
                        const rightWall = rightFacing.map(v => v.add(voxel_center.add(new Vector3(half_voxel_width, 0, 0))))
                        vertices.push(...rightWall)
                    } else if(current === 0 && right_neighbor === 1) {
                        const leftWall = leftFacing.map(v => v.add(voxel_center.add(new Vector3(half_voxel_width, 0, 0))))
                        vertices.push(...leftWall)
                    }

                    if(current > 0 && top_neighbor === 0) {
                        const topWall = topFacing.map(v => v.add(voxel_center.add(new Vector3(0, half_voxel_width, 0))))
                        vertices.push(...topWall)
                    } else if(current === 0 && top_neighbor > 0) {
                        const bottomWall = bottomFacing.map(v => v.add(voxel_center.add(new Vector3(0, half_voxel_width, 0))))
                        vertices.push(...bottomWall)
                    }

                    if(current > 0 && far_neighbor === 0) {
                        const farWall = farFacing.map(v => v.add(voxel_center.add(new Vector3(0, 0, half_voxel_width))))
                        vertices.push(...farWall)
                    } else if(current === 0 && far_neighbor > 0) {
                        const nearWall = nearFacing.map(v => v.add(voxel_center.add(new Vector3(0, 0, half_voxel_width))))
                        vertices.push(...nearWall)
                    }
                }
            }
        }

        const endTime = performance.now()
        const delta = endTime - startTime
        log.debug('Finished generating visible faces %dms', delta)

        this.initialize(vertices)
    }

    #property(property) {
        field('property').assert_string({ enum: Object.keys(this.#properties) })(property)
        return this.#properties[property]
    }

    get(x, y, z, property) {
        return this.#property(property).get(x, y, z)
    }

    set(x, y, z, property, value) {
        this.#property(property).set(x, y, z, value)
    }
}

class VoxelProperty {
    constructor(resolution, TypedArray) {
        field('resolution').assert_number()(resolution)
        this.resolution = resolution
        this.data = new TypedArray(Math.pow(this.resolution, DIMENSIONS))
    }

    #index(x, y, z) {
        field('x').assert_number({ min: 0, max: this.resolution - 1 })(x)
        field('y').assert_number({ min: 0, max: this.resolution - 1 })(y)
        field('z').assert_number({ min: 0, max: this.resolution - 1 })(z)
        return x + this.resolution * y + this.resolution * this.resolution * z
    }

    get(x, y, z) {
        return this.data[this.#index(x, y, z)]
    }

    set(x, y, z, value) {
        this.data[this.#index(x, y, z)] = value
    }
}

class MaterialProperty extends VoxelProperty {
    static AIR = 0
    static SAND = 1
    static DIRT = 2
    static STONE = 3

    static COLORS = [
        null,
        new Vector3(1, 0, 0),
        new Vector3(0, 1, 0),
        new Vector3(0, 0, 1),
    ]

    constructor(resolution) {
        super(resolution, Uint8Array)
    }

    get(x, y, z) {
        return super.get(x, y, z)
    }

    set(x, y, z, value) {
        field('value').assert_number({ min: MaterialProperty.AIR, max: MaterialProperty.STONE, integer: true})(value)
        super.set(x, y, z, value);
    }
}

class HeatProperty extends VoxelProperty {
    constructor(resolution) {
        super(resolution, Float32Array)
    }

    set(x, y, z, value) {
        field('value').assert_number({ min: 0 })(value)
        super.set(x, y, z, value);
    }
}
