import { Camera } from './camera.mjs'

export class Scene {
    constructor(objects, shaderSource) {
        this.objects = objects

        if(this.objects.find(o => o instanceof Camera) === undefined) {
            throw new Error('Scene must contain at least one camera')
        }

        // Currently supports only rendering multiple objects with the same pipeline (shaders, bind groups, ...)
        // TODO: allow scene to render multiple shapes with different pipelines
        this.shaderSource = shaderSource
    }

    get camera() {
        return this.objects.find(o => o instanceof Camera)
    }
}
