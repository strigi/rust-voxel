import {SpacialObject} from './spacial-object.mjs'
import {BLUE, DARK_BLUE, DARK_GREEN, DARK_RED, face, GREEN, RED} from './face-utils.mjs'
import {Matrix4} from '../math/index.mjs'

export class CubeMesh extends SpacialObject {
    static count = 0

    constructor() {
        super(`cube-${++CubeMesh.count}`)

        function front() {
            return face(Matrix4.translate(0, 0, -0.5), DARK_BLUE)
        }

        function back() {
            const tm = Matrix4.translate(0, 0, 0.5).mul(Matrix4.rotate(0, 180, 0))
            return face(tm, BLUE)
        }

        function left() {
            const tm = Matrix4.translate(-0.5, 0, 0).mul(Matrix4.rotate(0, 90, 0))
            return face(tm, DARK_RED)
        }

        function right() {
            const tm = Matrix4.translate(0.5, 0, 0).mul(Matrix4.rotate(0, -90, 0))
            return face(tm, RED)
        }

        function top() {
            const tm = Matrix4.translate(0, 0.5, 0).mul(Matrix4.rotate(90, 0, 0))
            return face(tm, GREEN)
        }

        function bottom() {
            const tm = Matrix4.translate(0, -0.5, 0).mul(Matrix4.rotate(-90, 0, 0))
            return face(tm, DARK_GREEN)
        }

        super.initialize([
            ...left(),
            ...right(),
            ...top(),
            ...bottom(),
            ...front(),
            ...back(),
        ])
    }
}
