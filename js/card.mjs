import {Camera} from './renderer/camera.mjs'
import field from './utils/validate.mjs'

const style = `   
    h1 {
        font-family: sans-serif;
        font-weight: normal;
        font-size: 10pt;
        color: white;
    }
    
    .error {
        padding: 1rem;
        color: rgb(240, 128, 128);
        background-color: rgba(240, 128, 128, 0.1);
    }
    
    .pre {
        white-space: pre;
    }
    
    .info {
        color: grey;
    }
`

export class Card extends HTMLElement {
    #root = document.createElement('div')
    #contentElement = document.createElement('content')

    constructor(title) {
        super()

        const shadow = this.attachShadow({ mode: 'closed' })
        shadow.appendChild(this.#root)
        const styleElement = document.createElement('style')
        shadow.appendChild(styleElement)
        styleElement.textContent = style

        const titleElement = document.createElement('h1')
        titleElement.innerText = title
        this.#root.appendChild(titleElement)
        this.#root.appendChild(this.#contentElement)
    }

    set content(html) {
        this.#contentElement.innerHTML = html
    }

}

export class CameraCard extends Card {
    constructor(camera) {
        field('camera').assert_defined().assert_instance(Camera)(camera)

        super('Camera')

        this.content = `
            <ul class="info">
                <li>Name: ${camera.name}</li>
                <li>Film: size=${(camera.width).toFixed(2)}x${(camera.height).toFixed(2)} (diagonal=${(camera.diagonal).toFixed(2)}) aspect=${camera.aspect.toFixed(2)}</li>
                <li>Projection: ${camera.perspective ? 'perspective': 'orthographic'}, near: ${camera.near.toFixed(3)}, far: ${camera.far.toFixed(3)} (depth=${camera.depth.toFixed(3)}, fovx: ${camera.fovx.toFixed(3)} deg, fovy: ${camera.fovy.toFixed(3)} deg</li>
                <li>Position: ${camera.position}, Orientation: ${camera.orientation}</li>
            </ul>
        `
    }
}

export class FrameCard extends Card {
    constructor({ averageFrameTime, averageFps, currentFrame, resolution: {width, height} }) {
        super('Frame')
        this.content = `
            <ul class="info">
                <li>Frame time: ${averageFrameTime.toFixed(1)}ms</li>
                <li>FPS: ${averageFps.toFixed(2)}</li>
                <li>Frame: ${currentFrame}</li>
                <li>Resolution: ${width}x${height}</li>
            </ul>
        `
    }
}

export class ErrorCard extends Card {
    constructor(error) {
        super('Error')

        field('error').assert_defined().assert_instance(Error)(error)

        this.content = `
            <div class="error">
                <div>${error.message}</div>
                ${ error.messages ?
                    `<div>
                        <ul>
                            <li>${error.messages.map(m => `${m.message} ${m.lineNum}:${m.linePos}`).join('</li><li>')}</li>
                        </ul>
                    </div>` : ''
                }
                
                <div class="pre">${error.stack}</div>
            </div>            
        `

        // const messageElement = document.createElement('div')
        // messageElement.innerText = 'ERROR'
        // messageElement.classList.add('bold')
        // messageElement.classList.add('danger')
        // messageElement.classList.add('bg-danger')
        // messageElement.classList.add('p-10')
        //
        // const stackElement = document.createElement('pre')
        // stackElement.innerText = error.stack
        //
        // const infoElement = document.createElement('div')
        // infoElement.appendChild(messageElement)
        // infoElement.appendChild(stackElement)
    }
}

customElements.define('strigi-error-card', ErrorCard)
window.customElements.define('strigi-card', Card);
window.customElements.define('strigi-camera-card', CameraCard);
window.customElements.define('strigi-frame-card', FrameCard);

