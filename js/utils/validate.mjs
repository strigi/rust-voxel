// Converters
// Traversal property('city.postalCode') or child('city').child('postalCode)
    // Context needs to be built per stage
    // for(let i = 0; i < ctx.value.length; i++) {
    //     field(`${self.name}[${i}]`).assert_number()(ctx.value[i])
    // }
    // field('array').assert_array({ sub: field().assert_number() })
    // field('person').assert_array({ sub: {
    //     age: field().assert_number({min: 18}),
    //     name: field().assert_string()
    // } })
    // Somehow push the paths `person.name` and `array[0]` into the stages
//TODO: min max equal more elegant error message
// string: min, max, blank, match
// number: min, max, nan, finite
// array: min, max, nesting
// object: property, nesting,
// optional
// asserts should be null/undefined safe
// casting vs strict

// - Optional (required: false)
// - conversions of optionals and overloading of arguments

// or sub({ 'city': field() }), ...

export default function field(name) {
    name = name ?? null

    const stages = []

    const self = value => {
        let intermediate_value = value;
        const ctx = build_context(value)
        for(const stage of stages) {
            intermediate_value = stage(ctx)
        }
        return intermediate_value
    }

    function build_context(value) {
        return {
            name,
            value,

            get type() {
                if(this.value === null) {
                    return 'null'
                }
                return typeof this.value
            },

            get instance() {
                return this.value?.constructor?.name
            }
        }
    }

    function build_validation_message_prefix(ctx) {
        let name_and_value;
        if(name !== null) {
            name_and_value = `Field '${ctx.name}' with v`
        } else {
            name_and_value = 'V'
        }
        name_and_value += `alue '${ctx.value}'`

        if(ctx.value === null || ctx.value === undefined) {
            return name_and_value
        }

        const type = `of type '${ctx.type}'`
        if(typeof ctx.value !== 'object') {
            return `${name_and_value} ${type}`
        }

        const instance = `instance of '${ctx.instance}'`
        return `${name_and_value} ${instance}`
    }

    self.assert_defined = (reason) => {
        stages.push(ctx => {
            if(ctx.value === null || ctx.value === undefined) {
                FieldError.throw(reason ?? (ctx => `${build_validation_message_prefix(ctx)} must neither be null nor undefined`), ctx)
            }
            return ctx.value
        })
        return self
    }

    self.assert_type = (type, opts) => {
        stages.push(ctx => {
            if(typeof ctx.value !== type) {
                FieldError.throw(opts?.reason ?? (ctx =>`${build_validation_message_prefix(ctx)} must be of type '${type}'`), ctx)
            }
            return ctx.value
        })
        return self
    }

    self.assert_number = (opts) => {
        const options = {
            min: undefined,
            max: undefined,
            nan: false,
            finite: true,
            integer: false,
            ...opts
        }

        if(options.min && typeof options.min !== 'number') {
            throw new Error('Invalid arg opts.min')
        }

        if(options.max && typeof options.max !== 'number') {
            throw new Error('Invalid arg opts.min')
        }

        if(typeof options.nan !== 'boolean') {
            throw new Error('Invalid arg opts.nan')
        }

        if(typeof options.finite !== 'boolean') {
            throw new Error('Invalid arg opts.finite')
        }

        if(typeof options.integer !== 'boolean') {
            throw new Error('Invalid arg opts.integer')
        }

        self.assert_type('number', opts)

        stages.push(ctx => {
            if(options.min && options.min > ctx.value) {
                FieldError.throw(options.reason ?? (ctx => `${build_validation_message_prefix(ctx)} must be >= '${opts.min}'`), ctx)
            }

            if(options.max && options.max < ctx.value) {
                FieldError.throw(options.reason ?? (ctx => `${build_validation_message_prefix(ctx)} must be <= '${opts.max}'`), ctx)
            }

            if(options.nan === false && Number.isNaN(ctx.value)) {
                FieldError.throw(options.reason ?? (ctx => `${build_validation_message_prefix(ctx)} must not be special value NaN`), ctx)
            }

            if(options.finite === true && !Number.isNaN(ctx.value) && !Number.isFinite(ctx.value)) {
                FieldError.throw(options.reason ?? (ctx => `${build_validation_message_prefix(ctx)} must not be special value ${Number.POSITIVE_INFINITY}`), ctx)
            }

            if(options.integer === true && !Number.isInteger(ctx.value)) {
                FieldError.throw(options.reason ?? (ctx => `${build_validation_message_prefix(ctx)} must be an integer (not a float)`), ctx)
            }

            return ctx.value
        })

        return self
    }

    self.assert_array = (opts) => {
        const options = {
            ...opts,
        }

        stages.push(ctx => {
            if(!Array.isArray(ctx.value)) {
                FieldError.throw(options.reason ?? (ctx => `${build_validation_message_prefix(ctx)} must be an Array`), ctx)
            }

            const length = ctx.value.length

            if(defined(options.length) && length !== options.length) {
                FieldError.throw(options.reason ?? (ctx => `${build_validation_message_prefix(ctx)} length '${length}' must be '${options.length}'`), ctx)
            }

            if(defined(options.min) && defined(options.max) && (length < options.min || length > options.max)) {
                FieldError.throw(options.reason ?? (ctx => `${build_validation_message_prefix(ctx)} length '${length}' must be between '${options.min} and '${options.max}'`), ctx)
            }

            if(defined(options.min) && length < options.min) {
                FieldError.throw(options.reason ?? (ctx => `${build_validation_message_prefix(ctx)} length '${length}' must be >= '${options.min}'`), ctx)
            }

            if(defined(options.max) && length > options.max) {
                FieldError.throw(options.reason ?? (ctx => `${build_validation_message_prefix(ctx)} length '${length}' must be <= '${options.max}'`), ctx)
            }

            return ctx.value

            function defined(value) {
                return value !== undefined && value !== null
            }
        })

        return self
    }

    // self.cast_number = () => {
    //     stages.push((ctx) => {
    //         return Number(ctx.value)
    //     })
    // }

    self.assert_string = (opts) => {
        const options = {
            message: null,
            enum: null,
            ...opts
        }

        self.assert_type('string', opts)

        stages.push(ctx => {
            if(options.enum && !options.enum.includes(ctx.value)) {
                FieldError.throw(standardMessage(options, ctx, `must be one of [${options.enum}]`))
            }

            return ctx.value
        })

        return self
    }

    self.assert_boolean = (opts) => {
        return self.assert_type('boolean', opts)
    }

    self.assert_object = (opts) => {
        return self.assert_type('object', opts)
    }

    self.assert_function = (opts) => {
        return self.assert_type('function', opts)
    }

    self.assert_instance = (constructor, opts) => {
        if(typeof constructor !== 'function') {
            throw new Error('Invalid argument')
        }

        stages.push(ctx => {
            if(!(ctx.value instanceof constructor)) {
                FieldError.throw(opts?.reason ?? `${build_validation_message_prefix(ctx)} must be an instance of '${constructor.name}' but was '${constructor?.name}'`, ctx)
            }
            return ctx.value
        })

        return self
    }

    self.toString = () => {
        return `field{name=${name}, stages.length=${stages.length}}`
    }

    return self

    function standardMessage(options, context, detail) {
        return options.reason ?? `${build_validation_message_prefix(context)} ${detail}`
    }
}

class FieldError extends Error {
    static throw(reason, context) {
        let message
        if(typeof reason === 'function') {
            message = reason(context)
        } else {
            message = reason
        }
        throw new FieldError(message)
    }

    constructor(message) {
        super(message)
        this.name = FieldError.name
    }
}
