import logger from "./logger.mjs";

const log = logger.getFromMeta(import.meta)

export async function fetchText(uri) {
    log.verbose(`Fetching text from URI '%s'`, uri)
    const response = await fetch(uri)
    if(response.status !== 200) {
        log.warn(`Unable to fetch text from URI '%s'`, uri)
        return null
    }
    const text = await response.text()
    log.silly(`Retrieved text from URI '%s':\n%s`, uri, text)
    return text;
}
