import field from './validate.mjs'

const allowed_levels = ['silly', 'debug', 'verbose', 'info', 'warn', 'error'].reverse()

class Logger {
    #config

    constructor(name, config) {
        this.name = name
        this.config = config
    }

    set config(config) {
        field('config').assert_object()(config)
        const entries = Object.entries(config)
        for(let i = 0; i < entries.length; i += 1) {
            const [key, value] = entries[i]
            field(`config[${key}]`).assert_string({ enum: allowed_levels })(value)
        }
        this.#config = config
    }

    enabled(name, level) {
        for(const [key, value] of Object.entries(this.#config)) {
            if(new RegExp(key).test(name) === false) {
                continue
            }
            return allowed_levels.indexOf(value) >= allowed_levels.indexOf(level)
        }
    }

    silly(message, ...splat) {
        this.#log(this.name, 'silly', message, splat)
    }

    debug(message, ...splat) {
        this.#log(this.name, 'debug', message, splat)
    }

    verbose(message, ...splat) {
        this.#log(this.name, 'debug', message, splat)
    }

    info(message, ...splat) {
        this.#log(this.name, 'info', message, splat)
    }

     warn(message, ...splat) {
         this.#log(this.name, 'warn', message, splat)
    }

    error(message, ...splat) {
        this.#log(this.name, 'error', message, splat)
    }

    get(name) {
        return new Logger(name, this.#config)
    }

    getFromMeta(yourMeta) {
        field().assert_object()
        return this.get(yourMeta.url.split('/').pop())
    }

    #log(name, level, message, splat) {
        if(!allowed_levels.includes(level)) {
            throw new Error(`Invalid log level '${level}' must be any of '${allowed_levels.join(',', )}'`)
        }

        if(!this.enabled(name, level)) {
            return
        }

        if(message === null) {
            message = 'null'
        } else if(message === undefined) {
            message = 'undefined'
        } else if(message instanceof Error) {
            splat = [message, ...splat]
            message = message.stack
        } else if(message instanceof  Object) {
            splat = [message, ...splat]
            message = message.toString()
        }

        const color_map = {
            silly: '#F2A2E8',
            debug: '#A7C7E7',
            verbose: 'teal',
            info: 'lightgreen',
            warn: 'gold',
            error: '#F08080',
        }
        const color = color_map[level] ? `color: ${color_map[level]};` : ''

        const splat_size = count_splat_placeholders(message)
        const used_splat = splat.slice(0, splat_size)
        const unused_splat = splat.slice(splat_size)

        const timestamp = new Date()
        const formatted_timestamp = timestamp.toISOString()

        const formatted_level = `[${level}]`.padEnd(longest_level() + 2)

        const max_name_length = 20;
        const formatted_name = name.padEnd(max_name_length)

        const attachments = '\n%o'.repeat(unused_splat.length)
        console.log(`%c${formatted_timestamp} ${formatted_level} ${formatted_name} ${message}${attachments}`, color, ...used_splat, ...unused_splat)
    }
}

const logger = new Logger('root', {
    '^.*$': 'silly'
})
export default logger;

function count_splat_placeholders(message) {
    const message_type = typeof message;
    if(message_type !== 'string') {
        throw new Error(`Argument 'message' must be of type string but value '${message}' is of type '${message_type}'`)
    }

    let count = 0;
    for(let i = 0; i < message.length - 1; i += 1) {
        // If this is not a % we don't need to look ahead
        const current_char = message[i]
        if(current_char !== '%') {
            continue
        }

        // If this is an escape of % (e.g. %%), it's not a placeholder.
        const next_char = message[i + 1]
        if(next_char === '%') {
            continue
        }

        count += 1
    }

    return count
}

function longest_level() {
    return allowed_levels.map(l => l.length).reduce((l, r) => Math.max(l, r), 0)
}