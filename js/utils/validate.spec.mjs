import {describe, test, expect, beforeEach} from 'vitest'

import field from './validate.mjs'

describe('field()', () => {
    let foo
    beforeEach(() => {
        foo = field('foo')
    })

    test('passes through unchanged if no stages are specified', () => {
        expect(foo(1507)).toBe(1507)
        expect(foo('hello')).toBe('hello')
        expect(foo(true)).toBe(true)

        let object = {}
        expect(foo(object)).toBe(object)
    })

    describe('assert_defined()', () => {
        test('passes through if value is not null or undefined', () => {
            expect(foo.assert_defined()('hello')).toBe('hello')
        })

        test('throws if value is null or undefined', () => {
            expect(() => foo.assert_defined()(null)).toThrow(`Field 'foo' with value 'null' must neither be null nor undefined`)
            expect(() => foo.assert_defined()(undefined)).toThrow(`Field 'foo' with value 'undefined' must neither be null nor undefined`)
        })

        test('uses custom message if reason is given', () => {
            expect(() => foo.assert_defined(`Because I say so`)(null)).toThrow(`Because I say so`)
        })

        test('allows interpolation if reason is a function', () => {
            expect(() => foo.assert_defined(ctx => `${ctx.name} ${ctx.value} ${ctx.type} ${ctx.instance}`)(null)).toThrow(`foo null null undefined`)
        })
    })

    describe('assert_number()', () => {
        test('passes through if value is a number', () => {
            expect(foo.assert_number()(1507)).toBe(1507)
        })

        test('passes through if value is a number within bounds of min and max', () => {
            expect(foo.assert_number({ min: 1000, max: 2000 })(1507)).toBe(1507)
        })

        test('throws if value is not a number', () => {
            expect(() => foo.assert_number()(null)).toThrow(`Field 'foo' with value 'null' must be of type 'number'`)
            expect(() => foo.assert_number()(undefined)).toThrow(`Field 'foo' with value 'undefined' must be of type 'number'`)
            expect(() => foo.assert_number()(false)).toThrow(`Field 'foo' with value 'false' of type 'boolean' must be of type 'number'`)
            expect(() => foo.assert_number()({})).toThrow(`Field 'foo' with value '[object Object]' instance of 'Object' must be of type 'number'`)
        })

        test('throws if value is less than min', () => {
            expect(() => foo.assert_number({min: 2000})(1507)).toThrow(`Field 'foo' with value '1507' of type 'number' must be >= '2000'`)
        })

        test('throws if value is more than max', () => {
            expect(() => foo.assert_number({max: 1000})(1507)).toThrow(`Field 'foo' with value '1507' of type 'number' must be <= '1000'`)
        })

        test('uses custom message if reason is given', () => {
            expect(() => foo.assert_number({reason: `Because I say so`})('Hello World')).toThrow(`Because I say so`)
        })

        test('allows interpolation if reason is a function', () => {
            expect(() => foo.assert_number({ reason: ctx => `${ctx.name} ${ctx.value} ${ctx.type} ${ctx.instance}` })('Hello World')).toThrow(`foo Hello World string String`)
        })

        test('NaN is not allowed by default', () => {
            expect(() => foo.assert_number()(NaN)).toThrow(`Field 'foo' with value 'NaN' of type 'number' must not be special value NaN`)
        })

        test('NaN is not allowed if nan is false', () => {
            expect(() => foo.assert_number({ nan: false })(NaN)).toThrow(`Field 'foo' with value 'NaN' of type 'number' must not be special value NaN`)
        })

        test('NaN is allowed if nan is true', () => {
            expect(foo.assert_number({ nan: true })(NaN)).toBe(NaN)
        })

        test('Infinite is not allowed by default', () => {
            let field = foo.assert_number()
            expect(() => field(Number.NEGATIVE_INFINITY)).toThrow(`Field 'foo' with value '-Infinity' of type 'number' must not be special value Infinity`)
            expect(() => field(Number.POSITIVE_INFINITY)).toThrow(`Field 'foo' with value 'Infinity' of type 'number' must not be special value Infinity`)
        })

        test('Infinite is not allowed if finite is true', () => {
            const field = foo.assert_number({ finite: true })
            expect(() => field(Number.NEGATIVE_INFINITY)).toThrow(`Field 'foo' with value '-Infinity' of type 'number' must not be special value Infinity`)
            expect(() => field(Number.POSITIVE_INFINITY)).toThrow(`Field 'foo' with value 'Infinity' of type 'number' must not be special value Infinity`)
        })

        test('Infinity is allowed if finite is false', () => {
            const field = foo.assert_number({ finite: false })
            expect(field(Number.NEGATIVE_INFINITY)).toBe(Number.NEGATIVE_INFINITY)
            expect(field(Number.POSITIVE_INFINITY)).toBe(Number.POSITIVE_INFINITY)
        })

        test('float is allowed by default', () => {
            expect(foo.assert_number()(3.14)).toBe(3.14)
        })

        test('float throws if integer is true', () => {
            expect(() => foo.assert_number({ integer: true })(3.14)).toThrow(`Field 'foo' with value '3.14' of type 'number' must be an integer (not a float)`)
        })
    })

    describe('assert_array()', () => {
        test('passes through if array', () => {
            const a = []
            foo.assert_array()
            expect((a)).toBe(a)
        })

        test('throws if not an array', () => {
            expect(() => foo.assert_array()('Hello')).toThrow(`Field 'foo' with value 'Hello' of type 'string' must be an Array`)
        })

        test('passes if length > min', () => {
            expect(foo.assert_array({ min: 5 })(['a', 'b', 'c', 'd', 'e', 'f'])).toHaveProperty('length', 6)
        })

        test('passes if length < max', () => {
            expect(foo.assert_array({ max: 3 })(['a', 'b'])).toHaveProperty('length', 2)
        })

        test('throws if length < min with no upper bound', () => {
            expect(() => foo.assert_array({ min: 3 })(['a', 'b'])).toThrow(`Field 'foo' with value 'a,b' instance of 'Array' length '2' must be >= '3'`)
        })

        test('throws if length is outside of bounds defined by min and max', () => {
            expect(() => foo.assert_array({ min: 3, max: 5 })(['a', 'b'])).toThrow(`Field 'foo' with value 'a,b' instance of 'Array' length '2' must be between '3 and '5'`)
        })

        test('throws if length > max with no lower bound', () => {
            expect(() => foo.assert_array({ max: 2 })(['a', 'b', 'c'])).toThrow(`Field 'foo' with value 'a,b,c' instance of 'Array' length '3' must be <= '2'`)
        })
    })

    describe('assert_string()', () => {
        test('throws if value is not a string', () => {
            expect(() => foo.assert_string()(false)).toThrow(`Field 'foo' with value 'false' of type 'boolean' must be of type 'string'`)
            expect(() => foo.assert_string()(null)).toThrow(`Field 'foo' with value 'null' must be of type 'string'`)
            expect(() => foo.assert_string()(undefined)).toThrow(`Field 'foo' with value 'undefined' must be of type 'string'`)
            expect(() => foo.assert_string()(1507)).toThrow(`Field 'foo' with value '1507' of type 'number' must be of type 'string'`)
            expect(() => foo.assert_string()({})).toThrow(`Field 'foo' with value '[object Object]' instance of 'Object' must be of type 'string'`)
            expect(() => foo.assert_string()(['x'])).toThrow(`Field 'foo' with value 'x' instance of 'Array' must be of type 'string'`)
        })

        test('passes if value is included in enum', () => {
            expect(foo.assert_string({enum: ['a', 'b', 'c']})('b')).toBe('b')
        })

        test('throws if value is not included in enum', () => {
            expect(() => foo.assert_string({enum: ['a', 'b', 'c']})('d')).toThrow(`Field 'foo' with value 'd' of type 'string' must be one of [a,b,c]`)
        })
    })
})