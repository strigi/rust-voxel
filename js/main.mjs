import logger from './utils/logger.mjs'
import renderer from './renderer/renderer.mjs'
import {CubeMesh} from './renderer/cube-mesh.mjs'
import {fetchText} from './utils/fetch.mjs'
import {Camera} from './renderer/camera.mjs'
import {Vector3} from './math/vector.mjs'
import {Actor} from './actor.mjs'
import {Scene} from './renderer/scene.mjs'
import {CameraCard, ErrorCard, FrameCard} from './card.mjs'
import {VoxelMesh} from './renderer/voxel-mesh.mjs'

const log = logger.getFromMeta(import.meta)

const info = document.querySelector('#info')

try {
    log.info('Creating renderer')

    // Width and height of the canvas
    const aspectRatio = 16 / 9
    const width = 800
    const height = width / aspectRatio

    const r = await renderer('canvas#a', { width, height })
    const s = await renderer('canvas#b', { width, height })

    function addFullscreenHandler(canvas) {
        canvas.addEventListener('dblclick', async event => {
            if(document.fullscreenElement) {
                await document.exitFullscreen()
            } else {
                await canvas.requestFullscreen()
            }
        })
    }
    addFullscreenHandler(r.canvas)
    addFullscreenHandler(s.canvas)

    let past = Date.now()

    let currentFrame = 0
    let markedFrame = currentFrame

    log.info('Creating scene objects')
    const objects = [
        new Camera(r.width / 1000, r.height / 1000),
        new Camera(r.width / 1000, r.height / 1000),
        // new CubeMesh()
        new VoxelMesh({resolution: 10}).rotate(new Vector3(-22.5, -22.5, 0))
    ]

    const scene_graph = document.querySelector('#scene-graph')
    let count = 0
    for(const mesh of objects) {
        const scene_graph_item = document.createElement('li')
        scene_graph_item.innerText = `${mesh.name} (#${mesh.id})`
        scene_graph_item.classList.add('cursor-pointer')
        if(count === 0) {
            scene_graph_item.classList.add('selected')
        }
        scene_graph_item.addEventListener('click', event => {
            actor.activeObject = mesh
            for(const child of scene_graph.children) {
                child.classList.remove('selected')
            }
            event.target.classList.add('selected')
        })
        scene_graph.appendChild(scene_graph_item)
        count++
    }

    const scene = new Scene(objects, await fetchText('combo.wgsl'))

    log.info('Creator actor')
    const actor = new Actor(scene)

    const key_bindings = document.querySelector('#key-bindings')
    for(const key of actor.controls.boundKeys()) {
        const key_bindings_item = document.createElement('li')
        key_bindings_item.innerText = key.code
        key_bindings.appendChild(key_bindings_item)
    }

    log.info('Preparing scene')
    await r.prepare(scene)
    await s.prepare(scene)

    const camera_a = scene.objects.find(o => o.name === 'camera-1')
    camera_a.translate(new Vector3(-5, 0, 6))
    camera_a.rotate(new Vector3(0, -90, -0))
    const camera_b = scene.objects.find(o => o.name === 'camera-2')
    camera_b.translate(new Vector3(0, 0, -10))
    camera_b.rotate(new Vector3(0, 0, 0))

    log.info('Starting rendering loop')
    ;(function loop(ticks = 0) {
        let handle
        try {
            handle = requestAnimationFrame(loop)
            currentFrame += 1
            const now = Date.now()

            const time = {
                now: now / 1000,
                delta: (now - past) / 1000
            }

            actor.act(time);

            r.render(camera_a)
            s.render(camera_b)

            if(time.delta > 0.1) {
                info.innerHTML = ``

                info.appendChild(new CameraCard(scene.camera))

                const frameDelta = currentFrame - markedFrame
                const averageFrameTime = 1000 * time.delta / frameDelta
                const averageFps = frameDelta / time.delta
                info.appendChild(new FrameCard({ averageFrameTime, averageFps, currentFrame, resolution: { width: r.width, height: r.height } }))

                // info.innerHTML += `<br/><strong>Active keys:</strong> ${[...a.controls.activeKeys()].map(k => k.bound ? k.code : `<span class='warn'>${k.code}${k.analog ? '=' + k.value.toFixed(3) : '' }</span>`).join(', ')}`
                // info.innerHTML += `<br/><strong>Controlled object (toggle using number keys):</strong> name: ${a.activeObject.name}, position: ${a.activeObject.position}, orientation: ${a.activeObject.orientation}`

                past = now
                markedFrame = currentFrame
            }
        } catch(error) {
            cancelAnimationFrame(handle)
            handleError(error)
        }
    })()
} catch(error) {
    handleError(error)
}

function handleError(error) {
    info.innerHTML = ``
    log.error(error.stack, error)
    info.appendChild(new ErrorCard(error))
}