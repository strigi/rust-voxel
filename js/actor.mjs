import {Controls} from './controls/controls.mjs'
import logger from './utils/logger.mjs'
import {Vector3} from './math/index.mjs'

const log = logger.getFromMeta(import.meta)

export class Actor {
    controls

    constructor(scene) {
        this.scene = scene

        const self = this

        this.activeObject = scene.objects[0]

        log.info('Registering keybindings for camera and mesh controller')
        this.controls = new Controls()
        const movingAmount = 0.05

        // Left right
        bindMany(['ArrowLeft', 'KeyA'], () => this.activeObject.translate(new Vector3(-movingAmount, 0, 0)))
        bindMany(['ArrowRight', 'KeyD'], () => this.activeObject.translate(new Vector3(movingAmount, 0, 0)))

        bindOne('GamepadAxis1', key => {
            if(key.value > 0) {
                this.activeObject.translate(new Vector3(movingAmount, 0, 0))
            } else if(key.value < 0) {
                this.activeObject.translate(new Vector3(-movingAmount, 0, 0))
            } else {
                throw new Error('Why is active with value zero ???')
            }
        })

        bindOne('GamepadAxis2', key => {
            if(key.value > 0) {
                this.activeObject.translate(new Vector3(0, 0, -movingAmount))
            } else if(key.value < 0) {
                this.activeObject.translate(new Vector3(0, 0, movingAmount))
            } else {
                throw new Error('Why is active with value zero ???')
            }
        })

        // Forward backward
        bindMany(['ArrowUp', 'KeyW'], () => this.activeObject.translate(new Vector3(0, 0, movingAmount)))
        bindMany(['ArrowDown', 'KeyS'], () => this.activeObject.translate(new Vector3(0, 0, -movingAmount)))

        // Up down
        bindMany(['PageUp', 'Space', 'GamepadButton7'], () => this.activeObject.translate(new Vector3(0, movingAmount, 0)))
        bindMany(['PageDown', 'ShiftLeft', 'GamepadButton8'], () => this.activeObject.translate(new Vector3(0, -movingAmount, 0)))
        bindMany(['PageDown', 'ShiftLeft', 'GamepadButton8'], () => this.activeObject.translate(new Vector3(0, -movingAmount, 0)))

        function bindMany(keys, listener) {
            for(const key of keys) {
                bindOne(key, listener)
            }
        }

        function bindOne(key, listener) {
            self.controls.bind(key, listener)
        }

        this.controls.bind('KeyP', () => this.scene.camera.perspective = !this.scene.camera.perspective, { once: true })
        this.controls.bind('NumpadAdd', () => this.scene.camera.addFov(+0.01), { once: false })
        this.controls.bind('NumpadSubtract', () => this.scene.camera.addFov(-0.01), { once: false })

        this.controls.bind('KeyR', () => this.activeObject.rotate(new Vector3(+1, 0, 0)))
        this.controls.bind('KeyT', () => this.activeObject.rotate(new Vector3(-1, 0, 0)))
        this.controls.bind('KeyF', () => this.activeObject.rotate(new Vector3(0, +1, 0)))
        this.controls.bind('KeyG', () => this.activeObject.rotate(new Vector3(0, -1, 0)))
        this.controls.bind('KeyV', () => this.activeObject.rotate(new Vector3(0, 0, +1)))
        this.controls.bind('KeyB', () => this.activeObject.rotate(new Vector3(0, 0, -1)))

        // const turningAmount = 0.5

        // this.controls.bind('KeyS', () => scene.objects[0])
        // this.controls.bind('KeyA', () => scene.objects[0])
        // this.controls.bind('KeyD', () => scene.objects[0])
        // this.controls.bind('KeyA', () => scene.objects[0])
        // this.controls.bind('KeyE', () => scene.objects[0])
        // this.turning(`KeyW`, new Vector3(-turningAmount, 0, 0))
        // this.turning(`KeyA`, new Vector3(0, turningAmount, 0))
        // this.turning(`KeyS`, new Vector3(0, -turningAmount, 0))
        // this.turning(`KeyZ`, new Vector3(0, 0, turningAmount))
        // this.turning(`KeyX`, new Vector3(0, 0, -turningAmount))
        //
        // this.moving('KeyE', new Vector3(movingAmount, 0, 0))
        // this.moving('KeyR', new Vector3(-movingAmount, 0, 0))
        // this.moving('KeyD', new Vector3(0, movingAmount, 0))
        // this.moving('KeyF', new Vector3(0, -movingAmount, 0))
        // this.moving('KeyC', new Vector3(0, 0, movingAmount))
        // this.moving('KeyV', new Vector3(0, 0, -movingAmount))
    }

    act() {
        // const speed = 3
        // for(const mesh of this.scene.objects) {
        //     mesh.rotate(new Vector3(speed / 11, speed / 13, speed / 17))
        // }
        this.controls.control()
    }
}
