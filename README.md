# Voxels

## WebGPU

WebGPU is the next-gen API for GPU programming, that goes beyond the capabilities of WebGL.
It is <abbr title="2023-05-18">currently</abbr> experimental in most browsers, but will land in Chromium soon.

### Specifications

- **WebGPU DOM API**
  - https://www.w3.org/TR/webgpu
- **WGSL (Shading Language)**
  - https://www.w3.org/TR/WGSL/

### Examples

https://austin-eng.com/webgpu-samples/

### Browser support

Currently (2023-01-08) WebGPU is not enabled by default on any browser.

- **Chromium** <small>(? >= v >= 108)</small>
  - Enabled by starting with the flag `enable-unsafe-webgpu` set to `true`, which can be done either via `chrome://flags/` or by setting it as a CLI option `--enable-unsafe-webgpu`
  - Additionally, when using _Linux_, _Chromium_ must be started with `--enable-features=Vulkan` (only via CLI, not available as a flag)
  ```sh
  # Example on Linux using only CLI options (no chrome://flags):
  $ chromium --enable-features=Vulkan --enable-unsafe-webgpu
  ```
  - More info can be found on the [WebGPU Orign Trial page](https://developer.chrome.com/docs/web-platform/webgpu/)
  - Tip: Using _IntelliJ_, you can add the CLI flags for the configured browsers, and start it using a _JavaScript Debug_ run configuration, which allows full debugging support from within the IDE.  

## WGPU

A Rust graphics API that will serve as the implementation of WebGPU for Firefox.

### Modules

```mermaid
graph LR
  A[math]
  B[scene]
  C[renderer]
  D[window]
  E[control]
  F[platform]
```

- _math_: General purpose math functions for `Vector`, `Matrix`, ...
- _scene_: Geometry related objects like `Mesh`, `Voxel`, `Vertex`, `Camera`. No specific WGPU stuff.
- _platform_: Platform specific implementations of `Window` like SDL, ...
- _renderer_: WGPU related logic, to work with surfaces, adapters, devices, pipelines, queues, buffers, ...

### WASM

WGPU rust can be compiled into WASM and run directly in the browser.

https://gfx-rs.github.io/2020/04/21/wgpu-web.html

